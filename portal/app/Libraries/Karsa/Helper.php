<?php

namespace Karsa;

use Auth;
use Config;
use DateTime;
use Route;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Illuminate\Support\Facades\Storage;

class Helper
{

    const FORMAT_DATE_ID_SHORT = 1;
    const FORMAT_DATE_ID_LONG = 2;
    const FORMAT_DATE_ID_LONG_DAY = 3;
    const FORMAT_DATE_EN_SHORT = 4;
    const FORMAT_DATETIME_ID_LONG_DAY = 5;
    const FORMAT_DATE_EN_SHORTEST = 6;

    /*
     * Number
     */

    public static function idNumber($number, $decimal = 0)
    {
        return number_format($number, $decimal);
    }

    public static function romawi($angka = false)
    {
        $tmp = array(1 => 'I', 'II', 'III', 'IV', 'V', 'VI', 'VII', 'VIII', 'IX', 'X', 'XI', 'XII');
        return $angka ? $tmp[$angka] : $tmp;
    }

    private static function _private_terbilang_baca($x)
    {
        $abil = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
        if ($x < 1000000000) {
            $x += 0;
        }
        if ($x < 12) {
            return " " . $abil[$x];
        } elseif ($x < 20) {
            return self::_private_terbilang_baca($x - 10) . " belas";
        } elseif ($x < 100) {
            return self::_private_terbilang_baca($x / 10) . " puluh" . self::_private_terbilang_baca($x % 10);
        } elseif ($x < 200) {
            return " seratus" . self::_private_terbilang_baca($x - 100);
        } elseif ($x < 1000) {
            return self::_private_terbilang_baca($x / 100) . " ratus" . self::_private_terbilang_baca($x % 100);
        } elseif ($x < 2000) {
            return " seribu" . self::_private_terbilang_baca($x - 1000);
        } elseif ($x < 1000000) {
            return self::_private_terbilang_baca($x / 1000) . " ribu" . self::_private_terbilang_baca($x % 1000);
        } elseif ($x < 1000000000) {
            return self::_private_terbilang_baca($x / 1000000) . " juta" . self::_private_terbilang_baca($x % 1000000);
        } elseif (strlen($x) < 13) {
            return self::_private_terbilang_baca(substr($x, 0, -9)) . " milyar" . self::_private_terbilang_baca(substr($x, strlen($x) - 9));
        } elseif (strlen($x) < 16) {
            return self::_private_terbilang_baca(substr($x, 0, -12)) . " triliun" . self::_private_terbilang_baca(substr($x, strlen($x) - 12));
        } else {
            return '';
        }
    }

    public static function terbilang($x)
    {
        $x .= '';
        $x = self::_private_terbilang_baca($x);
        return trim(ucwords(strtolower($x)));
    }

    /*
     * Date
     */

    public static function dateDiff($start, $end)
    {
        return (strtotime($end) - strtotime($start)) / 60 / 60 / 24;
    }

    public static function hari($hari = false)
    {
        $tmp = array(1 => 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jum\'at', 'Sabtu', 'Minggu');
        return $hari ? $tmp[$hari] : $tmp;
    }

    public static function bulan($bulan = false)
    {
        $tmp = array(1 => 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
        return $bulan ? $tmp[$bulan] : $tmp;
    }

    public static function getWorkDay($jump = 0, $date = FALSE, $format = self::FORMAT_DATE_ID_SHORT)
    {
        $date = new DateTime($date);
        do {
            if (!($date->format('N') > 5)) {
                $jump--;
            } else {
                $jump = 1;
                $date->modify('+1 day');
            }
        } while ($jump > 0);
        return self::formatDate($date->format('Y-m-d'), $format);
    }

    public static function formatDate($date, $format = self::FORMAT_DATE_ID_LONG)
    {
        if ($date == NULL)
            return NULL;
        switch ($format) {
            case self::FORMAT_DATE_ID_SHORT:
                return date('d-m-Y', strtotime($date));
            case self::FORMAT_DATETIME_ID_LONG_DAY:
            case self::FORMAT_DATE_ID_LONG_DAY:
            case self::FORMAT_DATE_ID_LONG:
                $res = array();
                preg_match('/(\d)\-(\d+)\-(\d+)\-(\d{4})\-(\d+)\-(\d+)\-(\d+)/', date('N-d-n-Y-H-i-s', strtotime($date)), $res);
                if ($format == self::FORMAT_DATE_ID_LONG)
                    return sprintf('%d %s %s', $res[2], self::bulan($res[3]), $res[4]);
                else if ($format == self::FORMAT_DATE_ID_LONG_DAY)
                    return sprintf('%s, %d %s %s', self::hari($res[1]), $res[2], self::bulan($res[3]), $res[4]);
                else if ($format == self::FORMAT_DATETIME_ID_LONG_DAY)
                    return sprintf('%s, %d %s %s. Pukul %s:%s', self::hari($res[1]), $res[2], self::bulan($res[3]), $res[4], $res[5], $res[6]);
                break;
            case self::FORMAT_DATE_EN_SHORT:
                return date('Y-m-d', strtotime($date));
            case self::FORMAT_DATE_EN_SHORTEST:
                return date('ymd', strtotime($date));
            default:
                return date($format, strtotime($date));
        }
    }
    
    public static function formatDateEn($date)
    {
        return self::formatDate($date, self::FORMAT_DATE_EN_SHORT);
    }
    
    public static function formatDateId($date)
    {
        return self::formatDate($date, self::FORMAT_DATE_ID_SHORT);
    }

    public static function getTodayDate($format = self::FORMAT_DATE_ID_SHORT)
    {
        return self::formatDate(date('d-m-Y H:i:s'), $format);
    }

    public static function dateGrouping($Ymd1, $Ymd2)
    {
        $Ymd1 = self::formatDate($Ymd1, true, self::FORMAT_DATE_EN_SHORT);
        $Ymd2 = self::formatDate($Ymd2, true, self::FORMAT_DATE_EN_SHORT);

        list($y1, $m1, $d1) = explode('-', $Ymd1);
        list($y2, $m2, $d2) = explode('-', $Ymd2);

        if ($y1 != $y2)
            $grouping = self::formatDate($Ymd1) . ' s.d ' . self::formatDate($Ymd2);
        else {
            if ($m1 == $m2) {
                if ($d1 == $d2)
                    $grouping = self::formatDate($Ymd2);
                else
                    $grouping = $d1 . ' s.d ' . self::formatDate($Ymd2);
            } else
                $grouping = self::formatDate($Ymd1, false) . ' s.d ' . self::formatDate($Ymd2);
        }
        return $grouping;
    }

    /*
     * Fluent
     */

    public static function fluentMultiSearch($rootQuery, $searchString, $fieldsCommaSeparated)
    {
        $string = explode(' ', str_replace('  ', ' ', $searchString));
        $fields = explode(',', $fieldsCommaSeparated);
        $rootQuery->where(function() use ($rootQuery, $string, $fields) {
            foreach ($string as $v) {
                $rootQuery->where(function ($andQuery) use ($rootQuery, $fields, $v) {
                    foreach ($fields as $w) {
                        $andQuery->orWhere($w, 'LIKE', "%{$v}%");
                    }
                });
            }
        });
        return $rootQuery;
    }

    /*
     * Layouts
     */

    public static function isActive($route)
    {
        $routes = explode(',', $route);
        $res = false;
        foreach ($routes as $r) {
            if (preg_match("/(^|,)($r)($|,|\.)/", Route::currentRouteName())) {
                $res = true;
                break;
            }
        }
        return $res ? 'active' : '';
    }

    public static function createSelect($data, $label, $id = 'id')
    {
        $res = array();
        foreach ($data as $v) {
            $tmp = false;
            $tmp = gettype($label) === 'object' && get_class($label) === 'Closure' ? $label($v) : $v->$label;
            $tmpId = gettype($id) === 'object' && get_class($id) === 'Closure' ? $id($v) : $v->$id;
            $res[$tmpId] = $tmp;
        }
        return $res;
    }

    public static function getAttributes($collections, $fields)
    {
        $res = [];
        foreach ($collections as $collection) {
            $tmp = [];
            foreach ($fields as $key => $value) {
                $key = is_int($key) ? $value : $key;
                if (is_string($value)) {
                    if (preg_match('/\./', $value)) {
                        $value = str_replace('.', '->', $value);
                        eval(sprintf('$value = $collection->%s;', $value));
                    } else {
                        $value = $collection->$value;
                    }
                } else {
                    $value = $value($collection);
                }

                $tmp[$key] = $value;
            }
            $res[] = $tmp;
        }
        return $res;
    }

    public static function link_to_route_auth($name, $title = null, $parameters = array(), $attributes = array())
    {
        return Auth::user() && Auth::user()->canCache($name) ? link_to_route($name, $title, $parameters, $attributes)->toHtml() : $title;
    }
    
    public static function getDataTablesLang()
    {
        return json_encode(['url'=> sprintf('%s/jquery.datatables/lang/%s.json', asset('vendor'), Config::get('app.locale'))]);
    }

    /*
     * File System
     */

    public static function deleteTree($dir)
    {
        if (file_exists($dir)) {
            $files = array_diff(scandir($dir), array('.', '..'));
            foreach ($files as $file) {
                (is_dir("$dir/$file")) ? self::deleteTree("$dir/$file") : unlink("$dir/$file");
            }
            return rmdir($dir);
        } else {
            return true;
        }
    }

    public static function isLinux()
    {
        return is_file('/proc/cpuinfo');
    }

    public static function isWindows()
    {
        return 'WIN' == strtoupper(substr(PHP_OS, 0, 3));
    }

    public static function physicalCoreCount()
    {
        $numCpus = 1;
        if (is_file('/proc/cpuinfo')) {
            $numCpus = (int) preg_replace('/[^\d]/', '', exec('cat /proc/cpuinfo | grep "cpu cores" | uniq'));
        } else if ('WIN' == strtoupper(substr(PHP_OS, 0, 3))) {
            exec('wmic cpu get NumberOfCores', $out);
            $numCpus = (int) $out[1];
        }

        return $numCpus;
    }

    public static function logicalCoreCount()
    {
        $numCpus = 1;
        if (is_file('/proc/cpuinfo')) {
            $numCpus = (int) preg_replace('/[^\d]/', '', exec('cat /proc/cpuinfo | grep "^processor"')) + 1;
        } else if ('WIN' == strtoupper(substr(PHP_OS, 0, 3))) {
            exec('wmic cpu get NumberOfLogicalProcessors', $out);
            $numCpus = (int) $out[1];
        }

        return $numCpus;
    }

    /*
     * Modules
     * nWidart/laravel-modules
     */
    public static function configGet($config)
    {        
        foreach (scandir( app_path('../modules') ) as $value) {
            if(in_array($value, ['.', '..']) || !is_dir(app_path('../modules/' . $value))) {
                continue;
            }
            $module = strtolower($value);
            if( config( "{$module}.master.{$config}") ) {
                return config( "{$module}.master.{$config}");
            }
        }
        return config($config);
    }
    
    /**
     * @param UploadedFile $gambar Gambar uploadedfile
     * @param string $path Path file
     * **/
    public static function uploadGambar(UploadedFile $gambar, $path)
    {
    	if(!$gambar) {
            return false;
    	} else {
            $filename = $gambar->getClientOriginalName();
            $directory = public_path($path); 
            if(!file_exists($directory)) {
                Storage::makeDirectory($path, $mode = 0777, true, true);
            }
            $gambar->move($path, $filename);

            return $path . $filename;
    	}
    	return false;
    }
}
