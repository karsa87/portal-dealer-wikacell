<?php

namespace Karsa;

use Session;

class Notification
{

    const TYPE_REGULAR = 1;
    const TYPE_SUCCESS = 2;
    const TYPE_DANGER = 3;
    const TYPE_INFO = 4;
    const TYPE_WARNING = 5;

    private static $ARRAY_TO_TYPE = array(
        self::TYPE_REGULAR => '',
        self::TYPE_SUCCESS => 'success',
        self::TYPE_DANGER => 'danger',
        self::TYPE_INFO => 'info',
        self::TYPE_WARNING => 'warning'
    );
    private static $message = NULL;

    public static function showGritter()
    {
        $result = '';
        self::getMessage();
        foreach (self::$message as $i => $v) {
            foreach ($v as $w) {
                $options = array_except($w, array('text'));
                if ($options) {
                    $options = ',' . json_encode($options);
                } else {
                    $options = '';
                }
                $w = (object) $w;
                $tmp = ucfirst(self::$ARRAY_TO_TYPE[$i]);
                $text = addslashes($w->text);
                $result .= "show{$tmp}Gritter('{$text}'{$options});";
            }
        }
        self::clear();
        return $result;
    }

    public static function clear()
    {
        Session::remove('_gritter');
        self::$message = NULL;
    }

    public static function getMessage($type = FALSE)
    {
        if (self::$message === NULL) {
            self::$message = Session::get('_gritter');
        }
        if (self::$message === NULL) {
            self::$message = array();
        }
        if ($type !== FALSE) {
            if (!isset(self::$message[$type])) {
                self::$message[$type] = array();
            }
        }
    }

    /**
     * 
     * @param int $type
     * @param string $message
     * @param mixed $options
     * 
     */
    public static function add($type, $message, $options = array())
    {
        self::getMessage($type);
        $options['text'] = $message;
        self::$message[$type][] = $options;
        Session::set('_gritter', self::$message);
    }

    /**
     * 
     * @param string $message
     * @param mixed $options
     */
    public static function addDanger($message, $options = array())
    {
        self::add(self::TYPE_DANGER, $message, $options);
    }

    /**
     * 
     * @param string $message
     * @param mixed $options
     */
    public static function addSuccess($message, $options = array())
    {
        self::add(self::TYPE_SUCCESS, $message, $options);
    }

    /**
     * 
     * @param string $message
     * @param mixed $options
     */
    public static function addInfo($message, $options = array())
    {
        self::add(self::TYPE_INFO, $message, $options);
    }

    /**
     * 
     * @param string $message
     * @param mixed $options
     */
    public static function addWarning($message, $options = array())
    {
        self::add(self::TYPE_WARNING, $message, $options);
    }

    /**
     * 
     * @param string $message
     * @param mixed $options
     */
    public static function addRegular($message, $options = array())
    {
        self::add(self::TYPE_REGULAR, $message, $options);
    }

}
