<?php

namespace App\Models;

use App\Component\Model;
use DB;
/**
 * @property integer $id 
 * @property integer $barang_id 
 * @property integer $cabang_id 
 * @property integer $stok 
 * @property integer $stok_masuk
 * @property integer $stok_keluar 
 * 
 * @property Barang $barang
 * @property Cabang $cabang 
 * **/

class Stok extends Model
{
    protected $table = 'stok';
    protected $fillable = ['barang_id','cabang_id','stok','stok_masuk','stok_keluar'];
    
    protected function rules() {
        return [
            'barang_id'=>['required'],
            'cabang_id'=>['required'],
            'stok'=>['required']
        ];
    }
    
    protected static function attributeNames() {
        return [
            'barang_id'=>'Barang',
            'cabang_id'=>'Cabang',
            'stok'=>'Stok'
        ];
    }
    
    public function barang(){
        return $this->belongsTo(Barang::class);
    }
    
    public function cabang(){
        return $this->belongsTo(Cabang::class);
    }
    
    public function delete() {
        if($this->stok_keluar > 0){
            $this->errors = ['Tidak bisa dihapus karena sudah dibuat transaksi'];
            return false;
        }
        
        return parent::delete();
    }
    
    public static function updateStok($cabang_id, $barang_id = [])
    {
        $queryStok = Stok::whereIn('cabang_id',[$cabang_id]);
        
        $beli = DB::table('pembelian_detail')
                ->select(DB::raw("SUM(jumlah) AS stok_masuk,0 AS stok_keluar"),'barang_id','cabang_id')
                ->join('pembelian','pembelian.id','=','pembelian_detail.pembelian_id')
                ->groupBy('barang_id')
                ->groupBy('cabang_id');
        $mutasiMasuk = DB::table('mutasi_barang_detail')
                ->select(DB::raw("SUM(jumlah) AS stok_masuk,0 AS stok_keluar"),
                        'barang_id',DB::raw('ke_cabang_id as cabang_id'))
                ->join('mutasi_barang','mutasi_barang.id','=','mutasi_barang_id')
                ->groupBy('barang_id')
                ->groupBy('cabang_id');
        $mutasiKeluar = DB::table('mutasi_barang_detail')
                ->select(DB::raw("0 AS stok_masuk, SUM(jumlah) AS stok_keluar"),
                        'barang_id',DB::raw('dari_cabang_id as cabang_id'))
                ->join('mutasi_barang','mutasi_barang.id','=','mutasi_barang_id')
                ->groupBy('barang_id')
                ->groupBy('cabang_id');
        $jual = DB::table('penjualan_detail')
                ->select(DB::raw("0 AS stok_masuk, SUM(jumlah) AS stok_keluar"),'barang_id','cabang_id')
                ->join('penjualan','penjualan.id','=','penjualan_detail.penjualan_id')
                ->groupBy('barang_id')
                ->groupBy('cabang_id');
        $returbeli = DB::table('retur_pembelian_detail')
                ->select(DB::raw("0 AS stok_masuk, SUM(jumlah) AS stok_keluar"),'barang_id','cabang_id')
                ->join('retur_pembelian','retur_pembelian.id','=','retur_pembelian_detail.retur_pembelian_id')
                ->groupBy('barang_id')
                ->groupBy('cabang_id');
        $returJual = DB::table('retur_penjualan_detail')
                ->select(DB::raw("SUM(jumlah) AS stok_masuk, 0 AS stok_keluar"),'barang_id','cabang_id')
                ->join('retur_penjualan','retur_penjualan.id','=','retur_penjualan_detail.retur_penjualan_id')
                ->groupBy('barang_id')
                ->groupBy('cabang_id');
        $tradein = DB::table('tradein')
                ->select(DB::raw("SUM(jumlah) AS stok_masuk, 0 AS stok_keluar"),'barang_id','cabang_id')
                ->groupBy('barang_id')
                ->groupBy('cabang_id');
        $tradeinTukar = DB::table('tradein')
                ->select(DB::raw("0 AS stok_masuk, SUM(jumlah) AS stok_keluar"),'barang_tukar_id as barang_id','cabang_id')
                ->groupBy('barang_id')
                ->groupBy('cabang_id');
        
        
        $sql = $beli->unionAll($mutasiMasuk)->unionAll($mutasiKeluar)
                ->unionAll($jual)->unionAll($returbeli)->unionAll($returJual)
                ->unionAll($tradein)->unionAll($tradeinTukar);
        
        $query = DB::table(DB::raw("({$sql->toSql()}) as A"))
                ->select(
                    DB::raw("SUM(A.stok_masuk) as stok_masuk, SUM(A.stok_keluar) as stok_keluar")
                    , 'A.barang_id', 'A.cabang_id'
                )
                ->groupBy('A.barang_id')
                ->groupBy('A.cabang_id');
        
        if(count($cabang_id) > 0){
            $query->whereIn('A.cabang_id',[$cabang_id]);
        }
        
        if(count($barang_id) > 0){
            $query->whereIn('A.barang_id',$barang_id);
            $queryStok->whereIn('barang_id',$barang_id);
        }
        
        $res = $query->get();
        
        $barang_ = $queryStok->get()->pluck('barang_id','id')->toArray();
        
        $create = [];
        $update = [];
        $valueUpdate = "";
        $barang__ = []; //barang in detected in stok
        foreach ($res as $v) {
            $barang__[] = $v->barang_id;
            $stok_masuk = $v->stok_masuk;
            $stok_keluar = $v->stok_keluar;
            $stok = $stok_masuk - $stok_keluar;
            
            if(!in_array($v->barang_id, $barang_)){
                $v->stok = $stok;
                $create[] = (array) $v;
            }else{
                $v->id = array_search($v->barang_id, $barang_);
                
                $valueUpdate .= sprintf(
                        '(%s,%s,%s,%s,%s,%s),', 
                        $v->id,
                        $v->barang_id, 
                        $v->cabang_id, 
                        $v->stok_masuk, 
                        $v->stok_keluar, 
                        $stok
                );
                $update[] = (array) $v;
            }
        }
        
        if(count($create) > 0){
            DB::table('stok')->insert($create);
        }
        
        if(count($update)){
            $valueUpdate = preg_replace('/(,)$/', '', $valueUpdate);
            $queryUpdate = sprintf(
                'INSERT INTO %s (id,barang_id,cabang_id,stok_masuk,stok_keluar,stok) VALUES %s ON DUPLICATE KEY UPDATE stok_masuk=VALUES(stok_masuk), stok_keluar=VALUES(stok_keluar), stok=VALUES(stok)', 
                'stok', 
                $valueUpdate
            );
            
            DB::insert($queryUpdate);
        }
        
        if(count($create) == 0 && count($update) == 0){
            $queryStok->update([
                'stok_masuk'=>0,
                'stok_keluar'=>0,
                'stok'=>0
            ]);
        }
        
        $diff = array_diff($barang_, $barang__); // different stok not found in $res will be set 0 because have not transaction
        if(count($diff) > 0){
            $queryStok = Stok::whereIn('cabang_id',[$cabang_id])
                    ->whereIn('barang_id',$diff);
            
            $queryStok->update([
                'stok_masuk'=>0,
                'stok_keluar'=>0,
                'stok'=>0
            ]);
        }
    }
}
