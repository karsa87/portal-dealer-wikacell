<?php

namespace App\Models;

use App\Component\Model;
use Karsa\Helper;

/**
 * @property string $no_promo 
 * @property string $nama 
 * @property string $keterangan 
 * @property string $start 
 * @property string $end 
 * @property string $image 
 * @property integer $is_show 
 * @property integer $status 
 * **/

class PortalPromo extends Model
{
    const IS_SHOW = 0;
    const IS_NOT_SHOW = 1;
    const STATUS_AKTIF = 0;
    const STATUS_NON_AKTIF = 1;
    
    public static $kodePrefix = "PP";

    protected $table    = 'portal_promo';
    protected $fillable = ['no_promo','nama','keterangan','start','end','is_show','status'];
    
    protected function rules()
    {
        return [
            'no_promo'  => ['required'],
            'nama'      => ['required'],
            'start'     => ['required'],
            'end'       => ['required'],
        ];
    }
    
    protected static function attributeNames()
    {
        return \Lang::get('portal_promo');
    }
    
    public function fill(array $attributes)
    {
        if(isset($attributes['start'])){
            $attributes['start'] = Helper::formatDate($attributes['start'], Helper::FORMAT_DATE_EN_SHORT);
        }
        
        if(isset($attributes['end'])){
            $attributes['end'] = Helper::formatDate($attributes['end'], Helper::FORMAT_DATE_EN_SHORT);
        }
        
        if(isset($attributes["image"])){
            $attributes["image"] = "[]";
        }
        
        return parent::fill($attributes);
    }
    
    public function lines()
    {
        return $this->hasMany(PortalPromoLine::class);
    }
    
    public static function getLastNoPromo()
    {
        $res = PortalPromo::
                select(\DB::raw('IFNULL( MAX( SUBSTRING(no_promo,' . strlen(self::$kodePrefix) . '+ 8) ), 0) + 1 max'))
                ->first()
                ->max;
        return sprintf('%s/%s%s', Helper::getTodayDate(Helper::FORMAT_DATE_EN_SHORTEST), self::$kodePrefix, str_pad($res, 6, "0", STR_PAD_LEFT) );
    }
    
    public function validateWithLine($input)
    {
        $res    = $this->validate();
        $errors = $this->errors;
        
        $lines = $input["line"];
        foreach($lines as $l){
            $row = new PortalPromoLine();
            $row->fill($l);
            
            if(!$row->validate()){
                $res    = false;
                $errors = array_merge($errors, $row->errors);
            }
        }
        
        $this->errors = $errors;
        
        return $res;
    }
    
    public function saveWithLine($input)
    {
        \DB::beginTransaction();
        $res = $this->save();
        
        $lines = $input["line"];
        foreach($lines as $id => $l){
            $row = PortalPromoLine::where('barang_id',$id)->first();
            $row = $row ? $row : new PortalPromoLine();
            $row->fill($l);
            $row->portal_promo_id = $this->id;
            
            if(!$row->saveWithBonus($l)){
                $res    = false;
                $errors = array_merge($errors, $row->errors);
            }
        }
        
        if($res){
            $this->lines()->whereNotIn("barang_id",  array_keys($lines))->delete();
        }
        
        $res ? \DB::commit() : \DB::rollBack();
        
        return $res;
    }
    
    public static function getPromo($where)
    {
        $model = PortalPromo::select('id','no_promo','nama')
                ->with([
                    'lines'=>function($q) use ($where){
                        $q->with('bonus')
                                ->where('barang_id',$where['barang_id'])
                                ->where('min_jumlah','<=',$where['jumlah']);
                    }
                ])
                ->where('start','<=',date('Y-m-d'))
                ->where('end','>=',date('Y-m-d'))
                ->first();
        
        $rs = [];
        if($model){
            $rs = [
                'id'    => $model->id,
                'nama'  => $model->nama
            ];

            foreach ($model->lines as $l){
                $rs['lines'][$l->barang_id] = [
                    "id"                => $l->id,
                    "min_jumlah"        => $l->min_jumlah,
                    "promo_line_id"     => $l->promo_line_id,
                    "jenis"             => $l->jenis,
                    "potongan_persen"   => $l->potongan_persen,
                    "potongan_ribuan"   => $l->potongan_ribuan,
                    "bonus"             => []
                ];

                foreach ($l->bonus as $b){
                    $rs['lines'][$l->barang_id]["bonus"][$b->id] = [
                        "nama"      => $b->nama,
                        "jumlah"    => $b->pivot->jumlah,
                        "promo_line_id" => $l->promo_line_id,
                    ];
                }
            }

            $rs = array_key_exists('lines', $rs) ? $rs : [];
        }
        
        return $rs;
    }
    
    public static function getShowList()
    {
        return Lookup::items(Lookup::PORTAL_PROMO_SHOW);
    }
    
    public function getShow()
    {
        return Lookup::item(Lookup::PORTAL_PROMO_SHOW, $this->is_show);
    }
}