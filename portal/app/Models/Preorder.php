<?php

namespace App\Models;

use App\Component\Model;
use Lang;
use Karsa\Helper;
use DB;

/**
 * @property string $no_order 
 * @property string $tanggal 
 * @property integer $member_card_id 
 * @property integer $jenis 
 * @property string $tanggal_tempo 
 * @property integer $bank_id 
 * @property double $total_awal 
 * @property double $total_potongan 
 * @property double $biaya_lain 
 * @property double $total_akhir 
 * @property integer $status 
 * @property integer $status_order
 * @property integer $penjualan_id 
 * @property double $saldo 
 * @property integer $use_saldo 
 * @property string $bukti 
 * 
 * @property Bank $bank 
 * @property Penjualan $penjualan 
 * @property MemberCard $memberCard 
 * **/

class Preorder extends Model 
{
    const JENIS_CASH = 10;
    const JENIS_TEMPO = 20;
    
    const STATUS_LUNAS = 10;
    const STATUS_BELUM_LUNAS = 20;
    
    const ORDER_PROSES = 10;
    const ORDER_MENUNGGU = 20;
    const ORDER_DIKIRIM = 30;
    const ORDER_SAMPAI = 40;
    const ORDER_SELESAI = 50;
    const ORDER_DIBATALKAN = 60;
    const ORDER_ON_CHART = 70;
    const ORDER_WAIT_KONFIRMASI = 80;
    
    const LINE_STATUS_NEW = 10;
    const LINE_STATUS_BONUS = 20;
    
    const USE_SALDO_YA = 10;
    const USE_SALDO_TIDAK = 20;

    public static $kodePrefix = 'PO';
    
    protected $table = "preorder";
    protected $fillable = ["no_order","member_card_id","jenis","tanggal_tempo","bank_id","biaya_lain",'bukti'];
    
    protected function rules()
    {
        $rules = [
            'no_order'=>['required','unique:'.$this->table.',id,'.$this->id],
            'bank_id'=>['required'],
            'tanggal_tempo'=>['required_if:jenis,'. self::JENIS_TEMPO],
        ];
        
        return $rules;
    }

    protected static function attributeNames()
    {
        return Lang::get('preorder');
    }
    
    public function detail()
    {
        return $this->belongsToMany(Barang::class, 'preorder_detail')->withPivot(['harga_jual', 'jumlah', 'total_awal','diskon','potongan','promo_line_id','total_potongan','total_akhir','status']);
    }
    
    public function bank()
    {
        return $this->belongsTo(Bank::class);
    }
    
    public function penjualan()
    {
        return $this->belongsTo(Penjualan::class);
    }
    
    public function memberCard()
    {
        return $this->belongsTo(MemberCard::class);
    }

    public function getStatus()
    {
        return Lookup::item(Lookup::PREORDER_STATUS, $this->status);
    }
    
    public  function getStatusBadge()
    {
        return sprintf('<span class="block" style="background:%s;font-size:10px;">%s</span>',$this->status==self::STATUS_LUNAS ? 'green':' red', $this->getStatus());
    }
    
    public function getJenis()
    {
        return Lookup::item(Lookup::PREORDER_JENIS, $this->jenis);
    }
    
    public  function getJenisBadge()
    {
        return sprintf('<span class="badge%s">%s</span>',$this->jenis==self::JENIS_CASH ? ' badge-success':'', $this->getJenis());
    }
    
    public static function getStatusOrderList()
    {
        return Lookup::items(Lookup::PREORDER_ORDER);
    }
    
    public function getStatusOrder()
    {
        return Lookup::item(Lookup::PREORDER_ORDER, $this->status_order);
    }
    
    public  function getStatusOrderBadge()
    {
        $color = "yellow";
        
        switch ($this->status_order){
            case self::ORDER_DIBATALKAN:
                $color = "#d34c4c"; //red
                break;
            case self::ORDER_DIKIRIM:
                $color = "#5990ea"; // blue
                break;
            case self::ORDER_MENUNGGU:
                $color = "#ccc14b"; //yellow
                break;
            case self::ORDER_PROSES:
                $color = "#ec75ff"; //purple
                break;
            case self::ORDER_SAMPAI:
                $color = "#ff8800"; //orange 
                break;
            case self::ORDER_SELESAI:
                $color = "#88c15d"; //green
                break;
            case self::ORDER_ON_CHART:
                $color = "#a36e20"; //brown
                break;
            case self::ORDER_WAIT_KONFIRMASI:
                $color = "#d34c4c"; //red
                break;
            default :
                $color = "yellow";
                break;
        }
        
        return sprintf('<span class="block" style="background:%s;font-size:10px;">%s</span>',$color, $this->getStatusOrder());
    }
    
    public function getLineStatus()
    {
        return Lookup::item(Lookup::PREORDER_LINE_STATUS, $this->detail->status);
    }
    
    public function getLineStatusBadge()
    {
        return sprintf('<span><font color="%s">%s</font></span>', ($this->detail->status == self::LINE_STATUS_BONUS) ? 'green' : 'blue', $this->getStatusOrder());
    }
    
    public function getUseSaldo()
    {
        return Lookup::item(Lookup::PREORDER_USE_SALDO, $this->use_saldo);
    }
    
    public static function getUseSaldoList(){
        return Lookup::items(Lookup::PREORDER_USE_SALDO);
    }

    public static function getLastNoOrder()
    {
        $res = Preorder::
                select(DB::raw('IFNULL( MAX( SUBSTRING(no_order,' . strlen(self::$kodePrefix) . '+ 8) ), 0) + 1 max'))
                ->first()
                ->max;
        return sprintf('%s/%s%s', Helper::getTodayDate(Helper::FORMAT_DATE_EN_SHORTEST), self::$kodePrefix, str_pad($res, 6, "0", STR_PAD_LEFT) );
    }
    
    public function fill(array $attributes)
    {
        if(empty($attributes['bank_id'])){
            $attributes['bank_id'] = null;
        }
        
        if(empty($attributes['penjualan_id'])){
            $attributes['penjualan_id'] = null;
        }
        
        if(isset($attributes['biaya_lain'])){
            $attributes['biaya_lain'] = preg_replace('/,/', '', $attributes['biaya_lain']);
        }
        
        if(isset($attributes['tanggal'])){
            $attributes['tanggal'] = Helper::formatDate($attributes['tanggal'], Helper::FORMAT_DATE_EN_SHORT);
        }
        
        if(isset($attributes['tanggal_tempo'])){
            $attributes['tanggal_tempo'] = Helper::formatDate($attributes['tanggal_tempo'], Helper::FORMAT_DATE_EN_SHORT);
        }
        
        if(empty($attributes['bukti'])){
            $attributes['bukti'] = json_encode([]);
        }
        
        return parent::fill($attributes);
    }
    
    public function payment()
    {
        $this->total_awal = $this->detail()->sum('total_awal');
        $this->total_potongan = $this->detail()->sum('total_potongan');
        $this->total_akhir = $this->detail()->sum('total_akhir');
        if($this->use_saldo == self::USE_SALDO_YA){
            $this->total_akhir -= $this->saldo;
        }
        if($this->jenis == self::JENIS_TEMPO){
            $this->status_order = self::ORDER_MENUNGGU;
        }else{
            $this->status_order = self::ORDER_WAIT_KONFIRMASI;
        }
        
        $this->save();
        
        if($this->use_saldo == self::USE_SALDO_YA){
            \Auth::user()->getMemberCard()->member->calculateSaldo();
        }
    }
}