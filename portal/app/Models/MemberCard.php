<?php

namespace App\Models;

use App\Component\Model;

/**
 * @property integer $id 
 * @property string $no_kartu 
 * @property double $saldo_piutang 
 * @property double $max_piutang
 * @property integer $jumlah_tempo 
 * @property double $poin 
 * @property integer $member_id 
 * @property string $jenis 
 * 
 * @property Member $member 
 * **/

class MemberCard extends Model
{
    const JENIS_UMUM = 10;
    const JENIS_RESELLER_1 = 20;
    const JENIS_RESELLER_2 = 30;
    const JENIS_INDEPENDENT_ACTOR = 40;
    const JENIS_LC = 50;

    protected $table = 'member_card';
    protected $fillable = ['no_kartu','saldo_piutang','max_piutang','jumlah_tempo','poin','member_id','jenis','kcp_id'];
    
    protected function rules() {
        return [
            'no_kartu'=>['required'],
            'member_id'=>[],
            'saldo_piutang'=>['numeric'],
            'max_piutang'=>['numeric','min:0'],
            'jumlah_tempo'=>['numeric','min:0'],
            'poin'=>['numeric'],
        ];
    }
    
    protected static function attributeNames() {
        return [
            'no_kartu'=>'No Kartu Member',
            'max_piutang'=>'Max Piutang',
            'saldo_piutang'=>'Saldo Piutang',
            'jumlah_tempo'=>'Jumlah Tempo',
            'poin'=>'Poin',
            'member_id'=>'Member',
            'jenis'=>'Jenis'
        ];
    }
    
    public function member(){
        return $this->belongsTo(Member::class);
    }
    
    public function penjualan()
    {
        return $this->hasMany(Penjualan::class);
    }
    
    public function getJenis()
    {
        return Lookup::item(Lookup::JENIS_MEMBER_CARD, $this->jenis);
    }
    
    public function getJenisHarga()
    {
        $result = $this->jenis;
        
        if($this->jenis == self::JENIS_INDEPENDENT_ACTOR
                || $this->jenis == self::JENIS_LC){
            $result = self::JENIS_UMUM;
        }
        
        return $result;
    }
    
    public static function getJenisList(){
        return Lookup::items(Lookup::JENIS_MEMBER_CARD);
    }
    
    public function getJenisBadge()
    {
        return sprintf('<span class="badge%s">%s</span>',$this->jenis==self::JENIS_UMUM?' badge-success':'', $this->getJenis());
    }
    
    public function fill(array $attributes) {
        if(isset($attributes['max_piutang'])){
            $attributes['max_piutang'] = preg_replace('/,/', '', $attributes['max_piutang']);
        }
        
        return parent::fill($attributes);
    }
    
    public function getPiutang()
    {
        $rs = $this->penjualan()->where("status", Penjualan::STATUS_BELUM_LUNAS)
                ->get();
        
        return $rs;
    }
    
    public function getTotalPiutang()
    {
        $rs = $this->penjualan()
                ->select(\DB::raw('IFNULL(SUM(penjualan.total_akhir),0) as total_akhir'),
                        \DB::raw('IFNULL(SUM(penjualan.terbayar),0) as terbayar'))
                ->where("status", Penjualan::STATUS_BELUM_LUNAS)
                ->first();
        
        return $rs->total_akhir - $rs->terbayar;
    }
}
