<?php

namespace App\Models;

use App\Component\Model;

/**
 * @property integer $id 
 * @property string $kode 
 * @property string $name 
 * @property integer $status 
 * **/

class Brand extends Model
{
    const STATUS_AKTIF = 1;
    const STATUS_NON_AKTIF = 0;

    protected $table = 'brand';
    protected $fillable = ['kode','nama','status'];
    
    public static $kodePrefix = 'B-';
    
    protected function rules()
    {
        return [
            'nama' => ['required'],
        ];
    }
    
    protected static function attributeNames()
    {
        return [
            'nama'  => 'Nama'
        ];
    }
    
    public static function getLastKode()
    {
        $res = Brand::
                select(\DB::raw('IFNULL( MAX( SUBSTRING(kode,' . strlen(self::$kodePrefix) . '+ 4) ), 0) + 1 max'))
                ->first()
                ->max;
        return sprintf('%s%s', self::$kodePrefix, str_pad($res, 4, "0", STR_PAD_LEFT) );
    }
    
    public function getStatusBadge()
    {
        return sprintf('<span class="badge%s">%s</span>',$this->status==self::STATUS_AKTIF?' badge-success':'', $this->getStatus());
    }
    
    public static function getStatusList()
    {
        return Lookup::items(Lookup::STATUS_BRAND);
    }
    
    public function getStatus()
    {
        return Lookup::item(Lookup::STATUS_BRAND, $this->status);
    }
}
