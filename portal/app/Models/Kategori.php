<?php

namespace App\Models;

use App\Component\Model;

/**
 * @property integer $id
 * @property string $kode
 * @property string $nama
 * @property integer $jenis
 */

class Kategori extends Model 
{
    const JENIS_ELEKTRONIK = 5;
    const JENIS_NONELEKTRONIK = 10;
    
    protected $table = 'kategori';
    protected $fillable = ['kode','nama','jenis'];
    
    public static $kodePrefix = 'K';
    
    protected function rules() {
        return [
            'kode'=>['required','unique:kategori,id,'.$this->id],
            'nama'=>['required'],
            'jenis'=>['required'],
        ];
    }
    
    protected static function attributeNames() {
        return [
            'kode'=>'Kode',
            'nama'=>'Nama Kategori',
        ];
    }
    
    public static function getLastKode()
    {
        $res = Kategori::
                select(\DB::raw('IFNULL( MAX( SUBSTRING(kode,' . strlen(self::$kodePrefix) . '+ 4) ), 0) + 1 max'))
                ->first()
                ->max;
        return sprintf('%s%s', self::$kodePrefix, str_pad($res, 4, "0", STR_PAD_LEFT) );
    }
    
    public static function getJenisList()
    {
        return Lookup::items(Lookup::JENIS_KATEGORI);
    }
    
    public function getJenis()
    {
        return Lookup::item(Lookup::JENIS_KATEGORI, $this->jenis);
    }
}