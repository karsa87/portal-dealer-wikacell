<?php

namespace App\Models;

use App\Component\Model;
use DB;
/**
 * @property integer $id
 * @property string $no_rek
 * @property string nama
 * @property string $nama_pemilik 
 * @property double saldo
 * @property double saldo_masuk
 * @property double saldo_keluar
 * @property double saldo_awal
 * @property string status
 * *****/

class Bank extends Model
{
    const BANK_KAS_BESAR = 1;
    const STATUS_AKTIF = 'Aktif';
    const STATUS_TIDAK_AKTIF = 'Tidak Aktif';

    protected $table = 'bank';
    protected $fillable = ['no_rek','nama','saldo','status','saldo_keluar','saldo_masuk', 'saldo_awal', 'nama_pemilik'];
    
    protected function rules() {
        return [
            'no_rek' => ['required','numeric'],
            'nama' => ['required'],
            'nama_pemilik' => ['required'],
            'saldo' => [],
            'status' => [],
            'saldo_awal'=>['numeric','min:0']
        ];
    }
    
    protected static function attributeNames() {
        return [
            'no_rek' => 'No Rekening',
            'nama' => 'Nama Bank',
            'nama_pemilik' => 'Nama Pemilik',
            'saldo' => 'Saldo',
            'status' => 'Status',
            'saldo_awal'=>'Saldo Awal',
            'saldo_keluar'=>'Pengeluaran',
            'saldo_masuk'=>'Pemasukkan'
        ];
    }
    
    public static function getStatusList(){
        return [
            self::STATUS_AKTIF => self::STATUS_AKTIF,
            self::STATUS_TIDAK_AKTIF => self::STATUS_TIDAK_AKTIF,
        ];
    }
    
    public function getStatusBadge()
    {
        return sprintf('<span class="badge%s">%s</span>',$this->status==self::STATUS_AKTIF?' badge-success':'', $this->status);
    }
    
    public function validate($rules = array()) {
        $this->saldo_awal = preg_replace('/,/', '', $this->saldo_awal);
        
        return parent::validate($rules);
    }
    
    public function save(array $options = array()) {
        if(!$this->exists){
            $this->saldo = 0;
        }
        
        return parent::save($options);
    }
    
    public function delete()
    {
        if($this->id == self::BANK_KAS_BESAR){
            return false;
        }else{
            return parent::delete();
        }
    }
    
    public function updateSaldo()
    {
        if($this->id == self::BANK_KAS_BESAR){
            self::updateKasBesar();
            return;
        }
        $beli = DB::table('pembelian')
                ->select(DB::raw("SUM(uang_muka) AS saldo_keluar, 0 AS saldo_masuk"))
                ->whereBankId($this->id)
                ->where('jenis','=',  Pembelian::JENIS_CASH);
        $mutasiKeluar = DB::table('mutasi_bank')
                ->select(DB::raw("SUM(saldo_keluar) AS saldo_keluar , 0 AS saldo_masuk"))
                ->where('dari_bank_id','=',$this->id);
        $mutasiMasuk = DB::table('mutasi_bank')
                ->select(DB::raw("0 AS saldo_keluar, SUM(saldo_keluar) AS saldo_masuk"))
                ->where('ke_bank_id','=',$this->id);
        $jual = DB::table('penjualan')
                ->select(DB::raw("0 AS saldo_keluar, SUM(uang_muka) AS saldo_masuk"))
                ->whereBankId($this->id)
                ->whereNotIn('jenis',[Penjualan::JENIS_CASH, Penjualan::JENIS_TEMPO]);
        $returbeli = DB::table('retur_pembelian')
                ->select(DB::raw("0 AS saldo_keluar, SUM(total_akhir) AS saldo_masuk"))
                ->whereBankId($this->id);
        $returjual = DB::table('retur_penjualan')
                ->select(DB::raw("SUM(total_akhir) AS saldo_keluar, 0 AS saldo_masuk"))
                ->whereBankId($this->id);
        $tradein = DB::table('tradein')
                ->select(DB::raw("SUM(total_akhir) AS saldo_keluar, 0 AS saldo_masuk"))
                ->where('perlakuan', '=', Tradein::PERLAKUAN_JUAL)
                ->whereBankId($this->id);
        $pembayaranHutPembelian = DB::table('pembayaran_hutang_pembelian')
                ->select(DB::raw("SUM(bayar) AS saldo_keluar, 0 AS saldo_masuk"))
                ->whereBankId($this->id);
        $pembayaranHutPenjualan = DB::table('pembayaran_hutang_penjualan')
                ->select(DB::raw("0 AS saldo_keluar, SUM(bayar) AS saldo_masuk"))
                ->whereBankId($this->id);
        $penerimaan_penjualan = DB::table('penerimaan_penjualan')
                ->select(DB::raw("0 AS saldo_keluar, SUM(uang) AS saldo_masuk"))
                ->whereBankId($this->id);
        $service = DB::table('service')
                ->select(DB::raw("0 AS saldo_keluar, SUM(biaya) AS saldo_masuk"))
                ->where('status','=',  Service::STATUS_AMBIL)
                ->whereBankId($this->id);
        $pengeluaran = DB::table('pengeluaran')
                ->select(DB::raw("SUM(biaya) AS saldo_keluar, 0 AS saldo_masuk"))
                ->whereBankId($this->id);
        $bankSetor_penambahan = DB::table('bank_setor')
                ->select(DB::raw("0 AS saldo_keluar, SUM(jumlah) AS saldo_masuk"))
                ->whereIn('jenis', [BankSetor::JENIS_PENAMBAHAN_MODAL, BankSetor::JENIS_SETOR_TUNAI_BANK])
                ->whereBankId($this->id);
        $bankSetor_pengurangan = DB::table('bank_setor')
                ->select(DB::raw("SUM(jumlah) AS saldo_keluar, 0 AS saldo_masuk"))
                ->whereIn('jenis', [BankSetor::JENIS_TRANSFER_BANK, BankSetor::JENIS_PENGAMBILAN_UANG])
                ->whereBankId($this->id);
        $insentive = DB::table('insentive')
                ->select(DB::raw("0 AS saldo_keluar, SUM(jumlah) AS saldo_masuk"))
                ->whereBankId($this->id);
        
        $res = $beli->unionAll($mutasiKeluar)->unionAll($mutasiMasuk)->unionAll($jual)->unionAll($returbeli)
                ->unionAll($returjual)->unionAll($tradein)->unionAll($pembayaranHutPembelian)
                ->unionAll($penerimaan_penjualan)->unionAll($service)->unionAll($pengeluaran)
                ->unionAll($bankSetor_penambahan)->unionAll($bankSetor_pengurangan)
                ->unionAll($pembayaranHutPenjualan)->unionAll($insentive)->get();
        
        $saldo_keluar = $saldo_masuk = 0;
        
        foreach ($res as $v) {
            $saldo_keluar += $v->saldo_keluar;
            $saldo_masuk += $v->saldo_masuk;
        }
        
        $saldoNow = $saldo_masuk - $saldo_keluar;

        $this->saldo = $saldoNow + $this->saldo_awal;
        $this->saldo_masuk = $saldo_masuk;
        $this->saldo_keluar = $saldo_keluar;
        
        $this->save();
    }
    
    public static function updateKasBesar()
    {
        $beli = DB::table('pembelian')
                ->select(DB::raw("SUM(uang_muka) AS saldo_keluar, 0 AS saldo_masuk"))
                ->whereIn('jenis',[Pembelian::JENIS_CASH, Pembelian::JENIS_TEMPO])
                ->where(function($q){
                    $q->whereNull('bank_id');
                    $q->orWhere('bank_id','=',  self::BANK_KAS_BESAR);
                });
        $jual = DB::table('penjualan')
                ->select(DB::raw("0 AS saldo_keluar, SUM(uang_muka) AS saldo_masuk"))
                ->whereIn('jenis',[Penjualan::JENIS_CASH, Penjualan::JENIS_TEMPO])
                ->where(function($q){
                    $q->whereNull('bank_id');
                    $q->orWhere('bank_id','=',  self::BANK_KAS_BESAR);
                });
        $returbeli = DB::table('retur_pembelian')
                ->select(DB::raw("0 AS saldo_keluar, SUM(total_akhir) AS saldo_masuk"))
                ->where(function($q){
                    $q->whereNull('bank_id');
                    $q->orWhere('bank_id','=',  self::BANK_KAS_BESAR);
                });
        $returjual = DB::table('retur_penjualan')
                ->select(DB::raw("SUM(total_akhir) AS saldo_keluar, 0 AS saldo_masuk"))
                ->where(function($q){
                    $q->whereNull('bank_id');
                    $q->orWhere('bank_id','=',  self::BANK_KAS_BESAR);
                });
        $tradein = DB::table('tradein')
                ->select(DB::raw("SUM(total_akhir) AS saldo_keluar, 0 AS saldo_masuk"))
                ->where('perlakuan', '=', Tradein::PERLAKUAN_JUAL)
                ->where(function($q){
                    $q->whereNull('bank_id');
                    $q->orWhere('bank_id','=',  self::BANK_KAS_BESAR);
                });
        $pembayaranHutPembelian = DB::table('pembayaran_hutang_pembelian')
                ->select(DB::raw("SUM(bayar) AS saldo_keluar, 0 AS saldo_masuk"))
                ->where(function($q){
                    $q->whereNull('bank_id');
                    $q->orWhere('bank_id','=',  self::BANK_KAS_BESAR);
                });
        $pembayaranHutPenjualan = DB::table('pembayaran_hutang_penjualan')
                ->select(DB::raw("0 AS saldo_keluar, SUM(bayar) AS saldo_masuk"))
                ->where(function($q){
                    $q->whereNull('bank_id');
                    $q->orWhere('bank_id','=',  self::BANK_KAS_BESAR);
                });
        $service = DB::table('service')
                ->select(DB::raw("0 AS saldo_keluar, SUM(biaya) AS saldo_masuk"))
                ->where('status','=',  Service::STATUS_AMBIL)
                ->where(function($q){
                    $q->whereNull('bank_id');
                    $q->orWhere('bank_id','=',  self::BANK_KAS_BESAR);
                });
        $pengeluaran = DB::table('pengeluaran')
                ->select(DB::raw("SUM(biaya) AS saldo_keluar, 0 AS saldo_masuk"))
                ->where(function($q){
                    $q->whereNull('bank_id');
                    $q->orWhere('bank_id','=',  self::BANK_KAS_BESAR);
                });
        
        $res = $beli->unionAll($pembayaranHutPenjualan)->unionAll($jual)->unionAll($returbeli)
                ->unionAll($returjual)->unionAll($tradein)->unionAll($pembayaranHutPembelian)
                ->unionAll($service)->unionAll($pengeluaran)->get();
        
        $saldo_keluar = $saldo_masuk = 0;
        
        foreach ($res as $v) {
            $saldo_keluar += $v->saldo_keluar;
            $saldo_masuk += $v->saldo_masuk;
        }
        
        $saldoNow = $saldo_masuk - $saldo_keluar;

        $bank = self::find(self::BANK_KAS_BESAR);
        $bank->saldo = $saldoNow + $bank->saldo_awal;
        $bank->saldo_masuk = $saldo_masuk;
        $bank->saldo_keluar = $saldo_keluar;
        
        $bank->save();
    }
}
