<?php

namespace App\Models;

use App\Component\Model;
use App\Models\Barang;
/**
 * @property integer $pembelian_id
 * @property integer $barang_id 
 * @property string $serial_number 
 * @property string $imei 
 * @property integer $is_use 
 * 
 * @property Barang $barang 
 * @property Pembelian $pembelian 
 *****/
class BarangCode extends Model
{
    const IS_USE_TERJUAL = 1;
    const IS_USE_BELUM_TERJUAL = 0;
    const IS_USE_RETUR = 99;

    protected $table = 'barang_code';
    protected $fillable = ['barang_id','serial_number','imei','pembelian_id'];
    
    protected static function attributeNames() {
        return [
            'is_use'=>'Status'
        ];
    }
    
    public function barangHarga()
    {
        return $this->hasMany(BarangHarga::class);
    }

    public function barang(){
        return $this->belongsTo(Barang::class);
    }
    
    public function pembelian(){
        return $this->belongsTo(Pembelian::class);
    }
    
    public function getHargaReseller1()
    {
        $barangCode = null;
        if($this->barang->kategori_id && $this->barang->kategori->jenis != Kategori::JENIS_NONELEKTRONIK){
            $barangCode = BarangHarga::join('barang_code','barang_code.id','=','barang_harga.barang_code_id')
                ->where('barang_id','=',$this->barang_id)->where('jenis','=',  BarangHarga::JENIS_RESELLER_1)->first();
        }else{
            $barangCode = $this->barangHarga()->where('jenis','=',  BarangHarga::JENIS_RESELLER_1)->first();
        }
        $harga = $barangCode;
        
        return $harga ? $harga : null;
    }
    
    public function getHargaReseller2()
    {
        $barangCode = null;
        if($this->barang->kategori_id && $this->barang->kategori->jenis != Kategori::JENIS_NONELEKTRONIK){
            $barangCode = BarangHarga::join('barang_code','barang_code.id','=','barang_harga.barang_code_id')
                ->where('barang_id','=',$this->barang_id)->where('jenis','=',  BarangHarga::JENIS_RESELLER_2)->first();
        }else{
            $barangCode = $this->barangHarga()->where('jenis','=',  BarangHarga::JENIS_RESELLER_2)->first();
        }
        $harga = $barangCode;
        
        return $harga ? $harga : null;
    }
    
    public function getHargaSRP()
    {
        $barangCode = null;
        if($this->barang->kategori_id && $this->barang->kategori->jenis != Kategori::JENIS_NONELEKTRONIK){
            $barangCode = BarangHarga::join('barang_code','barang_code.id','=','barang_harga.barang_code_id')
                ->where('barang_id','=',$this->barang_id)->where('jenis','=',  BarangHarga::JENIS_UMUM)->first();
        }else{
            $barangCode = $this->barangHarga()->where('jenis','=',  BarangHarga::JENIS_UMUM)->first();
        }
        $harga = $barangCode;
        
        return $harga ? $harga : null;
    }
    
    public function getStatusBadge()
    {
        return sprintf('<span class="badge%s">%s</span>',$this->is_use==self::IS_USE_BELUM_TERJUAL?' badge-success':' badge-danger', $this->is_use ? 'Terjual' : 'Belum Terjual');
    }
    
    public static function getAvgHarga($barang_id){
        $rs = BarangCode::join('barang_harga as bh', function ($join) {
                        $join->on("bh.barang_code_id","=","barang_code.id")
                                ->where("bh.jenis","=",  BarangHarga::JENIS_RESELLER_1);
                    })
                    ->where("is_use", self::IS_USE_BELUM_TERJUAL)
                    ->where("barang_id",$barang_id)
                    ->groupBy("barang_id")
                    ->avg("bh.harga");
                    
        return $rs;
    }
}