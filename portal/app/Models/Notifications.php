<?php

namespace App\Models;

use App\Component\Model;

/**
 * @property integer $id
 * @property string $from
 * @property string $to
 * @property string $message
 * @property string $route
 * @property string $parameter 
 * @property integer $is_read
 * 
 * @property User fromUser
 * @property User toUser
 */

class Notifications extends Model 
{
    const IS_READ = 1;
    const IS_NOT_READ = 0;

    protected $table = 'notification';
    protected $fillable = ['from','to','message','route','is_read','parameter'];
    
    protected function rules() {
        return [];
    }
    
    protected static function attributeNames() {
        return [
            'from'      => 'From',
            'to'        => 'To',
            'message'   => 'Message',
            'route'     => 'Route',
        ];
    }
    
    public function userFrom()
    {
        if(is_numeric($this->from)){
            return $this->belongsTo(User::class);
        }else{
            return $this->from;
        }
    }
    
    public function userTo()
    {
        if(is_numeric($this->to)){
            return $this->belongsTo(User::class);
        }else{
            return $this->to;
        }
    }
    
    public static function addNotification($from,$to,$message,$route,$parameter=[])
    {
        Notifications::create([
            'from'      => $from,
            'to'        => $to,
            'message'   => $message,
            'route'     => $route,
            'parameter' => json_encode($parameter)
        ]);
        
        return true;
    }
    
    public static function getNotifications($where)
    {
        return Notifications::where($where)->get()->toArray();
    }

    public function read(){
        $this->is_read = self::IS_READ;
        return $this->save();
    }
    
    public static function changeRead($where){
        return self::where($where)->update(['is_read'=>self::IS_READ]);
    }
}