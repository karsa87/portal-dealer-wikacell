<?php

namespace App\Models;

use App\Component\Model;
use App\Models\Penjualan;

/**
 * @property integer $id 
 * @property integer $penjualan_id 
 * @property integer $barang_id
 * @property integer $barang_code_id  
 * @property integer $jumlah 
 * @property double $harga_pokok 
 * @property double $harga_jual 
 * @property double $harga_akhir 
 * @property integer $promo_id
 * @property double $potongan 
 * 
 * @property Penjualan $penjualan 
 * @property Barang $barang 
 * @property BarangCode $barangCode 
 * @property Promo $promo 
 * **/

class PenjualanDetail extends Model
{
    protected $table = 'penjualan_detail';
    protected $fillable = ['penjualan_id','barang_id','barang_code_id','jumlah','harga_pokok','harga_jual','harga_akhir','potongan','promo_id'];
    
    protected function rules() {
        return [
            'penjualan_id'=>['required'],
            'barang_id'=>['required'],
            'barang_code_id'=>['required'],
            'jumlah'=>['required'],
            'harga_jual'=>['required']
        ];
    }
    
    protected static function attributeNames() {
        return [
            'penjualan_id'=>'Penjualan',
            'barang_id'=>'Barang',
            'jumlah'=>'Jumlah',
            'harga_jual'=>'Harga Jual',
            'harga_akhir'=>'Harga Akhir',
            'promo_id'=>'Promo',
            'potongan'=>'Discount (%)',
        ];
    }
    
    public function barang(){
        return $this->belongsTo(Barang::class);
    }
    
    public function barangCode(){
        return $this->belongsTo(BarangCode::class);
    }

    public function pembelian(){
        return $this->belongsTo(Pembelian::class);
    }
}
