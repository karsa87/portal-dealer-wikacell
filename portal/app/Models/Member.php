<?php

namespace App\Models;

use App\Component\Model;
use Zain\Helper;
use DB;

/**
 * @property integer $id 
 * @property string $nama 
 * @property string $alamat 
 * @property string $telepon 
 * @property string $tgl_lahir 
 * @property string $tempat_lahir
 * @property integer $jenis_id
 * @property string $no_id 
 * @property integer $user_id 
 * @property integer $jenis_member 
 * @property double $saldo 
 * 
 * @property User $user 
 * **/

class Member extends Model
{
    const JENIS_ID_KP = 5;
    const JENIS_ID_KTP = 10;
    const JENIS_ID_SIM = 15;
    
    const JENIS_MEMBER = 10;
    const JENIS_DEALER = 20;

    protected $table = 'member';
    protected $fillable = ['nama','alamat','telepon','tgl_lahir','tempat_lahir','jenis_id','no_id','kcp_id','jenis_member','saldo'];
    
    protected function rules() {
        return [
            'nama'=>['required'],
            'jenis_id'=>['required'],
            'no_id'=>['required'],
            'telepon'=>['required'],
            'alamat'=>['required'],
        ];
    }
    
    protected static function attributeNames() {
        return [
            'nama'=>'Nama Lengkap',
            'alamat'=>'Alamat Lengkap',
            'telepon'=>'Telepon',
            'tgl_lahir'=>'Tanggal Lahir',
            'tempat_lahir'=>'Tempat Lahir',
            'member_card'=>'Kartu Member Detail',
            'jenis_id'=>'Jenis Identitas',
            'no_id'=>'No Identitas',
            'user_id'=>'User',
        ];
    }
    
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    
    public function fill(array $attributes)
    {
        if(isset($attributes['tgl_lahir'])){
            $attributes['tgl_lahir'] = Helper::formatDate($attributes['tgl_lahir'], Helper::FORMAT_DATE_EN_SHORT);
        }
        
        return parent::fill($attributes);
    }
    
    public function memberCards(){
        return $this->hasOne(MemberCard::class);
    }
    
    public function memberSaldo()
    {
        return $this->hasMany(MemberSaldo::class);
    }

    public function validateWithMemberCard(array $input){
        $res = $this->validate();
        $errors = $this->errors;
        
        $memberCard = new MemberCard();
        $memberCard->fill($input);
        
        if(!$memberCard->validate()){
            $errors = array_merge($errors, $memberCard->errors);
            $res = false;
        }
        
        $this->errors = $errors;
        
        return $res;
    }
    
    public function saveWithMemberCard(array $input)
    {
        DB::beginTransaction();
        
        $res = $this->validate();
        $errors = $this->errors;
        
        if($res && $this->save() && $this->saveUser($input)){
            $memberCard = (count($this->memberCards) > 0) ? $this->memberCards : new MemberCard();
            $memberCard->fill($input);
            $memberCard->member_id = $this->id;
            
            $res = $memberCard->validate();
            
            if(!($res && $memberCard->save())){
                $res = false;
                $errors = array_merge($errors, $memberCard->errors);
            }
        }else{
            $res = false;
            $errors = array_merge($errors, $this->errors);
        }
        
        $res ? DB::commit() : DB::rollback();
        
        $this->errors = $errors;
        
        return $res;
    }
    
    public function saveUser(array $input)
    {
        if($this->jenis_member == self::JENIS_DEALER){
            $user = new User();
            if($this->user){
                $user  = $this->user;
            }
            
            $user->nama     = $this->nama;
            $user->username = $input["username"];
            $user->level    = User::LEVEL_MEMBER_DEALER;
            $user->status   = User::STATUS_AKTIF;
            $user->password = $input["password"];
            
            if($user->validate()){
                $user->save();
                
                $this->user_id = $user->id;
                $this->save();
                return TRUE;
            }else{
                $this->errors = $user->errors;
                return FALSE;
            }
        }else{
            if($this->user){
                $this->user_id = null;
                $this->user->delete();
            }
            
            return TRUE;
        }
    }

    public function delete()
    {
        DB::beginTransaction();
        $res = true;
        
        $res = $this->memberCards->delete();
        
        if($res){
            $res = parent::delete();
        }
                
        $res ? DB::commit() : DB::rollback();
        
        return $res;
    }
    
    public static function getJenisIDList() 
    {
        return Lookup::items(Lookup::JENIS_ID_MEMBER);
    }
    
    public function getJenisId()
    {
        return Lookup::item(Lookup::JENIS_ID_MEMBER, $this->jenis_id);
    }
    
    public static function getJenisMemberList() 
    {
        return Lookup::items(Lookup::JENIS_MEMBER);
    }
    
    public static function getJenisMember() 
    {
        return Lookup::item(Lookup::JENIS_MEMBER, $this->jenis_member);
    }
    
    public function calculateSaldo(){
        $member_card_id = $this->memberCards->id;
        
        $q1 = Preorder::select(\DB::raw("SUM(preorder.saldo) as saldo_total"))
                ->where('preorder.member_card_id',$member_card_id)
                ->whereNull("penjualan_id");
        
        $q3 = Penjualan::select(\DB::raw("SUM(saldo_member) as saldo_total"))
                ->where('member_card_id',$member_card_id)
                ->groupBy('member_card_id');
        
        $saldo_min = $q1->unionAll($q3)->get()->sum("saldo_total");
        $saldo_min = is_numeric($saldo_min) ? $saldo_min : 0;
        
        
        $saldo_add = Preorder::where('preorder.member_card_id',$member_card_id)
                ->where('status_order',  Preorder::ORDER_DIBATALKAN)
                ->sum("preorder.saldo");
        $saldo_add = is_numeric($saldo_add) ? $saldo_add : 0;
        
        $member_saldo = $this->memberSaldo()->sum('saldo');
        
        $saldo = $member_saldo + $saldo_add - $saldo_min; 
        
        $this->saldo = $saldo;
        
        return $this->save();
    }
}
