<?php

namespace App\Models;

use App\Component\Model;

/**
 * @property integer $id 
 * @property integer $barang_code_id 
 * @property string $jenis 
 * @property double $harga 
 * @property double $harga_beli 
 * @property string $status 
 * 
 * @property BarangCode $barangCode 
 * **/

class BarangHarga extends Model
{
    const JENIS_UMUM = 10;
    const JENIS_RESELLER_1 = 20;
    const JENIS_RESELLER_2 = 30;
    
    protected $table = 'barang_harga';
    protected $fillable = ['barang_id','jenis','harga','status','harga_beli'];
    
    protected function rules() {
        return [
            'jenis'=>['required'],
            'harga'=>['required']
        ];
    }
    
    protected static function attributeNames() {
        return [
            'barang_id'=>'Barang',
            'jenis'=>'Jenis',
            'harga'=>'Harga Umum',
            'status'=>'Status',
            'serial_number'=>'Serial Number',
            'imei'=>'Serial Number / IMEI / Kode',
            'barcode'=>'Barcode',
            'harga_umum'=>'SRP',
            'harga_reseller_1'=>'Reseller 1',
            'harga_reseller_2'=>'Reseller 2',
            'harga_beli'=>'Harga Beli'
        ];
    }
    
    public function barangCode(){
        return $this->belongsTo(BarangCode::class);
    }
    
    public function save(array $options = array()) {
        if(empty($this->harga)){
            $this->harga = 0;
        }
        $this->harga = preg_replace('/,/', '', $this->harga);
        $this->harga_beli = preg_replace('/,/', '', $this->harga_beli);
        
        return parent::save($options);
    }
    
    public function getJenis()
    {
        return Lookup::item(Lookup::JENIS_BARANG_HARGA, $this->jenis);
    }
    
    public function saveAll(array $input)
    {
        $res = true;
        foreach ($input['harga'] as $k => $v){
            $res = BarangHarga::where('barang_code_id','=',  $this->barang_code_id)->where('jenis','=',$k)->update(['harga'=>preg_replace('/,/', '', $v)]);
        }
        
        return $res;
    }
}
