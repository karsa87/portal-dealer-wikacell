<?php

namespace App\Models;

use Hash;
use Illuminate\Database\Eloquent\Builder;
use DB;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Illuminate\Support\Facades\Storage;

/**
 * @property integer $id 
 * @property string $nama 
 * @property string $username 
 * @property string $password 
 * @property string $level 
 * @property string $status 
 * @property string $api_token 
 * @property integer $cabang_id
 * @property string $last_login 
 * @property string $foto 
 * 
 * @property Cabang $cabang 
 **/

class User extends \App\Component\User{
    const USER_DEVELOPER = 1;

    protected $table = 'user';
    protected $fillable = ['nama','username','status','password','level','cabang_id','foto'];
    protected $hidden = ['password', 'remember_token','api_token'];

    const LEVEL_DEVELOPER = 0;
    const LEVEL_SUPERUSER = 1;
    const LEVEL_ADMIN = 5;
    const LEVEL_SUPERVISOR = 10;
    const LEVEL_KARYAWAN = 15;
    const LEVEL_TEKNISI = 20;
    const LEVEL_KASIER = 25;
    const LEVEL_MEMBER_DEALER = 30;
    const LEVEL_MEMBER = 35;

    const STATUS_AKTIF = "Aktif";
    const STATUS_NON_AKTIF = "Tidak Aktif";

    protected static function abilities() {
        return [
            self::LEVEL_DEVELOPER => [
                '*'
            ],
            self::LEVEL_SUPERUSER => [
                '*'
            ],
            self::LEVEL_MEMBER_DEALER => [
                '*'
            ]
        ];
    }
    
    protected function rules() {
        $rules = [
            'username'=>['required', "unique:{$this->table},username,{$this->id}"],
            'password'=>['required', 'min:3'],
            'level'=>[],
            'nama'=>['required'],
            'status'=>['required', 'in:' . implode(',', self::getStatusList()) ],
        ];
            
        if($this->exists) {
            unset( $rules['username'][0] );
            unset( $rules['password'][0] );
        }
        
        return $rules;
    }
    
    public static function attributeNames()
    {
        return [
            'nama' => 'Nama',
            'username' => 'Username',
            'password' => 'Password',
            'level' => 'Level',
            'status' => 'Status',
            'remember_token' => 'Remember Token',
            'cabang_id' => 'Default Cabang',
            'last_login' => 'Last Login',
            'foto' => 'Foto'
        ];
    }
    
    public function cabang(){
        return $this->belongsTo(Cabang::class);
    }
    
    public function getMemberCard()
    {
        $member = Member::where("user_id", $this->id)->first();
        if($member != null){
            return $member->memberCards;
        }else{
            return null;
        }
    }
    
    public static function getStatusList(){
        return [
            self::STATUS_AKTIF => self::STATUS_AKTIF,
            self::STATUS_NON_AKTIF => self::STATUS_NON_AKTIF
        ];
    }
    
    public static function getLevelList()
    {
        return Lookup::items(Lookup::LEVEL_USER);
    }
    
    public function getLevel()
    {
        return Lookup::item(Lookup::LEVEL_USER, $this->level);
    }
    
    public function isAktif()
    {
        return $this->status == self::STATUS_AKTIF;
    }
    
    public function fill(array $attributes)
    {
        if(empty($attributes['cabang_id'])){
            $attributes['cabang_id'] = null;
        }
        
        return parent::fill($attributes);
    }
    
    public function save(array $options = array())
    {
        if ($this->password && $this->password != $this->getOriginal('password')) {
            $this->password = Hash::make($this->password);
        } else {
            unset($this->password);
        }
        
        return parent::save($options);
    }
    
    public function delete()
    {
        if($this->id == self::USER_DEVELOPER){
            return false;
        }else{
            return parent::delete();
        }
    }
    
    public function getStatusBadge()
    {
        return sprintf('<span class="badge%s">%s</span>',$this->status==self::STATUS_AKTIF?' badge-success':'', $this->status);
    }
    
    public function validate($rules = array())
    {
        $res = true;
        if($this->isAdministrator()) {
            if( $this->getOriginal('status') == self::STATUS_AKTIF && $this->getAttribute('status') == self::STATUS_NON_AKTIF ) {
                if( User::administrator()->count() <= 1) {
                    $this->errors[] = 'Minimal harus ada satu administrator aktif';
                    $res = false;
                }
            }
        }
        return $res && parent::validate($rules);
    }
    
    public function isAdministrator()
    {
        return $this->level == self::LEVEL_SUPERUSER;
    }
    
    public function isAdmin()
    {
        return $this->level == self::LEVEL_ADMIN;
    }
    
    public function isDeveloper()
    {
        return $this->level == self::LEVEL_DEVELOPER;
    }
    
    public function isSales()
    {
        return $this->level == self::LEVEL_KARYAWAN;
    }
    
    public function scopeAktif(Builder $query)
    {
        return $query->whereStatus(self::STATUS_AKTIF);
    }
    
    public function scopeAdministrator(Builder $query)
    {
        return $query->whereIn('level', [self::LEVEL_SUPERUSER]);
    }
    
    public function uploadGambar(UploadedFile $gambar, $path)
    {
    	if(!$gambar) {
            return false;
    	} else {
            $filename = $gambar->getClientOriginalName();
            $directory = public_path($path); 
            if(!file_exists($directory)) {
                Storage::makeDirectory($path, $mode = 0777, true, true);
            }
            $gambar->move($path, $filename);

            return $path . $filename;
    	}
    	return false;
    }
    
    public function setFoto(UploadedFile $gambar){
    	if(!$gambar) {
            return false;
    	} else {
            $path = "images/user/{$this->id}/";
            $foto = self::uploadGambar($gambar, $path);
            $this->foto = $foto;
    	}
    	
    	return false;
    }
}
