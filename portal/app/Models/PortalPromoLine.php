<?php

namespace App\Models;

use App\Component\Model;

/**
 * @property integer $portal_promo_id 
 * @property integer $barang_id 
 * @property integer $min_jumlah 
 * @property integer $jenis 
 * @property double $potongan_persen 
 * @property double $potongan_ribuan 
 * 
 * @property PortalPromo $portalPromo 
 * @property Barang $barang 
 * **/

class PortalPromoLine extends Model
{
    
    const JENIS_PERSEN = 10;
    const JENIS_RIBUAN = 20;

    protected $table    = 'portal_promo_line';
    protected $fillable = ['portal_promo_id','barang_id','min_jumlah','jenis','potongan_persen','potongan_ribuan'];
    
    protected function rules()
    {
        return [
            'portal_promo_id'   => [],
            'barang_id'         => ['required'],
            'min_jumlah'        => ['required'],
            'jenis'             => ['required'],
        ];
    }
    
    protected static function attributeNames()
    {
        return \Lang::get('portal_promo_line');
    }
    
    public function portalPromo()
    {
        return $this->belongsTo(PortalPromo::class);
    }
    
    public function bonus()
    {
        return $this->belongsToMany(Barang::class, 'portal_promo_bonus')->withPivot(['jumlah']);
    }
    
    public function barang()
    {
        return $this->belongsTo(Barang::class);
    }
    
    public function saveWithBonus($input)
    {
        \DB::beginTransaction();
        $res = $this->save();
        
        if(array_key_exists("bonus", $input)){
            $data = $input['bonus'];
            $this->bonus()->sync($data);
        }else{
            $this->bonus()->sync([]);
        }
        
        $res ? \DB::commit() : \DB::rollBack();
        
        return $res;
    }
    
    public static function getJenisList()
    {
        return Lookup::items(Lookup::PORTAL_PROMO_JENIS);
    }
    
    public function getJenis()
    {
        return Lookup::item(Lookup::PORTAL_PROMO_JENIS, $this->jenis);
    }
}