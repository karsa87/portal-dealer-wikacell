<?php

namespace App\Models;

use App\Models\Preorder;
use App\Models\Member;
use App\Models\MemberCard;
use App\Models\PortalPromoLine;

/**
 * @property integer $id
 * @property string $jenis
 * @property integer $kode
 * @property string $nama
 */

class Lookup
{
    /*
     * Constant
     */

    const PREORDER_STATUS           = "preorder_status";
    const PREORDER_JENIS            = "preorder_jenis";
    const PREORDER_ORDER            = "preorder_status_order";
    const JENIS_ID_MEMBER           = 'jenis_id_member';
    const JENIS_MEMBER_CARD         = 'jenis_member_card';
    const PORTAL_PROMO_JENIS        = "portal_promo_jenis";
    const PREORDER_LINE_STATUS      = "preorder_line_status";
    const PREORDER_USE_SALDO        = "preorder_use_saldo";
    const PORTAL_PROMO_SHOW         = "portal_promo_show";
    /*
     * Attributes
     */

    private static $_items = [];

    /*
     * Item List
     */

    public static function getItems()
    {
        return [
            self::PREORDER_STATUS => [
                Preorder::STATUS_LUNAS          => 'Lunas',
                Preorder::STATUS_BELUM_LUNAS    => 'Belum Lunas',
            ],
            self::PREORDER_JENIS => [
                Preorder::JENIS_CASH    => 'Cash',
                Preorder::JENIS_TEMPO   => 'Tempo',
            ],
            self::PREORDER_ORDER => [
                Preorder::ORDER_MENUNGGU       => 'Menunggu Konfirmasi',
                Preorder::ORDER_PROSES         => 'Proses',
                Preorder::ORDER_DIKIRIM        => 'Dikirim',
                Preorder::ORDER_SAMPAI         => 'Sampai',
                Preorder::ORDER_SELESAI        => 'Selesai',
                Preorder::ORDER_DIBATALKAN     => 'Dibatalkan',
                Preorder::ORDER_ON_CHART       => 'Lanjutkan',
                Preorder::ORDER_WAIT_KONFIRMASI=> 'Menunggu Pembayaran'
            ],
            self::JENIS_ID_MEMBER => [
                Member::JENIS_ID_KP => 'Kartu Pelajar',
                Member::JENIS_ID_KTP => 'KTP',
                Member::JENIS_ID_SIM => 'SIM'
            ],
            self::JENIS_MEMBER_CARD => [
                MemberCard::JENIS_UMUM => 'Umum',
                MemberCard::JENIS_RESELLER_1 => 'Reseller 1',
                MemberCard::JENIS_RESELLER_2 => 'Reseller 2',
                MemberCard::JENIS_INDEPENDENT_ACTOR => 'Independent Actor',
                MemberCard::JENIS_LC => 'LC (Loyal Customer)',
            ],
            self::PORTAL_PROMO_JENIS => [
                PortalPromoLine::JENIS_RIBUAN => "Ribuan (Rp)",
                PortalPromoLine::JENIS_PERSEN => "Persen (%)",
            ],
            self::PREORDER_LINE_STATUS => [
                Preorder::LINE_STATUS_NEW   => "New",
                Preorder::LINE_STATUS_BONUS => "Bonus"
            ],
            self::PREORDER_USE_SALDO => [
                Preorder::USE_SALDO_YA   => "Ya",
                Preorder::USE_SALDO_TIDAK   => "Tidak",
            ],
	        self::PORTAL_PROMO_SHOW => [
                PortalPromo::IS_SHOW => "Ya",
                PortalPromo::IS_NOT_SHOW => "Tidak",
            ],
        ];
    }

    public static function items($jenis, $except = [])
    {
        if (!isset(self::$_items[$jenis])) {
            self::$_items = self::getItems();
        }
        $tmp = self::$_items[$jenis];
        foreach ($except as $id) {
            unset($tmp[$id]);
        }
        return $tmp;
    }

    public static function item($jenis, $kode)
    {
        if (!isset(self::$_items[$jenis][$kode])) {
            self::$_items = self::getItems();
        }
        return isset(self::$_items[$jenis][$kode]) ? self::$_items[$jenis][$kode] : false;
    }

}