<?php

namespace App\Models;

use App\Component\Model;
use Karsa\Helper;

/**
 * @property string $tanggal 
 * @property string $keterangan 
 * @property double $saldo 
 * @property integer $member_id 
 * 
 * @property Member $member 
 * **/

class MemberSaldo extends Model
{
    protected $table = 'member_saldo';
    protected $fillable = ['tanggal','saldo','member_id','keterangan'];
    
    protected function rules()
    {
        return [
            'tanggal'   => ['required'],
            'saldo'     => ['required']
        ];
    }
    
    protected static function attributeNames()
    {
        return [
            'tanggal'   => 'Tanggal',
            'saldo'     => 'Saldo'
        ];
    }
    
    public function member()
    {
        return $this->belongsTo(Member::class);
    }
    
    public function fill(array $attributes)
    {
        if(isset($attributes['tanggal'])){
            $attributes['tanggal']  = Helper::formatDate($attributes['tanggal'], Helper::FORMAT_DATE_EN_SHORT);
        }
        
        if(isset($attributes['saldo'])){
            $attributes['saldo'] = preg_replace('/,/', '', $attributes['saldo']);
        }
        
        return parent::fill($attributes);
    }
    
    public function save(array $options = array())
    {
        $res = parent::save($options);
        
        if($res){
            $this->member->calculateSaldo();
        }
        
        return $res;
    }
    
    public function delete()
    {
        $member = $this->member;
        $res = parent::delete();
        
        if($res){
            $member = $member->calculateSaldo();
        }
        
        return $res;
    }
}