<?php

namespace App\Models;

use App\Component\Model;
use DB;
use Karsa\Helper;

/**
 * @property integer $id 
 * @property string $no_referensi 
 * @property integer $member_card_id 
 * @property integer $cabang_id 
 * @property string $tgl_penjualan 
 * @property string $jenis 
 * @property double $total_akhir 
 * @property double $uang_muka 
 * @property string $nama 
 * @property string $alamat 
 * @property string $telepon 
 * @property integer $bank_id
 * @property string $status
 * @property double $terbayar 
 * @property string $tgl_tempo 
 * @property integer $sales_id 
 * @property integer $discount_id 
 * @property integer $voucher_id 
 * @property double $potongan_discount 
 * @property double $potongan_voucher 
 * @property double $total_potongan 
 * @property double $total_potongan_detail
 * @property double $total_potongan_header 
 * @property double $total_awal 
 * @property double $ongkir 
 * @property double $potongan_sales 
 * 
 * @property Bank $bank 
 * **/

class Penjualan extends Model
{
    const JENIS_CASH = 'Cash';
    const JENIS_TEMPO = 'Tempo';
    const JENIS_EDC = 'EDC';
    const JENIS_TRANSFER = 'Transfer';
    const STATUS_LUNAS = 'Lunas';
    const STATUS_BELUM_LUNAS = 'Belum Lunas';

    public static $kodePrefix = 'J';
    protected $table = 'penjualan';
    
    protected function rules() {
        $rules = [
            'no_referensi'=>['required','unique:penjualan'],
            'cabang_id'=>['required'],
            'tgl_penjualan'=>['required'],
            'jenis'=>['required'],
            'telepon'=>['numeric'],
            'bank_id'=>['required_if:jenis,'. self::JENIS_EDC .',jenis,'. self::JENIS_TRANSFER],
        ];
        
        if($this->exists){
            unset($rules['no_referensi'][1]);
        }
        
        return $rules;
    }
    
    protected static function attributeNames() {
        return [
            'no_referensi'=>'No Nota',
            'member_card_id'=>'Member',
            'cabang_id'=>'Cabang',
            'tgl_penjualan'=>'Tanggal',
            'tgl_tempo'=>'Tanggal Tempo',
            'jenis'=>'Jenis',
            'total_akhir'=>'Total (Setelah potongan)',
            'ongkir'=>'Ongkir',
            'nama'=>'Nama',
            'alamat'=>'Alamat',
            'telepon'=>'Telepon',
            'bank_id'=>'Bank',
            'uang_muka'=>'Bayar',
            'status'=>'Status',
            'terbayar'=>'Terbayar',
            'sales_id'=>'Sales',
            'voucher_id'=>'Voucher',
            'discount_id'=>'Discount',
            'potongan_discount'=>'Potongan Discount (%)',
            'potongan_voucher'=>'Potongan Voucher (Rp)',
            'total_awal' => 'Total (Sebelum potongan)',
            'total_potongan' => 'Total Potongan',
            'potongan_sales' => 'Potongan Sales'
        ];
    }
    
    public function memberCard(){
        return $this->belongsTo(MemberCard::class);
    }
    
    public function bank(){
        return $this->belongsTo(Bank::class);
    }
    
    public function details(){
        return $this->hasMany(PenjualanDetail::class);
    }

    public function getStatusBadge()
    {
        return sprintf('<b class="block" style="background-color: %s">%s</b>', ($this->status == self::STATUS_LUNAS) ? 'green' : 'red', $this->status);
    }

    public function getJenisBadge()
    {
        return sprintf('<b class="block" style="background-color: %s">%s</b>', ($this->jenis == self::JENIS_CASH) ? 'green' : 'grey', $this->jenis);
    }
}
