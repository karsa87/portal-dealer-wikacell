<?php

namespace App\Models;

use App\Models\Kategori;
use App\Models\Brand;
use App\Component\Model;
use DB;
use Lang;

/**
 * @property integer $id
 * @property string $kode
 * @property string $nama
 * @property string $barcode
 * @property string $jenis
 * @property double $poin
 * @property double $harga_pokok
 * @property string $keterangan
 * @property string $barcode
 * @property integer $kategori_id 
 * @property integer $brand_id 
 * 
 * @property Kategori $kategori
 * @property Brand $brand 
 * **/

class Barang extends Model
{
    const DEFAULT_POIN = 1;
    const STATUS_YA = 1;
    const STATUS_TIDAK = 0;
    
    const JENIS_ELEKTRONIK = 5;
    const JENIS_NONELEKTRONIK = 10;

    protected $table = 'barang';
    protected $fillable = ['barcode','kode','nama','jenis','kategori_id','garansi','poin','harga_pokok','keterangan','brand_id'];

    public static $kodePrefix = 'B-';
    
    protected function rules() {
        return [
            'kode'=>['required','unique:barang,id,'.$this->id],
            'nama'=>['required'],
            'kategori_id'=>['required'],
            'poin'=>['numeric','min:0']
        ];
    }
    
    public static function attributeNames()
    {
        return Lang::get('barang');
    }
    
    public function stok(){
        return $this->hasMany(Stok::class);
    }
    
    public function barangCode(){
        return $this->hasMany(BarangCode::class);
    }
    
    public function kategori()
    {
        return $this->belongsTo(Kategori::class);
    }
    
    public function brand()
    {
        return $this->belongsTo(Brand::class, 'brand_id');
    }
    
    public function fill(array $attributes)
    {
        if(empty($attributes['brand_id'])){
            $attributes['brand_id'] = null;
        }
        
        return parent::fill($attributes);
    }

    public static function updateHargaPokok($ids = []){
    
        $beli = DB::table('pembelian_detail')
                ->select(DB::raw("
                    SUM(jumlah) AS stok_masuk,
                    0 AS stok_keluar,
                    SUM(jumlah*harga_beli) AS harga, barang_id"))
                ->groupBy('barang_id');
        $returbeli = DB::table('retur_pembelian_detail')
                ->select(DB::raw("
                    0 AS stok_masuk,
                    (SUM(jumlah) * -1) AS stok_keluar,
                    (SUM(jumlah*harga_beli) * -1) AS harga, barang_id"))
                ->groupBy('barang_id');
        
        $query = DB::table(DB::raw("({$beli->unionAll($returbeli)->toSql()}) as A"))
                ->select(
                    DB::raw("SUM(stok_masuk) as stok_masuk, SUM(stok_keluar) as stok_keluar, "
                            . "SUM(harga) as harga, barang_id")
                )
                ->groupBy('barang_id');
        
        if(count($ids) > 0){
            $query->whereIn('barang_id',$ids);
        }
        
        $res = $query->get();
        
        foreach ($res as $row) {
            $stok_masuk = $row->stok_masuk;
            $stok_keluar = $row->stok_keluar;
            $stok = $stok_masuk - $stok_keluar;
            $harga = $row->harga;
            
            $hpp = $harga / ($stok_masuk - $stok_keluar);
            
            if($stok == 0){
                $lastPenjualan = PenjualanDetail::whereBarangId($this->id)
                    ->join('penjualan', 'penjualan.id', '=', 'penjualan_detail.penjualan_id')
                    ->orderBy('penjualan.created_at', 'DESC')
                    ->first();
                $hpp = $lastPenjualan ? $lastPenjualan->harga_pokok : 0;
            }
            
            Barang::where('id','=',$row->barang_id)
                ->update([
                    'harga_pokok'=>$hpp
                ]);
        }
    }
    
    public function updateStok()
    {
        $beli = DB::table('pembelian_detail')
                ->select(DB::raw("
                    SUM(jumlah) AS stok_masuk,
                    0 AS stok_keluar"))
                ->join('pembelian','pembelian.id','=','pembelian_detail.pembelian_id')
                ->where('pembelian_detail.barang_id','=',  $this->barang_id)
                ->groupBy('pembelian.cabang_id');
        $mutasiMasuk = DB::table('mutasi_barang')
                ->select(DB::raw("
                    SUM(stok_keluar) AS stok_masuk,
                    0 AS stok_keluar"))
                ->where('barang_id','=',  $this->barang_id)
                ->groupBy('ke_cabang_id');
        $mutasiKeluar = DB::table('mutasi_barang')
                ->select(DB::raw("
                    0 AS stok_masuk,
                    SUM(stok_keluar) AS stok_keluar"))
                ->where('barang_id','=',  $this->barang_id)
                ->groupBy('dari_cabang_id');
        $jual = DB::table('penjualan_detail')
                ->select(DB::raw("
                    0 AS stok_masuk,
                    SUM(jumlah) AS stok_keluar"))
                ->join('penjualan','penjualan.id','=','penjualan_detail.penjualan_id')
                ->where('penjualan_detail.barang_id','=',  $this->barang_id)
                ->groupBy('penjualan.cabang_id');
        $returbeli = DB::table('retur_pembelian_detail')
                ->select(DB::raw("
                    0 AS stok_masuk,
                    SUM(jumlah) AS stok_keluar"))
                ->join('retur_pembelian','retur_pembelian.id','=','retur_pembelian_detail.retur_pembelian_id')
                ->where('retur_pembelian_detail.barang_id','=',  $this->barang_id)
                ->groupBy('retur_pembelian.cabang_id');
        
        $res = $beli->unionAll($mutasiMasuk)->unionAll($mutasiKeluar)->unionAll($jual)->unionAll($returbeli)->get();
        
        $stok_masuk = $stok_keluar = $stok = 0;
        foreach ($res as $v) {
            $stok_masuk += $v->stok_masuk;
            $stok_keluar += $v->stok_keluar;
        }
        
        $stok = $stok_masuk - $stok_keluar;
        
        $this->stok_masuk = $stok_masuk;
        $this->stok_keluar = $stok_keluar;
        $this->stok = $stok;
        
        $this->save();
    }
    
    public function delete() {
        DB::beginTransaction();
        $res = true;
        
        $res = (count($this->stok) > 0) ? $this->stok()->delete() : true;
        
        $res = parent::delete();
        
        $res ? DB::commit() : DB::rollback();
        
        return $res;
    }
    
    public function getBarangName(){
        return $this->mergeBarangName($this->nama, $this->kode, '','', $this->id) ;
    }
    
    public static function mergeBarangName($nama, $kode, $imei = null, $serial_number = null, $id=null){
        if($id){
            $nama = Helper::link_to_route_auth('kabupaten_kota.show', $nama,['id'=>$id]);
        }
        
        $return = "<div class='tt-small'><b>$nama</b>";
        if(!empty($imei) || !empty($serial_number)){
            $return .= '<span>';
            
            if(!empty($imei)){
                $return .= "/ Imei : $imei";
            }
            
            if(!empty($serial_number)){
                $return .= "/ SN : $serial_number";
            }
            
            $return .= '</span>';
        }
        
        $return .= '</div>';
        
        return $return;
    }
    
    public static function getJenisList()
    {
        return Lookup::items(Lookup::JENIS_BARANG);
    }
    
    public function getJenis()
    {
        return Lookup::item(Lookup::JENIS_BARANG, $this->jenis);
    }
    
    public function save(array $options = array())
    {
        if($this->poin == null || $this->poin == ''){
            $this->poin = self::DEFAULT_POIN;
        }
        
        return parent::save($options);
    }
    
    public static function getLastKode()
    {
        $res = Barang::
                select(\DB::raw('IFNULL( MAX( SUBSTRING(kode,' . strlen(self::$kodePrefix) . '+ 1) ), 0) + 1 max'))
                ->first()
                ->max;
        return sprintf('%s%s', self::$kodePrefix, str_pad($res, 4, "0", STR_PAD_LEFT) );
    }
}
