<?php

namespace App\Component;

use Auth;
use Validator;

class Model extends \Illuminate\Database\Eloquent\Model
{
    
    public $errors = [];

    public function save(array $options = array())
    {
        if ($this->timestamps && Auth::user()) {
            if (!$this->exists) {
                $this->created_by = Auth::user()->id;
            }
            $this->updated_by = Auth::user()->id;
        }
        return parent::save($options);
    }
    
    public function validate( $rules = [] )
    {
        $rules = array_merge($this->rules(), $rules);
        $validator = Validator::make($this->getAttributes(), $rules);
        $validator->setAttributeNames(static::attributeNames());

        if ($validator->fails()) {
            $this->errors = array_merge($this->errors, $validator->errors()->all());
        }
        return empty($this->errors);
    }
    
    protected function rules()
    {
        return [];
    }
    
    protected static function attributeNames(){
        return [];
    }
    
    public static function getAttributeName($field)
    {
        $list = static::attributeNames();
        return isset($list[$field]) ? $list[$field] : ucwords($field);
    }
    
    public function delete()
    {
        try {
            $res = parent::delete();
        } catch (\Illuminate\Database\QueryException $ex) {
            $res = false;
        }
        return $res;
    }

}
