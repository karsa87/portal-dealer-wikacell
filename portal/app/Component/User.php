<?php 

namespace App\Component;

use Auth;
use Hash;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Validator;
use Zain\Helper;

class User extends Authenticatable
{
    public $errors = [];
    
    protected static function abilities()
    {
        return [
        ];
    }


    public function hasAbility()
    {
        /*
         * $route = 'guru'
         * $route = 'guru.index'
         * $route = '!guru.index'
         */
        $rules = static::abilities();
        $myRule = $rules[$this->level];
        
        $routes = func_get_args();        

        $finalRes = null;
        foreach ($routes as $route) {
            $res = null;
            @list($controller, $action) = explode('.', $route);
            $rule = "{$controller}" . ($action ? ".{$action}" : '');
            foreach (range(0,$action ? 1 : 0) as $i) {
                if( in_array("!{$rule}", $myRule) && $res === null ) {
                    $res = false;
                } elseif( in_array("{$rule}", $myRule) ) {
                    $res = true;
                }
                $rule = $controller;
                if($res === false) {
                    break;
                }
            }
            $finalRes = $res;
            if( $res !== false ) {
                break;
            }
        }
        if ( $finalRes === null && in_array('*', $myRule)) {
            $finalRes = true;
        }
        
        return $finalRes;
    }
    
    protected function rules()
    {
        return [];
    }
    
    public function save(array $options = array())
    {
        if ($this->timestamps && Auth::user()) {
            if (!$this->exists) {
                $this->created_by = Auth::user()->id;
            }
            $this->updated_by = Auth::user()->id;
        }
        return parent::save($options);
    }
    
    public static function attributeNames()
    {
        return [];
    }
    
    public static function getAttributeName($field)
    {
        $list = static::attributeNames();
        return isset($list[$field]) ? $list[$field] : ucwords($field);
    }

    public function validate( $rules = [] )
    {
        $rules = array_merge($this->rules(), $rules);
        $validator = Validator::make($this->getAttributes(), $rules);
        $validator->setAttributeNames(static::attributeNames());

        if ($validator->fails()) {
            $this->errors = array_merge($this->errors, $validator->errors()->all());
        }
        return empty($this->errors);
    }
    
    public function delete()
    {
        try {
            $res = parent::delete();
        } catch (QueryException $ex) {
            $res = false;
        }
        return $res;
    }
    
}
