<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Bllim\Datatables\Datatables;
use View;
use App\Models\Barang;
use App\Models\BarangCode;
use App\Models\BarangHarga;
use Karsa\Helper;
use Illuminate\Http\Request;
use App\Models\Preorder;
use App\Models\Bank;
use App\Models\Stok;
use App\Models\PortalPromo;
use App\Models\PortalPromoLine;
use App\Models\Notifications;
use Karsa\Notification;

/* @var $model Preorder */

class PreorderController extends Controller
{
    public function index()
    {
        return \Redirect::route("preorder.create");
    }

    public function indexData(Request $request)
    {
        $query = Barang::select("barang.nama","barang.keterangan",  \DB::raw("SUM(IFNULL(bh2.harga, 0)) AS harga"), \DB::raw("0 as qty"), "barang.id")
                ->join("kategori as k","k.id","=","barang.kategori_id")
                ->leftjoin('stok',  function ($join) {
                    $join->on('stok.barang_id','=','barang.id');
                    $join->where("stok.cabang_id",1);
                })
                ->leftjoin(\DB::raw("(select avg(bh.harga) as harga,bc.barang_id from barang_code as bc left join barang_harga as bh on bh.barang_code_id = bc.id and bh.jenis = ".BarangHarga::JENIS_RESELLER_1." where bc.is_use= ". BarangCode::IS_USE_BELUM_TERJUAL ." group by bc.barang_id) as bh2"),'bh2.barang_id','=','barang.id')
                ->where('stok','>',0)
                ->where('bh2.harga','>','0')
                ->groupBy('barang.id');
                
        $search =$request->get('search');
        
        if($search){
            $like = '%'.$search.'%';
            $query->where('barang.nama','like',$like);
        }
        
        if(!empty($request->get('kategori_id'))){
            $query->where('kategori_id','=',$request->get('kategori_id'));
        }
        
        if(!empty($request->get('brand_id'))){
            $query->where('brand_id','=',$request->get('brand_id'));
        }

        $rs = Datatables::of($query)
                ->edit_column('harga', function(Barang $v){ return Helper::idNumber($v->harga); })
                ->edit_column('qty', function(Barang $v){ 
                    return "<div class='qty'>"
                    . "<center><button onclick='updateQty(0,".$v->id.")'>-</button>"
                        . "<input type='text' value='0' id='counter-".$v->id."' readonly>"
                        . "<button onclick='updateQty(1,".$v->id.")'>+</button>"
                    . "</center></div>";
                })
                ->edit_column('id', function(Barang $v){ 
                    return "<button id='btn-toggle-".$v->id."' "
                            . "onclick='updateList(".$v->id.")' "
                            . "class='btn-add'>Add to list</button>"; 
                })
                ->make();
        
        return $rs;
    }
    
    public function create()
    {
        $preorder = Preorder::where("status_order",  Preorder::ORDER_ON_CHART)
                ->where("member_card_id",\Auth::user()->getMemberCard()->id)
                ->first();
        
        if($preorder){
            $now    = date_create(date("Y-m-d H:i:s"));
            $date   = date_create($preorder->tanggal);
            $diff   = date_diff($date, $now, FALSE);
            
            if($diff->days < 30){
                return \Redirect::route("preorder.edit", ["id"=>$preorder->id]);
            }
            
            $preorder->delete();
        }
        
        $saldo = \Auth::user()->getMemberCard()->member->saldo;
        $model = new Preorder();
        $model->no_order        = Preorder::getLastNoOrder();
        $model->member_card_id  = \Auth::user()->getMemberCard()->id;
        $model->tanggal         = date('Y-m-d H:i:s');
        $model->jenis           = Preorder::JENIS_CASH;
        $model->tanggal_tempo   = null;
        $model->bank_id         = Bank::BANK_KAS_BESAR;
        $model->total_awal      = 0;
        $model->total_potongan  = 0;
        $model->biaya_lain      = 0;
        $model->total_akhir     = 0;
        $model->status          = Preorder::STATUS_BELUM_LUNAS;
        $model->status_order    = Preorder::ORDER_ON_CHART;
        $model->penjualan_id    = null;
        $model->use_saldo       = Preorder::USE_SALDO_TIDAK;
        $model->saldo           = 0;
        $model->save();
        
        return View::make('preorder.create', compact('model','saldo'));
    }
    
    public function store(Request $request)
    {
        $input = $request->all();
        $model = new Preorder();
        $model->fill($input);
        
        if($model->validate() && $model->save()){
            return $model->id;
        }
        
        return '<div>'.implode('<div></div>', $model->errors).'</div>';
    }
    
    public function show($id)
    {
        $model = Preorder::find($id);
        return View::make('preorder.show',  compact('model'));
    }
    
    public function edit($id)
    {
        $model = Preorder::with([
                    'detail' => function($q) {
                        $q->wherePivot('status', Preorder::LINE_STATUS_NEW);
                    }
                ])
                ->find($id);
        $detail = $model->detail;
        $saldo = \Auth::user()->getMemberCard()->member->saldo;
        
        return View::make('preorder.edit',  compact('model','detail', 'saldo'));
    }
    
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $model = Preorder::find($id);
        $model->fill($input);
        
        if($model->validate() && $model->save()){
            return $model->id;
        }
        
        return '<div>'.implode('<div></div>', $model->errors).'</div>';
    }
    
    public function destroy($id)
    {
        $model = Preorder::find($id);
        return $model->delete() ? '1' : 'Data Tidak Bisa Dihapus';
    }
    
    public function updatePreorder(Request $request)
    {
        $status     = 0;
        $message    = "";
        
        $input      = $request->all();
        if(array_key_exists("id", $input)){
            $preorder           = Preorder::find($input["id"]);
            foreach ($input as $k => $v){
                if($k == "id" || $k == "_token"){
                    continue;
                }
                $preorder[$k] = $v;
            }
            
            if(!($preorder->validate() && $preorder->save())){
                $status = 1;
                $message = $preorder->errors;
            }
        }else{
            $status = 1;
            $message = "Preorder id not found";
        }
        
        return \Response::json([
            "status"    => $status,
            "message"   => $message,
            "date"      => []
        ]);
    }

    public function updateListPreorder(Request $request)
    {
        $status     = 0;
        $message    = "";
        $data       = [];
        $input = $request->all();

        $model  = Preorder::find($input['preorder_id']);
        $detail = $model->detail()->where('barang_id',$input['barang_id'])->first();
        
        $stok = Stok::where("barang_id",$input["barang_id"])->sum(\DB::raw("IFNULL(stok,0)"));
        
        if($stok < $input["qty"]){
            $status = 1;
            $message = "Stok tidak mencukupi, tersisa $stok saja";
        }
        
        if(!$status){
            $harga = BarangCode::getAvgHarga($input['barang_id']);

            $promo = PortalPromo::getPromo([
                'barang_id' => $input['barang_id'],
                'jumlah' => $input['qty']
            ]);
            
            $potongan   = 0;
            $diskon     = 0;
            $promo_line_id = null;
            $total_potongan = (double) 0;
            $bonus      = [];
            
            if(count($promo) > 0 && array_key_exists($input["barang_id"], $promo["lines"])){
                $barang = $promo["lines"][$input["barang_id"]];
                
                $p = 0;
                if($barang["jenis"] == PortalPromoLine::JENIS_PERSEN){
                    $diskon = $barang["potongan_persen"];
                    $p = $harga * ($diskon / 100);
                }elseif ($barang["jenis"] == PortalPromoLine::JENIS_RIBUAN) {
                    $potongan = $barang["potongan_ribuan"];
                    $p = $potongan;
                }

//                $total_potongan = $p * $input['qty'];
                $total_potongan = $p;

                $bonus = $barang['bonus'];
                $promo_line_id = $barang["id"];
            }else if(count($detail) > 0){
                $line = PortalPromoLine::with("bonus")
                        ->where("barang_id",$input['barang_id'])
                        ->get()
                        ->toArray();
                if(count($line) > 0){
                    foreach ($line[0]["bonus"] as $b){
                        $model->detail()->where('status', Preorder::LINE_STATUS_BONUS)->detach($b["id"]);
                    }
                }
            }
            
            $total_awal     = $harga*$input['qty'];
            $total_akhir    = $total_awal - $total_potongan;
            
            $v = [
                'harga_jual'    => $harga,
                'jumlah'        => $input['qty'],
                'total_awal'    => $total_awal,
                'promo_line_id' => $promo_line_id,
                'diskon'        => $diskon,
                'potongan'      => $potongan,
                'total_potongan'=> $total_potongan,
                'total_akhir'   => $total_akhir,
                'status'        => Preorder::LINE_STATUS_NEW
            ];
            
            if(count($detail) > 0){
                $model->detail()->updateExistingPivot($input["barang_id"], $v);
            }else{
                $model->detail()->syncWithoutDetaching([$input["barang_id"]=>$v]);
                $detail = $model->detail()->where('barang_id',$input['barang_id'])->first();
            }
            
            // add bonus
            $ket_bonus = [];
            foreach ($bonus as $id => $b){
                $harga = BarangCode::getAvgHarga($id);
                
                $i = [
                    'harga_jual'    => $harga,
                    'jumlah'        => $b['jumlah'],
                    'total_awal'    => ($harga * $b['jumlah']),
                    'diskon'        => 0,
                    'potongan'      => $harga,
//                    'total_potongan'=> ($harga * $b['jumlah']),
                    'total_potongan'=> $harga,
                    'total_akhir'   => 0,
                    'status'        => Preorder::LINE_STATUS_BONUS
                ];
                
                $count = $model->detail()->where('barang_id',$id)->count();
                
                if($count > 0){
                    $model->detail()->updateExistingPivot($id, $i);
                }else{            
                    $model->detail()->syncWithoutDetaching([$id=>$i]);
                }
                
                $ket_bonus[] = $b["jumlah"] . " " . $b["nama"];
            }

            $data = [
                'id'        => $input["barang_id"],
//                'kode'      => $detail->kode,
                'nama'      => $detail->nama,
                'qty'       => $input["qty"],
                'subtotal'  => $v["total_awal"],
                'bonus'     => implode(", ", $ket_bonus)
            ];
        }
        
        $result = \Response::json([
            "status"    =>$status,
            "message"   =>$message,
            "data"      =>$data
        ]);
        
        return $result;
    }
    
    public function deleteList(Request $request)
    {
        $model  = Preorder::find($request->get("preorder_id"));
        
        $line   = PortalPromoLine::with("bonus")
                ->where("barang_id",$request->get("barang_id"))
                ->get()
                ->toArray();
        
        if(count($line) > 0){
            foreach ($line[0]["bonus"] as $b){
                $model->detail()->where('status', Preorder::LINE_STATUS_BONUS)->detach($b["id"]);
            }
        }
                
        $model->detail()->detach($request->get("barang_id"));
        
        return \Response::json([
            "status"    => 0,
            "message"   => ""
        ]);
    }

    public function checkout(Request $request){
        $model = Preorder::with('memberCard')
                ->where("created_by", \Auth::user()->id)
                ->where("id",$request->id)
                ->first();
        
        if($model){
            return View::make('preorder.checkout',  compact('model'));
        }else{
            Notification::addDanger("Anda tidak memiliki order ini");
            
            return \Redirect::route("dashboard");
        }
    }
    
    public function payment(Request $request){
        $model = Preorder::where("created_by", \Auth::user()->id)
                ->where("id",$request->id)
                ->first();
        
        if($model){
            $member  = auth()->user()->getMemberCard();
            $max_piutang = $member->member->max_piutang;
            $piutang = $member->getTotalPiutang() + $model->total_akhir;
            
            if($piutang > $max_piutang && $model->jenis == Preorder::JENIS_TEMPO){
                Notification::addDanger("Kuota hutang anda sudah penuh");
                return \Redirect::back();
            }

            $model->payment();
            Notifications::addNotification(
                \Auth::user()->id, 
                \App\Models\User::LEVEL_ADMIN, 
                "Ada Pesananan Baru #".$model->no_order, 
                "preorder.show",
                ["id"=>$model->id]
            );
            
            if($model->jenis == Preorder::JENIS_CASH){
                return \Redirect::route("trackorder.upload", $model->id);
            }
            
            return \Redirect::route("dashboard");
        }else{
            \Karsa\Notification::addDanger("Anda tidak memiliki order ini");
            
            return \Redirect::route("dashboard");
        }
    }
}