<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use View;
use Bllim\Datatables\Datatables;
use App\Models\PortalPromo;
use App\Models\PortalPromoLine;
use Auth;
use DB;
use Karsa\Helper;
use Illuminate\Support\Facades\Response;

class PortalPromoController extends Controller
{
    public function index()
    {        
        return View::make('portal_promo.index');
    }
    
    public function indexData(Request $request){
        $query = PortalPromo::select('no_promo','nama','start','end','id');
        
        if($request->get('search')){
            $search = $request->get('search');
            $search = "%$search%";
            $query->where(function($q) use($search){
                $q->where('no_promo','like',$search)
                        ->orWhere('nama','like',$search);
            });
        }
        
        if($request->get('start')){
            $query->where('start','>=',Helper::formatDate($request->get('start'), Helper::FORMAT_DATE_EN_SHORT));
        }
        
        if($request->get('end')){
            $query->where('end','<=',Helper::formatDate($request->get('end'), Helper::FORMAT_DATE_EN_SHORT));
        }
        
        $result = Datatables::of($query)
                ->edit_column('start', function(PortalPromo $v){
                    return Helper::formatDate($v->start, Helper::FORMAT_DATE_ID_LONG);
                })
                ->edit_column('end', function(PortalPromo $v){
                    return Helper::formatDate($v->end, Helper::FORMAT_DATE_ID_LONG);
                })
                ->edit_column('id', function(PortalPromo $v){
                    ob_start();
                    
                    if(Auth::user()->hasAbility('portal_promo.show')){
                        ?>
                        <a href="<?= route('portal_promo.show', $v->id) ?>" class="btn btn-sm btn-info" title="View">
                            <i class="fa fa-eye"></i>
                        </a>
                        <?php
                    }
                    
                    return ob_get_clean();
                })
                ->make();
        
        return $result;
    }

    public function create()
    {
        return \Redirect::route('dashboard');
    }

    public function store(Request $request)
    {
        return \Redirect::route('dashboard');
    }

    public function show($id)
    {
        $model = PortalPromo::with("lines")->find($id);
        return View::make('portal_promo.show',  compact('model'));
    }

    public function edit($id)
    {
        return \Redirect::route('dashboard');
    }

    public function update(Request $request, $id)
    {
        return \Redirect::route('dashboard');
    }

    public function destroy($id)
    {
        return \Redirect::route('dashboard');
    }
}
