<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Bllim\Datatables\Datatables;
use View;
use Karsa\Helper;
use Illuminate\Http\Request;
use App\Models\Preorder;

class OrderController extends Controller {

    public function index()
    {        
        return View::make('order.index');
    }
    
    public function indexData(Request $request)
    {
        $query = Preorder::select('preorder.no_order','preorder.tanggal','preorder.jenis',
                'preorder.tanggal_tempo','preorder.status_order','preorder.status',
                'preorder.total_akhir','preorder.id', 
                \DB::raw("(p.total_akhir - p.saldo_member) as total_akhir2"), "p.no_referensi")
                ->leftJoin("penjualan as p","p.id","=","preorder.penjualan_id")
                ->where('preorder.created_by', \Auth::user()->id)
                ->where('preorder.status_order','!=', Preorder::ORDER_ON_CHART)
                ->where('preorder.status_order','!=', Preorder::ORDER_SAMPAI)
                ->where('preorder.status_order','!=', Preorder::ORDER_SELESAI);
        
        if($request->get('search')){
            $search = $request->get('search');
            $query->where('no_order','like',"%$search%");
        }
        
        if($request->get('status_order')){
            $query->where('status_order',$request->get('status_order'));
        }
        
        if($request->get('tanggal')){
            $tanggal = Helper::formatDate($request->get('tanggal'), Helper::FORMAT_DATE_EN_SHORT);
            $query->where('tanggal',$tanggal);
        }

        return Datatables::of($query)
                ->remove_column("total_akhir2","no_referensi")
                ->edit_column('total_akhir', function(Preorder $v){ 
                    $total = $v->total_akhir;
                    if($v->total_akhir2 != 0){
                        $total = $v->total_akhir2;
                    }
                    return Helper::idNumber($total);
                })
                ->edit_column('tanggal', function(Preorder $v){ return Helper::formatDate($v->tanggal, Helper::FORMAT_DATE_ID_SHORT); })
                ->edit_column('tanggal_tempo', function(Preorder $v){ return Helper::formatDate($v->tanggal_tempo, Helper::FORMAT_DATE_ID_SHORT); })
                ->edit_column('status_order', function(Preorder $v){ return $v->getStatusOrderBadge(); })
                ->edit_column('status', function(Preorder $v){ return $v->getStatusBadge(); })
                ->edit_column('jenis', function(Preorder $v){ return $v->getJenisBadge(); })
                ->edit_column('no_order', function(Preorder $v){
                    return $v->no_referensi ? $v->no_referensi : $v->no_order ;
                })
                ->edit_column('id', function(Preorder $v){
                    ob_start();
                    
                    if(\Auth::user()->hasAbility('order.show')){
                        ?>
                        <a href="<?= route('order.show', $v->id) ?>" class="btn btn-sm btn-info" title="View">
                            <i class="fa fa-eye"></i>
                        </a>
                        <?php
                    }
                    
                    return ob_get_clean();
                })
                ->make();
    }
    
    public function show($id)
    {
        $model = Preorder::with([
            'penjualan' => function($q){
                $q->with([
                    'memberCard'=>function($q2){
                        $q2->with('member');
                    },
                    'bank'
                ]);
            }
        ])->where('id',$id)->first();
        
        return View::make('order.show',  compact('model'));
    }
}