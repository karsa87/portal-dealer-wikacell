<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Bllim\Datatables\Datatables;
use View;
use App\Models\Barang;
use App\Models\BarangCode;
use App\Models\BarangHarga;
use Karsa\Helper;
use Illuminate\Http\Request;
use App\Models\Preorder;
use App\Models\PortalPromo;

class DashboardController extends Controller {

    public function index()
    {
        $list_order = Preorder::whereNotIn("status_order",[Preorder::ORDER_SELESAI])
                ->where("created_by", \Auth::user()->id)
                ->orderBy("tanggal","DESC")
                ->limit(3)
                ->get();
        
        $promo = PortalPromo::where('status', PortalPromo::STATUS_AKTIF)
                ->where('start','<=', date('Y-m-d'))
                ->where('end','>=', date('Y-m-d'))
                ->where('is_show',PortalPromo::IS_SHOW)
                ->first();
        
        return View::make('dashboard.index',  compact("list_order","promo"));
    }
    
    public function indexData(Request $request)
    {
        $query = Barang::select("barang.nama","k.nama as kategori","barang.keterangan",  \DB::raw("SUM(IFNULL(bh2.harga, 0)) AS harga"))
                ->join("kategori as k","k.id","=","barang.kategori_id")
                ->leftjoin('stok',  function ($join) {
                    $join->on('stok.barang_id','=','barang.id');
                    $join->where("stok.cabang_id",1);
                })
                ->leftjoin(\DB::raw("(select avg(bh.harga) as harga,bc.barang_id from barang_code as bc left join barang_harga as bh on bh.barang_code_id = bc.id and bh.jenis = ".BarangHarga::JENIS_RESELLER_1." where bc.is_use= ". BarangCode::IS_USE_BELUM_TERJUAL ." group by bc.barang_id) as bh2"),'bh2.barang_id','=','barang.id')
                ->where('stok','>',0)
                ->groupBy('barang.id');
                
        $search =$request->get('search');
        
        if($search){
            $like = '%'.$search.'%';
            $query->where('barang.nama','like',$like);
        }
        
        if(!empty($request->get('kategori_id'))){
            $query->where('kategori_id','=',$request->get('kategori_id'));
        }
        
        if(!empty($request->get('brand_id'))){
            $query->where('brand_id','=',$request->get('brand_id'));
        }
        
        return Datatables::of($query)
                ->edit_column('harga', function(Barang $v){ return Helper::idNumber($v->harga); })
                ->make();
    }
}