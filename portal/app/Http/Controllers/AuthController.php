<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\User;
use Auth;
use Hash;
use DB;
use Illuminate\Http\Request;
use Redirect;
use View;
use Karsa\Notification;

class AuthController extends Controller
{

    public function index()
    {
        if (Auth::check()) {
            return Redirect::intended('/dashboard');
        }        
        return View::make('layout.login');
    }

    public function login(Request $request)
    {
        $user = User::whereUsername($request->get('username'))->first();
        if (count($user) <= 0) {
            return 'Username tidak terdaftar';
        } elseif (!Hash::check($request->get('password'), $user->password)) {
            return 'Password tidak cocok';
        } elseif (!$user->isAktif()) {
            return 'Username dalam status tidak aktif.';
        } else {
            Auth::login($user, false);
            Notification::addSuccess("Selamat datang, " . Auth::user()->username);
            return '1';
        }
    }

    public function logout()
    {
        Auth::logout();
        Notification::addSuccess('Anda berhasil logout');
        return Redirect::route('auth.index');
    }

    public function apiLogin(Request $request){
        
        $username = $request->get('username');
        $password = $request->get('password');

        $err = [];
        DB::beginTransaction();
        
        $token = "";

        $user = User::whereUsername($username)->first();
        if (count($user) == 0) {
            $err = 'Username tidak terdaftar';
        } elseif (!Hash::check($password, $user->password)) {
            $err = 'Password tidak cocok';
        } else {
            Auth::login($user, true);
            $token = Auth::user()->remember_token;
            // Notification::addSuccess("Selamat datang, " . Auth::user()->username);            
        }
        
        DB::commit();
        
        
        if(count($err) > 0){
            $result = array(
                'status'=>count($err) > 0 ? '1' : '0',
                'message'=> $err,
                'data' => []
            );
        }else{
            $result = array(
                'status'=>count($err) > 0 ? '1' : '0',
                'message'=> $err,
                'data' => [
                    'user_id' => $user->id,
                    'token' => $token
                ]
            );
        }
        
        return json_encode($result);
    }
	
    public function apiLogout(Request $request){
        $token = $request->get('token');
        $err = [];
        DB::beginTransaction();

        $user = User::where('remember_token','=',$token)->first();
        if(count($user) <= 0){
            $result['status'] = '100';
            $result['message'] = "Maaf session / akun anda sudah login di aplikasi lain";

            return json_encode($result);
        }

        $user->remember_token = null;
        $user->save();
        
        DB::commit();
        
        
        $result = array(
            'status'=>count($err) > 0 ? '1' : '0',
            'message'=> 'Berhasil logout',
            'data' => []
        );
        
        return json_encode($result);
    }
}
