<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use View;
use App\Models\Member;
use App\Models\MemberSaldo;
use Auth;
use Bllim\Datatables\Datatables;
use Karsa\Helper;
use DB;
use App\Models\Preorder;

class MemberController extends Controller
{
    public function edit($id)
    {
        $model = Member::find($id);
        $user = $model->user;
        if($user){
            $model->username = $user->username;
            $model->password = $user->password;
        }
        $modelCard = $model->memberCards;
        return View::make('member.edit',  compact('model','modelCard'));
    }

    public function update(Request $request, $id)
    {
        $input = $request->all();
        $model = Member::find($id);
        $model->fill($input);
        
        if($model->validateWithMemberCard($input) && $model->saveWithMemberCard($input)){
            return $model->id;
        }
        
        return '<div>'.  implode('<div></div>', $model->errors).'</div>';
    }
    
    public function saldo()
    {
        Auth::user()->getMemberCard()->member->calculateSaldo();
        return \View::make('member.saldo');
    }
    
    public function indexDataSaldo(Request $request)
    {
        $q1 = DB::table('member_saldo as ms')
                ->select(DB::raw(" ms.tanggal, ms.saldo, ms.keterangan, 'Debit' as tipe, ms.created_at"))
                ->where("ms.member_id",  Auth::user()->getMemberCard()->member->id);
        
        $q2 = DB::table('penjualan as p')
                ->select(DB::raw("p.tgl_penjualan as tanggal, p.saldo_member as saldo, CONCAT('Pembelian : ', p.no_referensi) as keterangan, 'Kredit' as tipe, p.created_at"))
                ->where("member_card_id",  Auth::user()->getMemberCard()->id)
                ->where("p.saldo_member", ">",  0);
        
        $q3 = DB::table("preorder")
                ->select(DB::raw("tanggal, saldo, CONCAT('Preorder : ', no_order) as keterangan,'Kredit' as tipe, created_at"))
                ->where("member_card_id",Auth::user()->getMemberCard()->id)
                ->whereNull("penjualan_id")
                ->where("saldo", ">",  0);
        
        $q4 = DB::table("preorder")
                ->select(DB::raw("tanggal, saldo, CONCAT('Void Preorder : ', no_order) as keterangan,'Debit' as tipe, created_at"))
                ->where("member_card_id",Auth::user()->getMemberCard()->id)
                ->where("status_order",Preorder::ORDER_DIBATALKAN)
                ->where("saldo", ">",  0);
        
        if($request->get("tanggal")){
            $tanggal = date("Y-m-d",strtotime($request->get("tanggal")));
            $q1->where("tanggal",$tanggal);
            $q2->where("tgl_penjualan",$tanggal);
        }

        if($request->get("search")){
            $search = $request->get("search");

            $q1->where(function($q) use ($search) {
                $q->where("saldo","=",$search)
                    ->orWhere("keterangan","like","%". $search ."%");
            });

            $q2->where(function($q) use ($search) {
                $q->where("saldo_member","=",$search)
                    ->orWhere("keterangan","like","%". $search ."%");
            });
        }

        $query = $q1->unionAll($q2)->unionAll($q3)->unionAll($q4)->orderBy("created_at","ASC");
        
        $result= [];
        $saldo = 0;
        $rs = $query->get()->toArray();
        $i = count($rs) - 1;
        
        foreach ($rs as $k => $v) {
            $v->tanggal = Helper::formatDate($v->tanggal, Helper::FORMAT_DATE_ID_LONG);
            $v->nominal = Helper::idNumber($v->saldo);
            
            if($v->tipe == "Debit"){
                $v->tipe = '<span class="block" style="background:green;font-size:10px;">'.$v->tipe.'</span>';
                $saldo += $v->saldo;
            }elseif ($v->tipe == "Kredit") {
                $v->tipe = '<span class="block" style="background: red;font-size:10px;">'.$v->tipe.'</span>';
                $saldo -= $v->saldo;
            }
            $v->sisa = Helper::idNumber($saldo);
            $result[] = $v;
        }
        
        krsort($result);
        $data = array_values($result);
        
        $json_data = array(
                    "recordsTotal"    => intval($data),  
                    "recordsFiltered" => intval($data), 
                    "data"            => $data   
        );
        return response()->json($json_data);
    }
}
