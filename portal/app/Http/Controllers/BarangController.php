<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use View;
use Auth;
use DB;
use Bllim\Datatables\Datatables;
use Illuminate\Support\Facades\Response;
use Zain\Helper;
use App\Models\Barang;
use App\Models\BarangHarga;
use App\Models\BarangCode;
use App\Models\MemberCard;
use App\Models\Cabang;
use App\Models\Kategori;
use Redirect;
use App\Models\Stok;
use Zain\Notification;
use App\Models\PembelianDetail;

class BarangController extends Controller
{
    public function index()
    {
        $listCabang = Cabang::all();
        $listCabang = Helper::createSelect($listCabang, 'nama');
        $listKategori = Kategori::all();
        $listKategori = Helper::createSelect($listKategori, 'nama');
        return View::make('barang.index',  compact('listCabang','listKategori'));
    }
    
    public function indexData(Request $request){
        $query = Barang::select('barang.kode','barang.nama','barang.kategori_id',
                'barang.keterangan','barang.updated_at',DB::raw('SUM(IFNULL(bh.harga, 0)) AS harga'),
                DB::raw('SUM(IFNULL(stok, 0)) AS stok'),'barang.id')
                ->leftjoin('stok',  function ($join) use ($request) {
                    $cabang_id = $request->get('cabang_id');
                    $join->on('stok.barang_id','=','barang.id');
                    
                    if($cabang_id){
                        $join->on('stok.cabang_id','=',DB::raw($cabang_id));
                    }
                })
                ->leftjoin(DB::raw("(select avg(bh.harga) as harga,bc.barang_id from barang_code as bc left join barang_harga as bh on bh.barang_code_id = bc.id and bh.jenis = ".BarangHarga::JENIS_UMUM." where bc.is_use= ". BarangCode::IS_USE_BELUM_TERJUAL ." group by bc.barang_id) as bh"),'bh.barang_id','=','barang.id')
                ->groupBy('barang.id');
        
        $search =$request->get('search');
        if($search){
            $query->where(function($q)use($search){
                $like = '%'.$search.'%';
                $q->where('barang.kode','like',$like)
                        ->orWhere('barang.nama','like',$like)
                        ->orWhere('barang.jenis','like',$like)
                        ->orWhere('barang.keterangan','like',$like);
            });
        }
        
        if(!empty($request->get('kategori_id'))){
            $query->where('kategori_id','=',$request->get('kategori_id'));
        }
        
        if(!empty($request->get('brand_id'))){
            $query->where('brand_id','=',$request->get('brand_id'));
        }
                
        $result = Datatables::of($query)
                ->edit_column('kode', function(Barang $v){ return $v->kode; })
                ->edit_column('harga', function(Barang $v){ return Helper::idNumber($v->harga); })
                ->edit_column('kategori_id', function(Barang $v){ return $v->kategori_id ? $v->kategori->nama : null; })
                ->edit_column('updated_at', function(Barang $v){ return Helper::formatDate($v->updated_at, Helper::FORMAT_DATE_ID_LONG_DAY); })
                ->edit_column('id', function(Barang $v){
                    ob_start();
                    
                    if(Auth::user()->hasAbility('barang.show')){
                        ?>
                        <a href="<?php echo route('barang.show', $v->id) ?>" class="btn btn-sm btn-info" title="View">
                            <i class="fa fa-eye"></i>
                        </a>
                        <?php
                    }
                    
                    if(Auth::user()->hasAbility('barang.edit')){
                        ?>
                        <a href="<?php echo route('barang.edit', $v->id) ?>" class="btn btn-sm btn-warning" title="Edit"><i class="fa fa-edit"></i></a>
                        <?php
                    }
                    
                    if(Auth::user()->hasAbility('barang.destroy')){
                        ?>
                        <button onclick="destroy(<?php echo $v->id ?>);" class="btn btn-sm btn-danger" title="Hapus"><i class="fa fa-trash"></i></button>
                        <?php
                    }
                    
                    return ob_get_clean();
                })
                ->make();
        
        return $result;
    }
    
    public function indexDataList(){
        return View::make('barang.list_harga');
    }
    
    public function indexDataListHarga(Request $request){
        $query = Barang::leftjoin('stok',  function ($join) use ($request) {
                    $cabang_id = $request->get('cabang_id');
                    $join->on('stok.barang_id','=','barang.id');
                    
                    if($cabang_id){
                        $join->on('stok.cabang_id','=',DB::raw($cabang_id));
                    }
                })
                        ->leftjoin(DB::raw("(select avg(bh.harga) as harga,bc.barang_id from barang_code as bc left join barang_harga as bh on bh.barang_code_id = bc.id and bh.jenis = ".BarangHarga::JENIS_UMUM." where bc.is_use= ". BarangCode::IS_USE_BELUM_TERJUAL ." group by bc.barang_id) as bh1"),'bh1.barang_id','=','barang.id')
                        ->leftjoin(DB::raw("(select avg(bh.harga) as harga,bc.barang_id from barang_code as bc left join barang_harga as bh on bh.barang_code_id = bc.id and bh.jenis = ".BarangHarga::JENIS_RESELLER_1." where bc.is_use= ". BarangCode::IS_USE_BELUM_TERJUAL ." group by bc.barang_id) as bh2"),'bh2.barang_id','=','barang.id')
                        ->where('stok','>',0)
                        ->groupBy('barang.id');
        
        if(Auth::user()->isAdministrator() || Auth::user()->isAdmin() 
                || Auth::user()->isDeveloper()){
            $query = $query->select(DB::raw('SUM(IFNULL(stok, 0)) AS stok'),'barang.nama',
                DB::raw('SUM(IFNULL(bh1.harga, 0)) AS harga_srp'),
                DB::raw('SUM(IFNULL(bh2.harga, 0)) AS harga_reseller'));
        }else{
            $query = $query->select(DB::raw('SUM(IFNULL(stok, 0)) AS stok'),'barang.nama',
                DB::raw('SUM(IFNULL(bh1.harga, 0)) AS harga_srp'));
        }
                
        $search =$request->get('search');
        
        if($search){
            $like = '%'.$search.'%';
            $query->where('barang.nama','like',$like);
        }
        
        if(!empty($request->get('kategori_id'))){
            $query->where('kategori_id','=',$request->get('kategori_id'));
        }
        
        if(!empty($request->get('brand_id'))){
            $query->where('brand_id','=',$request->get('brand_id'));
        }
                
        $result = Datatables::of($query)
                ->edit_column('stok', function(Barang $v){ return Helper::idNumber($v->stok); })
                ->edit_column('harga_srp', function(Barang $v){ return Helper::idNumber($v->harga_srp); });
        
        if(Auth::user()->isAdministrator() || Auth::user()->isAdmin() 
                || Auth::user()->isDeveloper()){
            $result = $result->edit_column('harga_reseller', function(Barang $v){ return Helper::idNumber($v->harga_reseller); });
        }
        
        return $result->make();
    }

    public function listBarang(Request $request){
        $barang_id = $request->get('barang_id');        
        
        $query = BarangCode::leftJoin('barang_harga as bh_s', function($join){
                    $join->on('bh_s.barang_code_id','=','barang_code.id');
                    $join->on('bh_s.jenis','=',  \DB::raw(BarangHarga::JENIS_UMUM));
                })
                ->leftJoin('barang_harga as bh_r1', function($join){
                    $join->on('bh_r1.barang_code_id','=','barang_code.id');
                    $join->on('bh_r1.jenis','=',  \DB::raw(BarangHarga::JENIS_RESELLER_1));
                })
                ->leftJoin('barang_harga as bh_r2', function($join){
                    $join->on('bh_r2.barang_code_id','=','barang_code.id');
                    $join->on('bh_r2.jenis','=',  \DB::raw(BarangHarga::JENIS_RESELLER_2));
                })
                ->where('barang_id','=',$barang_id)
                ->where('is_use','=',BarangCode::IS_USE_BELUM_TERJUAL);
                
        if(Auth::user()->level == \App\Models\User::LEVEL_ADMINISTRATOR){
            $query->select('imei', 'bh_s.harga_beli', 'bh_s.harga as srp', 'bh_r1.harga as reseller1', 'bh_r2.harga as reseller2');
        }else{
            $query->select('imei');
        }
        
        $result = Datatables::of($query)
                ->edit_column('harga_beli', function(BarangCode $v){
                    return Helper::idNumber($v->harga_beli);
                })
                ->edit_column('srp', function(BarangCode $v){
                    return Helper::idNumber($v->srp);
                })
                ->edit_column('reseller1', function(BarangCode $v){
                    return Helper::idNumber($v->reseller1);
                })
                ->edit_column('reseller2', function(BarangCode $v){
                    return Helper::idNumber($v->reseller2);
                })
                ->make();
        
        return $result;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new Barang();
        return View::make('barang.create',  compact('model'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $model = new Barang();
        $model->fill($input);
        
        if($model->validate() && $model->save()){
            return $model->id;
        }
        
        return '<div>'.  implode('<div></div>', $model->errors).'</div>';
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = Barang::find($id);
        
        $barangCode = $model->barangCode()->get();
        
        if($model->kategori->jenis == \App\Models\Kategori::JENIS_ELEKTRONIK){
            return View::make('barang.show',  compact('model','barangCode'));
        }else{
            $listPembelian = PembelianDetail::where('barang_id','=',$id)->groupBy('pembelian_id')->get();
            
            return View::make('barang.show',  compact('model','barangCode','listPembelian'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Barang::find($id);
        
        return View::make('barang.edit',  compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $model = Barang::find($id);
        $model->fill($input);
        
        if($model->validate() && $model->save()){
            return $model->id;
        }
        
        return '<div>'.  implode('<div></div>', $model->errors).'</div>';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = Barang::find($id);
        
        return $model->delete() ? '1' : 'Data Tidak Bisa Dihapus karena sudah ada transaksi untuk barang ini';
    }
    
    public function calculateStok()
    {
        Notification::addWarning('Process menghitung stok barang');
        
        $listCabang = Cabang::all();
        $ids = Barang::all()->pluck('id')->toArray();
        
        foreach ($listCabang as $c){
            Stok::updateStok($c->id, $ids);
        }
        
        Notification::addSuccess('Process selesai...');
        return Redirect::route('barang.index');
    }


    public function ajaxBarang(Request $request ,$query){
        $tmp = DB::table('barang')
                ->select('barang.nama', 'barang.id','harga_pokok','barang.kode',DB::raw('0 as harga_jual'),
                        'barang.kategori_id','kategori.jenis')
                ->join('kategori','kategori.id','=','barang.kategori_id')
                ->orderBy('nama', 'ASC')
                ->limit(5);
        
        $tmp = Helper::fluentMultiSearch($tmp, $query, 'barang.kode,barang.nama');
        
        $data = $tmp->get();
        
        $res = [];
        foreach ( $data as $v){
            $res[] = [
                'id' => $v->id,
                'value'=> $v->nama,
                'show'=> Barang::mergeBarangName($v->nama, $v->kode),
                'nama'=> $v->nama,
                'harga_pokok'=>(double) $v->harga_pokok,
                'kode'=>$v->kode,
                'harga_jual'=>(double) $v->harga_jual,
                'kategori_id'=>$v->kategori_id,
                'kategori_jenis'=>$v->jenis,
            ];
        }

        return Response::json($res);
    }
    
    public function ajaxBarangCode(Request $request ,$query){
        $tmp = DB::table('barang_code')
                ->select('barang.nama', 'barang.id','harga_pokok','barang.kode',DB::raw('0 as harga_jual'),'serial_number',
                        'imei','barang_code.id as barang_code_id','barang.kategori_id','kategori.jenis')
                ->rightjoin('barang','barang_code.barang_id','=','barang.id')
                ->join('kategori','kategori.id','=','barang.kategori_id')
                ->where('barang_code.is_use', '=', \App\Models\BarangCode::IS_USE_BELUM_TERJUAL)
                ->orderBy('nama', 'ASC')
                ->limit(5);
        
        $member_card_id = $request->get('member_card_id');
        if($member_card_id){
            $tmp = DB::table('barang_code')
                    ->select('barang.nama', 'barang.id','harga_pokok','barang.kode','harga as harga_jual','serial_number',
                            'imei','barang_code.id as barang_code_id','barang.kategori_id','kategori.jenis')
                    ->join('barang','barang_code.barang_id','=','barang.id')
                    ->join('kategori','kategori.id','=','barang.kategori_id')
                    ->leftjoin('barang_harga as bh','bh.barang_code_id','=','barang_code.id')
                    ->where('bh.jenis','=',DB::raw('(SELECT jenis FROM member_card where id='.$member_card_id.')'))
                    ->where('barang_code.is_use', '=', \App\Models\BarangCode::IS_USE_BELUM_TERJUAL)
                    ->orderBy('nama', 'ASC')
                    ->limit(5);
        }
        
        $tmp = Helper::fluentMultiSearch($tmp, $query, 'barang.kode,barang.nama,imei,serial_number');
        
        $data = $tmp->get();
        
        $res = [];
        foreach ( $data as $v){
            $res[] = [
                'id' => $v->id,
                'barang_code_id' => $v->barang_code_id,
                'value'=> $v->nama,
                'show'=> Barang::mergeBarangName($v->nama, $v->kode, $v->imei, $v->serial_number),
                'nama'=> $v->nama,
                'harga_pokok'=>(double) $v->harga_pokok,
                'kode'=>$v->kode,
                'harga_jual'=>(double) $v->harga_jual,
                'serial_number'=>$v->serial_number,
                'imei'=>$v->imei,
                'kategori_id'=>$v->kategori_id,
                'kategori_jenis'=>$v->jenis,
            ];
        }

        return Response::json($res);
    }
    
    public function ajaxBarangHarga(Request $request){
        $barang_code_id = $request->get('barang_code_id');
        $member_card_id = $request->get('member_card_id');
        
        $tmp = DB::table('barang_harga')
                ->select('barang_id', 'harga')
                ->leftJoin('barang_code', 'barang_code.id','=','barang_harga.barang_code_id')
                ->where('barang_code_id', '=', $barang_code_id)
                ->limit(1);
        
        if($member_card_id){
            $member = MemberCard::find($member_card_id);
            if($member){
                $tmp->where('jenis','=',$member->getJenisHarga());
            }
        }

        $data = $tmp->get();
        
        return Response::json($data);
    }
    
    public function ajaxAllBarangHarga(Request $request){
        $barang_code_id = $request->get('barang_code_id');
        
        $tmp = DB::table('barang_harga')
                ->select('barang_id', 'harga','jenis','harga_beli')
                ->leftJoin('barang_code', 'barang_code.id','=','barang_harga.barang_code_id')
                ->where('barang_code_id', '=', $barang_code_id);
        
        $tmp = $tmp->get();
        $data = [];
        foreach ($tmp as $v){
            if($v->jenis == BarangHarga::JENIS_UMUM){
                $data['srp'] = (double) $v->harga;
            }elseif($v->jenis == BarangHarga::JENIS_RESELLER_1){
                $data['reseller1'] = (double) $v->harga;
            }elseif($v->jenis == BarangHarga::JENIS_RESELLER_2){
                $data['reseller2'] = (double) $v->harga;
            }
            
            $data['harga_beli'] = (double) $v->harga_beli;
        }
        
        return Response::json($data);
    }
}
