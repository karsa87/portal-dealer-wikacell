<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Bllim\Datatables\Datatables;
use View;
use Karsa\Helper;
use Illuminate\Http\Request;
use App\Models\Preorder;
use App\Models\Notifications;

class TrackorderController extends Controller {

    public function index()
    {        
        return View::make('trackorder.index');
    }
    
    public function indexData(Request $request)
    {
        $query = Preorder::select('preorder.no_order','preorder.tanggal','preorder.jenis',
                'preorder.tanggal_tempo','preorder.status_order','preorder.status',
                'preorder.total_akhir','preorder.id', 
                \DB::raw("(p.total_akhir - p.saldo_member) as total_akhir2"), "p.no_referensi")
                ->leftJoin("penjualan as p","p.id","=","preorder.penjualan_id")
                ->where('preorder.created_by', \Auth::user()->id)
                ->where('preorder.status_order','!=', Preorder::ORDER_ON_CHART)
                ->where('preorder.status_order','!=', Preorder::ORDER_SAMPAI)
                ->where('preorder.status_order','!=', Preorder::ORDER_SELESAI);
        
        if($request->get('search')){
            $search = $request->get('search');
            $query->where('no_order','like',"%$search%");
        }
        
        if($request->get('status_order')){
            $query->where('status_order',$request->get('status_order'));
        }
        
        if($request->get('tanggal')){
            $tanggal = Helper::formatDate($request->get('tanggal'), Helper::FORMAT_DATE_EN_SHORT);
            $query->where('tanggal',$tanggal);
        }

        return Datatables::of($query)
                ->remove_column("total_akhir2","no_referensi")
                ->edit_column('total_akhir', function(Preorder $v){ 
                    $total = $v->total_akhir;
                    if($v->total_akhir2 != 0){
                        $total = $v->total_akhir2;
                    }
                    return Helper::idNumber($total);
                })
                ->edit_column('tanggal', function(Preorder $v){ return Helper::formatDate($v->tanggal, Helper::FORMAT_DATE_ID_SHORT); })
                ->edit_column('tanggal_tempo', function(Preorder $v){ return Helper::formatDate($v->tanggal_tempo, Helper::FORMAT_DATE_ID_SHORT); })
                ->edit_column('status_order', function(Preorder $v){ return $v->getStatusOrderBadge(); })
                ->edit_column('status', function(Preorder $v){ return $v->getStatusBadge(); })
                ->edit_column('jenis', function(Preorder $v){ return $v->getJenisBadge(); })
                ->edit_column('no_order', function(Preorder $v){
                    return $v->no_referensi ? $v->no_referensi : $v->no_order ;
                })
                ->edit_column('id', function(Preorder $v){
                    ob_start();
                    
                    if(\Auth::user()->hasAbility('trackorder.show')){
                        ?>
                        <a href="<?= route('trackorder.show', $v->id) ?>" class="btn btn-sm btn-info" title="View">
                            <i class="fa fa-eye"></i>
                        </a>
                        <?php
                    }
                    
                    if(\Auth::user()->hasAbility('trackorder.upload') 
                            && ($v->status_order == Preorder::ORDER_WAIT_KONFIRMASI
                            || $v->status_order == Preorder::ORDER_MENUNGGU)){
                        ?>
                        <a href="<?= route('trackorder.upload', $v->id) ?>" class="btn btn-sm btn-warning" 
                           title="Upload">
                            <i class="fa fa-cloud-upload"></i>
                        </a>
                        <?php
                    }
                    
                    return ob_get_clean();
                })
                ->make();
    }
    
    public function show($id)
    {
        $model = Preorder::with([
            'penjualan' => function($q){
                $q->with([
                    'memberCard'=>function($q2){
                        $q2->with('member');
                    },
                    'bank'
                ]);
            }
        ])->where('id',$id)->first();
        
        return View::make('trackorder.show',  compact('model'));
    }
    
    public function upload($id)
    {
        $model = Preorder::find($id);
        return View::make('trackorder.upload',  compact('model'));
    }
    
    public function uploadBukti(Request $request, $id)
    {
        $model = Preorder::find($id);
        $result = [];
        if($request->hasFile('file')){
            $path = "images/preorder/{$model->id}/";
            $files = $request->file('file');
            
            $bukti = [];
            foreach ($files as $file){
                $rs = Helper::uploadGambar($file, $path);
            
                $bukti = json_decode($model->bukti, TRUE);
                if($rs){
                    $bukti[] = $rs;
                    $model->bukti = json_encode($bukti);
                    $model->save();
                }
            }
            
            $result = $bukti;
        }elseif(count(json_decode($model->bukti, TRUE)) <= 0){
            \Karsa\Notification::addDanger("Upload proof of transfer");
            
            return \Redirect::back();
        }
        
        if(!$request->ajax()){
            $model->status_order = Preorder::ORDER_MENUNGGU;
            $model->save();
            
            Notifications::addNotification(
                \Auth::user()->id, 
                \App\Models\User::LEVEL_ADMIN, 
                'Pesanan Preorder #'.$model->no_order.' upload bukti', 
                "preorder.show",
                ["id"=>$model->id]
            );
            
            return \Redirect::route('trackorder');
        }else{
            return \Response::json($result);
        }
    }
    
    public function deleteBuktiAll(Request $request, $id)
    {
        $model = Preorder::find($id);
        $bukti = json_decode($model->bukti, true);
        foreach ($bukti as $b){
            $dir = base_path('../'.$b);
            unlink($dir);
        }
        $model->bukti = "[]";
        
        return $model->save() ? '1' : '0';
    }
}