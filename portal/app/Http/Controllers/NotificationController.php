<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Bllim\Datatables\Datatables;
use View;
use Karsa\Helper;
use Illuminate\Http\Request;
use App\Models\Notifications;

class NotificationController extends Controller {

    public function index()
    {
        Notifications::changeRead([
            'is_read'   => Notifications::IS_NOT_READ,
            'to'        => \Auth::user()->id
        ]);
        return View::make('notification.index');
    }
    
    public function indexData(Request $request)
    {
        $query = Notifications::select('message','id')
                ->where([
                            'to'        => \Auth::user()->id
                        ]);
        
        return Datatables::of($query)->remove_column('id')->make();
    }
    
    public function show($id)
    {
        $model = Notifications::find($id);
        $model->read();
        return \Redirect::route($model->route, json_decode($model->parameter, true));
    }
}