<?php

namespace App\Http\Middleware;

use Auth;
use Closure;
use Response;
use Route;

class AccessListControl
{

    public function handle($request, Closure $next)
    {
        if( !Auth::user()->hasAbility( Route::currentRouteName() ) ){
            return Response::view('layout.403', [], 403);
        }

        return $next($request);
    }

}
