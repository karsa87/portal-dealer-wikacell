<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['web'] ], function() {
    Route::get('/',         ['as' => 'auth.index',  'uses' => 'AuthController@index']);
    Route::get('login',     ['as' => 'auth.index',  'uses' => 'AuthController@index']);
    Route::post('login',    ['as' => 'auth.login',  'uses' => 'AuthController@login']);
    Route::get('logout',    ['as' => 'auth.logout', 'uses' => 'AuthController@logout']);
});

Route::group(['middleware' => ['web','auth','acl'] ], function() {
    /*
     * Dashboard
     */
    Route::get('dashboard/indexData', ['as' => 'dashboard.index.data', 'uses' => 'DashboardController@indexData']);
    Route::get('dashboard', ['as'=>'dashboard', 'uses'=>'DashboardController@index']);

    /*
     * Form preorder
     */
    Route::get('preorder/indexData', ['as' => 'preorder.index.data', 'uses' => 'PreorderController@indexData']);
    Route::get('preorder/checkout', ['as' => 'preorder.checkout', 'uses' => 'PreorderController@checkout']);
    Route::get('preorder/payment', ['as' => 'preorder.payment', 'uses' => 'PreorderController@payment']);
    Route::post('preorder/updateListPreorder', ['as' => 'preorder.update.list_preorder', 'uses'=>"PreorderController@updateListPreorder"]);
    Route::post('preorder/updatePreorder', ['as' => 'preorder.update.preorder', 'uses' => 'PreorderController@updatePreorder']);
    Route::post('preorder/deleteList', ['as' => 'preorder.delete.list', 'uses'=>"PreorderController@deleteList"]);
    Route::resource('preorder','PreorderController');

     /*
     * Trackorder
     */
    Route::get('trackorder/indexData', ['as' => 'trackorder.index.data', 'uses' => 'TrackorderController@indexData']);
    Route::put('trackorder/upload/{id}', ['as'=>'trackorder.upload', 'uses'=>'TrackorderController@uploadBukti']);
    Route::post('trackorder/upload/{id}', ['as'=>'trackorder.upload', 'uses'=>'TrackorderController@uploadBukti']);
    Route::post('trackorder/deleteAll/{id}', ['as'=>'trackorder.deleteAll', 'uses'=>'TrackorderController@deleteBuktiAll']);
    Route::get('trackorder/upload/{id}', ['as'=>'trackorder.upload', 'uses'=>'TrackorderController@upload']);
    Route::get('trackorder', ['as'=>'trackorder', 'uses'=>'TrackorderController@index']);
    Route::get('trackorder/{trackorder}', ['as'=>'trackorder.show', 'uses'=>'TrackorderController@show']);
    
     /*
     * Order
     */
    Route::get('order/indexData', ['as' => 'order.index.data', 'uses' => 'OrderController@indexData']);
    Route::resource('order','OrderController');
    
    /**
     * Saldo
     * **/
    Route::get('profile/saldo/indexData', ['as' => 'profile.saldo.data', 'uses' => 'MemberController@indexDataSaldo']);
    Route::get('profile/saldo', ['as' => 'profile.saldo', 'uses' => 'MemberController@saldo']);
    Route::resource('profile','MemberController');
    
     /*** Portal Promo ***/
    Route::get('portal_promo/indexData', [
        'as'=>'portal_promo.index.data', 'uses'=>'PortalPromoController@indexData'
    ]);
    Route::resource('portal_promo','PortalPromoController');
    
     /*
     * Notification
     */
    Route::get('notification/indexData', ['as' => 'notification.index.data', 'uses' => 'NotificationController@indexData']);
    Route::get('notification', ['as'=>'notification.index', 'uses'=>'NotificationController@index']);
    Route::get('notification/{id}', ['as'=>'notification.show', 'uses'=>'NotificationController@show']);
});
