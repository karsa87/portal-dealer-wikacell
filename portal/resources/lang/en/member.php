<?php

return [
    
    '_title'                => 'Profile',
    
    'nama'                  =>'Full Name',
    'alamat'                =>'Address',
    'telepon'               =>'Telephone',
    'tgl_lahir'             =>'Birthdate',
    'tempat_lahir'          =>'Place Of Birth',
    'member_card'           =>'Member Card Detail',
    'jenis_id'              =>'Identity Type',
    'no_id'                 =>'Identity Number',
    'user_id'               =>'User',
    'no_kartu'              =>'Member Card Number',
    'max_piutang'           =>'Max Receivables',
    'saldo_piutang'         =>'Saldo Receivables',
    'jumlah_tempo'          =>'Amount Of Tempo',
    'poin'                  =>'Point',
    'member_id'             =>'Member',
    'jenis'                 =>'Type'
];
