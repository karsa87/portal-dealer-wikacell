<?php

return [
    
    '_title'                    => 'Barang',
    
    'barcode'                   => 'Barcode',
    'kode'                      => 'Code',
	'nama'                      => 'Name',
    'jenis'                     => 'Tyoe',
    'kategori_id'               => 'Category',
    'brand_id'                  => 'Brand',
    'poin'                      => 'Point',
    'harga_pokok'               => 'Cost Goods',
    'keterangan'                => 'Description',
    'status'                    => 'Status',
    'harga'                     => 'Cost',
];
