<?php

return [
    
    '_title'                    => 'Portal Promo',
    
    'no_promo'                  => 'Promo Number',
    'nama'                      => 'Name',
    'start'                     => 'Start',
    'end'                       => 'End',
    'keterangan'                => 'Description',
];
