<?php

return [
    
    '_title'                => 'Dashboard',
    
    'no_order'              => 'No Order',
    'tanggal'               => 'Date',
	'member_card_id'        => 'Member',
    'jenis'                 => 'Type',
    'tanggal_tempo'         => 'Due Date',
    'bank_id'               => 'Bank',
    'total_awal'            => 'Total Start',
    'total_potongan'        => 'Total Pieces',
    'biaya_lain'            => 'Other Cost',
    'total_akhir'           => 'Final',
    'status'                => 'Status',
    'status_order'          => 'Order Status',
    'penjualan_id'          => 'Order',
    
    'barang_id'             => 'Product',
    'harga_jual'            => 'Sell Price',
    'jumlah'                => 'Quantity',
    'total_awal'            => 'Total Start',
    'diskon'                => 'Diskon',
    'potongan'              => 'Piece',
    'promo_id'              => 'Promo',
    'total_potongan'        => 'Total Piece',
    'total_akhir'           => 'Total End'
];
