<?php

return [
    
    '_title'                    => 'Portal Promo Line',
    
    'portal_promo_id'           => 'Portal Promo',
    'barang_id'                 => 'Product',
    'min_jumlah'                => 'Min Amount',
    'jenis'                     => 'Type',
    'potongan_persen'           => 'Pieces Percent (%)',
    'potongan_ribuan'           => 'Thousand Pieces (Rp)',
    'jumlah'                    => 'Amount',
    'bonus_barang_id'           => 'Bonus Goods'
];
