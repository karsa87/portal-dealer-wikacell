<?php

return [
    
    '_title'                    => 'Portal Promo Line',
    
    'portal_promo_id'           => 'Portal Promo',
    'barang_id'                 => 'Barang',
    'min_jumlah'                => 'Min Jumlah',
    'jenis'                     => 'Jenis',
    'potongan_persen'           => 'Potongan Persen (%)',
    'potongan_ribuan'           => 'Potongan Ribuan (Rp)',
    'jumlah'                    => 'Jumlah',
    'bonus_barang_id'           => 'Bonus Barang'
];
