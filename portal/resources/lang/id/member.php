<?php

return [
    
    '_title'                => 'Profile',
    
    'nama'                  =>'Nama Lengkap',
    'alamat'                =>'Alamat Lengkap',
    'telepon'               =>'Telepon',
    'tgl_lahir'             =>'Tanggal Lahir',
    'tempat_lahir'          =>'Tempat Lahir',
    'member_card'           =>'Kartu Member Detail',
    'jenis_id'              =>'Jenis Identitas',
    'no_id'                 =>'No Identitas',
    'user_id'               =>'User',
    'no_kartu'              =>'No Kartu Member',
    'max_piutang'           =>'Max Piutang',
    'saldo_piutang'         =>'Saldo Piutang',
    'jumlah_tempo'          =>'Jumlah Tempo',
    'poin'                  =>'Poin',
    'member_id'             =>'Member',
    'jenis'                 =>'Jenis'
];
