<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines contain the default error messages used by
	| the validator class. Some of these rules have multiple versions such
	| such as the size rules. Feel free to tweak each of these messages.
	|
	*/

	"accepted"         => "Kolom :attribute harus be accepted.",
	"active_url"       => "Kolom :attribute adalah URL yang tidak valid.",
	"after"            => "Kolom :attribute harus berisi tanggal setelah kolom :date.",
	"alpha"            => "Kolom :attribute hanya boleh berisi huruf.",
	"alpha_dash"       => "Kolom :attribute hanya boleh berisi huruf, angka, dan dash.",
	"alpha_num"        => "Kolom :attribute hanya boleh berisi huruf dan angka.",
	"array"            => "Kolom :attribute harus berisi sebuah array.",
	"before"           => "Kolom :attribute harus berisi tanggal sebelum kolom :date.",
	"between"          => array(
		"numeric" => "Kolom :attribute harus diantara :min - :max.",
		"file"    => "Kolom :attribute harus diantara :min - :max kilobytes.",
		"string"  => "Kolom :attribute harus diantara :min - :max karakter.",
		"array"   => "Kolom :attribute harus memiliki antara :min - :max item.",
	),
	"confirmed"        => "Kolom konfirmasi :attribute tidak cocok.",
	"date"             => "Kolom :attribute adalah tanggal yang tidak valid.",
	"date_format"      => "Kolom :attribute tidak cocok dengan format :format.",
	"different"        => "Kolom :attribute dan :other harus berbeda.",
	"digits"           => "Kolom :attribute harus berisi :digits digit.",
	"digits_between"   => "Kolom :attribute harus diantara :min dan :max digit.",
	"email"            => "Kolom :attribute harus berisi email yang valid.",
	"exists"           => "Kolom selected :attribute is invalid.",
	"image"            => "Kolom :attribute harus be an image.",
	"in"               => "Kolom selected :attribute is invalid.",
	"integer"          => "Kolom :attribute harus berisi bilangan bulat.",
	"ip"               => "Kolom :attribute harus be a valid IP address.",
	"max"              => array(
		"numeric" => "Kolom :attribute tidak boleh lebih dari :max.",
		"file"    => "Kolom :attribute tidak boleh lebih dari :max kilobytes.",
		"string"  => "Kolom :attribute tidak boleh lebih dari :max karakter.",
		"array"   => "Kolom :attribute tidak boleh memiliki lebih dari :max item.",
	),
	"mimes"            => "Kolom :attribute harus be a file of type: :values.",
	"min"              => array(
		"numeric" => "Kolom :attribute harus berisi minimal :min.",
		"file"    => "Kolom :attribute harus berisi minimal :min kilobytes.",
		"string"  => "Kolom :attribute harus berisi minimal :min karakter.",
		"array"   => "Kolom :attribute harus memiliki minimal :min item.",
	),
	"not_in"           => "Kolom selected :attribute is invalid.",
	"numeric"          => "Kolom :attribute harus berisi angka.",
	"regex"            => "Kolom :attribute format is invalid.",
	"required"         => "Kolom :attribute harus diisi.",
	"required_if"      => "Kolom :attribute harus diisi jika kolom :other berisi :value.",
	"required_with"    => "Kolom :attribute harus diisi jika kolom :values ada.",
	"required_without" => "Kolom :attribute harus diisi jika kolom :values tidak ada.",
	"same"             => "Kolom :attribute dan :other harus sama.",
	"size"             => array(
		"numeric" => "Kolom :attribute harus berukuran :size.",
		"file"    => "Kolom :attribute harus berukuran :size kilobytes.",
		"string"  => "Kolom :attribute harus berukuran :size karakter.",
		"array"   => "Kolom :attribute harus mengandung :size item.",
	),
	"unique"           => "Kolom :attribute sudah digunakan.",
	"url"              => "Format kolom :attribute tidak valid.",

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| Here you may specify custom validation messages for attributes using the
	| convention "attribute.rule" to name the lines. This makes it quick to
	| specify a specific custom language line for a given attribute rule.
	|
	*/

	'custom' => array(),

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Attributes
	|--------------------------------------------------------------------------
	|
	| The following language lines are used to swap attribute place-holders
	| with something more reader friendly such as E-Mail Address instead
	| of "email". This simply helps us make messages a little cleaner.
	|
	*/

	'attributes' => array(),

);
