<?php

return [
    
    '_title'                    => 'Portal Promo',
    
    'no_promo'                  => 'No Promo',
    'nama'                      => 'Nama',
    'start'                     => 'Mulai',
    'end'                       => 'Sampai',
    'keterangan'                => 'Keterangan',
];
