<?php

return [
    
    '_title'                    => 'Barang',
    
    'barcode'                   => 'Kode Batang',
    'kode'                      => 'Kode',
	'nama'                      => 'Nama',
    'jenis'                     => 'Jenis',
    'kategori_id'               => 'Kategori',
    'brand_id'                  => 'Merek',
    'poin'                      => 'Poin',
    'harga_pokok'               => 'Harga Pokok',
    'keterangan'                => 'Keterangan',
    'status'                    => 'Status',
    'harga'                     => 'Harga',
];
