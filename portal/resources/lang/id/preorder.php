<?php

return [
    
    '_title'                => 'Dashboard',
    
    'no_order'              => 'No Order',
    'tanggal'               => 'Tanggal',
	'member_card_id'        => 'Member',
    'jenis'                 => 'Jenis',
    'tanggal_tempo'         => 'Tanggal Tempo',
    'bank_id'               => 'Bank',
    'total_awal'            => 'Total Awal',
    'total_potongan'        => 'Total Potongan',
    'biaya_lain'            => 'Biaya Lain',
    'total_akhir'           => 'Total Akhir',
    'status'                => 'Status',
    'status_order'          => 'Status Order',
    'penjualan_id'          => 'Penjualan',
    'preorder_id'           => 'Preorder',
    
    'barang_id'             => 'Barang',
    'harga_jual'            => 'Harga Jual',
    'jumlah'                => 'Jumlah',
    'total_awal'            => 'Total Awal',
    'diskon'                => 'Diskon',
    'potongan'              => 'Potongan',
    'promo_id'              => 'Promo',
    'total_potongan'        => 'Total Potongan',
    'total_akhir'           => 'Total Akhir'
    
];
