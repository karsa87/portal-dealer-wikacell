<?php
use Karsa\Helper;
use App\Models\Preorder;
?>

@extends('layout.default')

@section('title')
Dashboard
@endsection

@section('content')
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
    <div class="row">
        <div class="col-lg-4">
            <a href="<?= route('trackorder') ?>" class="btn btn-sm btn-success">Back</a>
            <?php if(\Auth::user()->hasAbility('trackorder.upload') 
                    && ($model->status_order == Preorder::ORDER_WAIT_KONFIRMASI
                    || $model->status_order == Preorder::ORDER_MENUNGGU)) :
                ?>
                <a href="<?= route('trackorder.upload', $model->id) ?>" class="btn btn-sm btn-warning" 
                   title="Upload">
                    <i class="fa fa-cloud-upload"></i>
                    Upload
                </a>
            <?php endif; ?>
        </div>
    </div>
    <?php if($model->penjualan_id) : $penjualan = $model->penjualan; ?>
    <h3 style="color:blue;">Detail Penjualan</h3>

    <div class="row">
        <div class="col-lg-5">
            <div class="box panel-blue">
                <div class="box-header">
                    <h3 class="box-title">Detail Penjualan</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">                        
                        <div class="col-md-4">
                            <span>No Nota</span><br>
                            <?= $penjualan->no_referensi ?>
                        </div>
                        <div class="col-md-4">
                            <span>Tanggal</span><br>
                            <?= Helper::formatDate($penjualan->tgl_penjualan, Helper::FORMAT_DATE_ID_LONG) ?>
                        </div>
                        <div class="col-md-12">
                            <div class="v-border"></div>
                        </div>
                    </div>
                    <div class="row">                        
                        <div class="col-md-4">
                            <span>Member</span><br>
                            <?= $penjualan->memberCard->member->nama ?>
                        </div>
                        <div class="col-md-4">
                            <span>Alamat</span><br>
                            <?= $penjualan->memberCard->member->alamat ?>
                        </div>
                        <div class="col-md-12">
                            <div class="d-border"></div>
                        </div>
                    </div>
                    <div class="row">                        
                        <div class="col-md-4">
                            <span>Bank</span><br>
                            <?= $penjualan->bank_id ? $penjualan->bank->nama : "-" ?>
                        </div>
                        <div class="col-md-12">
                            <div class="d-border"></div>
                        </div>
                    </div>
                    <div class="row">                        
                        <div class="col-md-4">
                            <span>Tanggal Tempo</span><br>
                            <?= Helper::formatDate($penjualan->tgl_tempo, Helper::FORMAT_DATE_ID_LONG) ?>
                        </div>
                        <div class="col-md-4">
                            <span>Jenis</span><br>
                            <?= $penjualan->getJenisBadge() ?>
                        </div>
                        <div class="col-md-4">
                            <span>Status</span><br>
                            <?= $penjualan->getStatusBadge() ?>
                        </div>                        
                    </div>
                </div>

            </div>
        </div>

        <div class="col-lg-5">
            <div class="box panel-green">
                <div class="box-header">
                    <h3 class="box-title">Total Penjualan</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table width="100%">
                        <tr>
                            <td align="right" width="50%"><span>Total(Sebelum Potongan)</span></td>
                            <td width="5%"></td>
                            <td><?= $penjualan->total_awal ? Helper::idNumber($penjualan->total_awal):null ?></td>
                        </tr>
                        <tr>
                            <td align="right"><span>Voucher</span></td>
                            <td></td>
                            <td><?= $penjualan->voucher_id ? $penjualan->voucher->nama : null ?></td>
                        </tr>
                        <tr>
                            <td align="right"><span>Potongan Voucher(Rp)</span></td>
                            <td></td>
                            <td><?= Helper::idNumber($penjualan->potongan_voucher) ?></td>
                        </tr>
                        <tr>
                            <td align="right"><span>Potongan Sales</span></td>
                            <td></td>
                            <td><?= Helper::idNumber($penjualan->potongan_sales) ?></td>
                        </tr>
                        <tr>
                            <td align="right"><span>Ongkir</span></td>
                            <td></td>
                            <td><?= Helper::idNumber($penjualan->ongkir) ?></td>
                        </tr>
                        <tr>
                            <td align="right"><span>Total(Setelah Potongan)</span></td>
                            <td></td>
                            <td><?= $penjualan->total_akhir ? Helper::idNumber($penjualan->total_akhir):null ?></td>
                        </tr>
                        <tr>
                            <td align="right"><span>Bayar</span></td>
                            <td></td>
                            <td><?= $penjualan->uang_muka ? Helper::idNumber($penjualan->uang_muka - $penjualan->saldo_member):null ?></td>
                        </tr>
                        <tr>
                            <td align="right"><span>Saldo</span></td>
                            <td></td>
                            <td><?= $penjualan->saldo_member ? Helper::idNumber($penjualan->saldo_member):null ?></td>
                        </tr>
                        <tr>
                            <td align="right"><span>Terbayar</span></td>
                            <td></td>
                            <td><?= $penjualan->terbayar ? Helper::idNumber($penjualan->terbayar):null ?></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>        
    </div>
    
    <div class="row">
        <div class="col-md-12">
            <div class="theadshow">
                Daftar Barang
            </div>
            <table class="table table-hover table-bordered" style="background-color: #fff;">
                <thead class="bg-gray">                        
                    <th>Nama Barang</th>
                    <th>Imei</th>
                    <th>Jumlah</th>
                    <th>Harga Jual</th>
                    <th>Promo</th>
                    <th>Discount(%)</th>
                    <th>Harga Akhir</th>
                </thead>
                <tbody>
                    <?php $total = 0; ?>
                    <?php foreach ($penjualan->details as $detail) : ?>
                    <tr>
                        <td><?= $detail->barang->nama ?></td>
                        <td><?= $detail->barangCode->imei ?></td>
                        <td class="text-right"><?= Helper::idNumber($detail->jumlah) ?></td>
                        <td class="text-right"><?= Helper::idNumber($detail->harga_jual) ?></td>
                        <td class="text-right"><?= $detail->promo_id ? $detail->promo->nama : null ?></td>
                        <td class="text-right"><?= Helper::idNumber($detail->potongan) ?></td>
                        <td class="text-right"><?= Helper::idNumber($detail->harga_akhir) ?></td>
                    </tr>
                    <?php $total += $detail->harga_akhir; ?>
                    <?php endforeach ?>
                </tbody>                        
                <tfoot class="bg-gray">
                    <tr>
                        <td colspan="6" class="text-right"><b>Total</b></td>
                        <td class="text-right"><?= Helper::idNumber($total) ?></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
    <?php endif; ?>
    <hr style="border-color: black; ">
    <div class="row">
        <div class="col-md-8">
          <div class="box">
            <div class="box-header bg-aqua">
                <h3 class="box-title">Checkout</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
                <?php
                    $no = 1;
                    $total_qty = 0;
                    $total_awal = 0;
                    $total_akhir = 0;
                    $total_potongan = 0;
                ?>
                <table class="table table-hover" id="mainTable">
                    <thead>
                        <th class="text-center">No</th>
                        <th>Barang</th>
                        <th class="text-center">Qty</th>
                        <th class="text-center">Harga Satuan</th>
                        <th class="text-center">Potongan</th>
                        <th class="text-center">Total</th>
                    </thead>
                    <tbody>
                        <?php foreach ($model->detail as $k => $d) : 
                            $c = "";
                            if($d->pivot->status == Preorder::LINE_STATUS_BONUS){
                                $c = "<span class='badge bg-red'><small>bonus</small></span>";
                            }
                        ?>
                        <tr>
                            <td class="text-center"><?= $no++; ?></td>
                            <td><?= sprintf("%s %s",$d->nama,$c) ?></td>
                            <td class="text-center"><?= Helper::idNumber($d->pivot->jumlah) ?></td>
                            <td align="right">Rp. <?= Helper::idNumber($d->pivot->harga_jual) ?></td>
                            <td align="right">Rp. <?= Helper::idNumber($d->pivot->total_potongan) ?></td>
                            <td align="right">Rp. <?= Helper::idNumber($d->pivot->total_awal) ?></td>
                        </tr>
                        <?php $total_qty += $d->pivot->jumlah ?>
                        <?php $total_awal += $d->pivot->total_awal ?>
                        <?php $total_akhir += $d->pivot->total_akhir ?>
                        <?php $total_potongan += $d->pivot->total_potongan ?>
                        <?php endforeach; ?>
                    </tbody>
                    <tfoot class="bg-gray" style="opacity: .50">
                        <tr>
                            <td colspan="5" class="text-right"><b>Sub Total : </b></td>
                            <td align="right">Rp. <?= Helper::idNumber($total_awal) ?></td>
                        </tr>
                        <tr>
                            <td colspan="5" class="text-right"><b>Total Potongan : </b></td>
                            <td align="right">Rp. <?= Helper::idNumber($total_potongan) ?></td>
                        </tr>
                        <tr>
                            <td colspan="5" class="text-right"><b>Total : </b></td>
                            <td align="right">Rp. <?= Helper::idNumber($total_akhir) ?></td>
                        </tr>
                    </tfoot>                
                </table>
            </div>
          </div>
        </div>
        <div class="col-lg-4">
            <div class="box">
                <div class="box-header bg-blue">
                    <h3 class="box-title">Informasi Toko</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive">
                    <table width="100%">
                        <tr>
                            <td width="20%">Delivery To</td>
                            <td width="3%">:</td>
                            <td><?= $model->memberCard->member->nama ?></td>
                        </tr>
                        <tr>
                            <td>Alamat</td>
                            <td>:</td>
                            <td><?= $model->memberCard->member->alamat ?></td>
                        </tr>
                        <tr>
                            <td>Telp</td>
                            <td>:</td>
                            <td><?= $model->memberCard->member->telepon ?></td>
                        </tr>
                        <?php if($model->penjualan_id) : ?>
                        <tr>
                            <td>No Nota</td>
                            <td>:</td>
                            <td><?= $model->penjualan->no_referensi ?></td>
                        </tr>
                        <?php endif; ?>
                        <tr>
                            <td>No Order</td>
                            <td>:</td>
                            <td><?= $model->no_order ?></td>
                        </tr>
                        <tr>
                            <td>Status</td>
                            <td>:</td>
                            <td><?= $model->getStatusOrderBadge() ?></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <!-- /.col -->
    </div>
    
    <div class="row">
        <div class="col-lg-12">
            <div class="box panel-green">
                <div class="box-header">
                    <h3 class="box-title">Bukti Transfer</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row" style="display: inline-block; margin-top: 20px;" id="div-bukti">
                        <?php 
                            $model->bukti = (isset($model->bukti)) ? $model->bukti : "[]";
                        ?>
                        <?php foreach (json_decode($model->bukti, TRUE) as $k => $bukti) : ?>
                        <div class="col-sm-4 col-md-2 img-bukti">
                            <a class="thumbnail fancybox" rel="ligthbox" href="<?= asset('/').$bukti ?>">
                                <img class="img-responsive" alt="" src="<?= asset('/').$bukti ?>" />
                                <div class='text-right'>
                                    <small class='text-muted'>Image Title</small>
                                </div> <!-- text-right / end -->
                            </a>
                        </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->
</section>
    <!-- /.content -->
</div>
@endsection

@section('js-include')
<script type="text/javascript" src="<?= asset('vendor/') ?>/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?= asset('vendor/') ?>/jquery.datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?= asset('vendor/') ?>/jquery.datatables/jquery.dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="<?= asset('vendor/') ?>/assets/js/jquery.fancybox.min.js"></script>
@endsection

@section('css-include')
<link rel="stylesheet" href="<?= asset('vendor/') ?>/assets/css/jquery.fancybox.min.css" />
@endsection 


@section('js-inline')
//<script type="text/javascript">
$(function(){
    $(".fancybox").fancybox({
        openEffect: "none",
        closeEffect: "none"
    });
});
@endsection
