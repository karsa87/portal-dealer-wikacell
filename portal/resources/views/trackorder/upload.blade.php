@extends('layout/default')

@section('title')
Upload Bukti
@endsection

@section('breadcrumb')
<li>
    <a href="<?= route('trackorder')?>">Upload Bukti</a>
</li>
<li>
    Create
</li>
@endsection

@section('content-header')
Upload Bukti
@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="content-wrapper">    
            <!-- Main content -->
            <section class="content">
                <?php if(count(json_decode($model->bukti, TRUE)) > 0) : ?>
                <div class="row">
                    <div class="col-sm-4 col-md-2">
                        <a href="jacascript:void(0)" class="btn btn-block btn-danger" 
                           onclick="deleteAll()">
                            <span class="fa fa-trash"></span> <font>Delete All</font>
                        </a>
                    </div>
                </div>
                <div class="row" style="display: inline-block; margin-top: 20px;" id="div-bukti">
                    <?php foreach (json_decode($model->bukti, TRUE) as $k => $bukti) : ?>
                    <div class="col-sm-4 col-md-2 img-bukti">
                        <a class="thumbnail fancybox" rel="ligthbox" href="<?= asset('/').$bukti ?>">
                            <img class="img-responsive" alt="" src="<?= asset('/').$bukti ?>" />
                            <div class='text-right'>
                                <small class='text-muted'>Image Title</small>
                            </div> <!-- text-right / end -->
                        </a>
                    </div>
                    <?php endforeach; ?>
                </div>
                <?php endif; ?>
                <div class="row">
                    <div class="col-lg-12">
                        <?= Form::model($model, [
                            'route' => ['trackorder.upload', $model->id], 
                            'id'=>'mainForm', 
                            'method'=>'PUT',
                            'files'=>true,
                        ]) ?>
                        <?= csrf_field() ?>
                        <div class="form-group">
                            <div id="upload"></div>
                        </div>
                        <div class="form-group text-right">
                            <a href="<?= route('trackorder') ?>" class="btn btn-cancel">Back</a>
                            <button class="btn btn-checkout">Upload</button>
                        </div>
                        <?= Form::close() ?>
                    </div>
                </div>
            </section>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js-include')
<script type="text/javascript" src="<?= asset('vendor/') ?>/utils/dropzone.js"></script>
<script type="text/javascript" src="<?= asset('vendor/') ?>/assets/js/jquery.fancybox.min.js"></script>
@endsection

@section('css-include')
<link rel="stylesheet" href="<?= asset('vendor/') ?>/utils/dropzone.css" />
<link rel="stylesheet" href="<?= asset('vendor/') ?>/assets/css/jquery.fancybox.min.css" />
@endsection 

@section('js-inline')
//<script type="text/javascript">
function deleteAll(){
    $.post("<?= route('trackorder.deleteAll',$model->id) ?>", {_token: '<?= csrf_token() ?>'},
    function(result){
        if(result == '1'){
            showSuccessGritter("Berhasil hapus bukti");
            $('.img-bukti').remove();
        }else{
            showDangerGritter("Gagal hapus bukti");
        }
    }).fail(function(){
        console.log("error");
    });
}
$(function(){
    $("div#upload").addClass("dropzone").dropzone({
        url: "<?= route('trackorder.upload', $model->id) ?>",
        params: {
            _token: "<?= csrf_token() ?>"
        },
        maxFilesize: 2, // MB
        maxFiles: 5, 
        parallelUploads: 5,
        uploadMultiple: true,
        capture: "camera", 
        dictDefaultMessage: "Mohon melakukan konfirmasi Pembayaran, jika telah melakukan transfer, dengan cara menghubungi tim Sales kami atau Mengupload Bukti transfer pada Portal",
        success: function(file, response){
            $('.img-bukti').remove();
            $.each(response, function(a,b){
                var div_bukti = '<div class="col-sm-4 col-md-2 img-bukti">'
                +'<a class="thumbnail fancybox" rel="ligthbox" href="<?= asset('/') ?>'+b+'">'
                +'<img class="img-responsive" alt="" src="<?= asset('/') ?>'+b+'" />'
                +'<div class="text-right">'
                +'<small class="text-muted">Bukti</small>'
                +'</div>'
                +'</a>'
                +'</div>';
                
                $("#div-bukti").append(div_bukti);
            });
        }
    });
    
    $(".fancybox").fancybox({
        openEffect: "none",
        closeEffect: "none"
    });
});
@endsection