<?php
use App\Models\Notifications;
?>

@extends('layout.default')

@section('title')
Dashboard
@endsection

@section('content')
<div class="content-wrapper">    
    <!-- Main content -->
    <section class="content">    
    <div class="row">
        <div class="col-md-8">
          <div class="box">
            <div class="box-header bg-aqua">
                <h3 class="box-title"><?= Notifications::getAttributeName('message') ?></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
                <p><?= $model->message ?></p>
                <p class="text-right"><a href="<?= route('notification.index') ?>" class="btn-checkout">Back</a></p>
            </div>
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection