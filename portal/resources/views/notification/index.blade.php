
<?php
use Karsa\Helper;
use App\Models\Notifications;
?>

@extends('layout.default')

@section('title')
Notification
@endsection

@section('content')
<div class="content-wrapper">    
    <!-- Main content -->
    <section class="content">
    <div class="row">
        <div class="col-md-9">
          <div class="box">
            <div class="box-header">
                <h3 class="box-title">Notifications</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
              <table class="table table-hover" id="mainTable">
                <thead>
                    <th class="column-padding-right"><?= Notifications::getAttributeName('message') ?></th>
                </thead>
              </table>
            </div>
          </div>
        </div>        
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection

@section('js-include')
<script type="text/javascript" src="<?= asset('vendor/') ?>/jquery.datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?= asset('vendor/') ?>/jquery.datatables/jquery.dataTables.bootstrap.min.js"></script>
@endsection

@section('js-inline')
//<script type="text/javascript">
$(function(){
    mainTable = $('#mainTable').dataTable({
        "aoColumns": [ 
            { },
        ],
        "stateSave": true,
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": "<?= route('notification.index.data')?>",
        "sServerMethod": "GET",
        "aaSorting": [[ 0, "asc" ]],
        "bLengthChange": false,
        "bFilter": false,
        "iDisplayRow": 25,
//        "iDisplayLength": 50,
//        "pageLength" : 25,
        "aLengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]],
        "fnServerParams": function ( aoData ) {
            aoData.push( { "name": "_token", "value": '<?=  csrf_token() ?>' } );
        }
    });
})
@endsection
