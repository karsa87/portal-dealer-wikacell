<?php

use App\Models\Member;
use App\Models\MemberCard;
use App\Models\User;
use Karsa\Helper;

/* @var $model Member */

?>
<div class="row">
    <div class="col-md-5">
        <div class="panel panel-default">
            <div class="panel-heading">Data Member</div>
            <div class="panel-body">
                <div class="row">                        
                    <div class="form-group col-lg-5">
                        <?= Form::label('jenis_id', Member::getAttributeName('jenis_id')) ?>
                        <dd><?= $model->getJenisId() ?></dd>
                    </div>
                    <div class="form-group col-lg-5">
                        <?= Form::label('no_id', Member::getAttributeName('no_id')) ?>
                        <dd><?= $model->no_id ?></dd>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">                       
                        <p class="ket">Isikan data profile sesuai kartu identitas</p>                        
                    </div>
                </div>
                <div class="row">                        
                    <div class="form-group col-lg-10">
                        <?= Form::label('nama', Member::getAttributeName('nama')) ?>
                        <dd><?= $model->nama ?></dd>
                    </div>
                </div>
                <div class="row">                        
                    <div class="form-group col-lg-5">
                        <?= Form::label('tempat_lahir', Member::getAttributeName('tempat_lahir')) ?>
                        <?= Form::text('tempat_lahir', null,['class'=>'form-control','placeholder'=>'e.x : Surabaya']) ?>
                    </div>
                    <div class="form-group col-lg-5">
                        <?= Form::label('tgl_lahir', Member::getAttributeName('tgl_lahir')) ?>
                        <div class="input-group">
                            <?= Form::text('tgl_lahir', $model->tgl_lahir ? 
                                    Helper::formatDate($model->tgl_lahir, Helper::FORMAT_DATE_ID_SHORT) : null,
                                    ['class'=>'form-control datepicker','placeholder'=>'e.x : 21-01-2017']) ?>
                            <span class="input-group-addon">
                                <i class="fa fa-calendar bigger-110"></i>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-lg-5">
                        <?= Form::label('telepon', Member::getAttributeName('telepon')) ?>
                        <?= Form::text('telepon',null,['class'=>'form-control','placeholder'=>'e.x : 081220923580']) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-lg-10">
                        <?= Form::label('alamat', Member::getAttributeName('alamat')) ?>
                        <?= Form::text('alamat',null,['class'=>'form-control','placeholder'=>'e.x : Jalan Soekarno Hatta D505 Malang']) ?>
                    </div>
                </div>
            </div>
        </div>            
    </div>

    <div class="col-md-7">
        <div class="panel panel-primary">
            <div class="panel-heading">Data Kartu Member</div>
            <div class="panel-body" style="background: #f5f5f5;">
                <div class="row">
                    <div class="form-group col-lg-3">
                        <?= Form::label('no_kartu', MemberCard::getAttributeName('no_kartu')) ?>
                        <dd><?= $modelCard->no_kartu ?></dd>
                    </div>
                    <div class="form-group col-lg-3">
                        <?= Form::label('jenis', MemberCard::getAttributeName('jenis')) ?>
                        <dd><?= $modelCard->getJenis() ?></dd>
                    </div>                
                    <div class="form-group col-lg-3">
                        <?= Form::label('jumlah_tempo', MemberCard::getAttributeName('jumlah_tempo')) ?>
                        <dd><?= $modelCard->jumlah_tempo ?></dd>
                    </div>
                    <div class="form-group col-lg-3">
                        <?= Form::label('max_piutang', MemberCard::getAttributeName('max_piutang')) ?>
                        <dd><?= Helper::idNumber($modelCard->max_piutang) ?></dd>
                    </div>
                </div>
            </div>
        </div>                               
    </div>
    
    <div class="col-md-7" id="form-user">
        <div class="panel panel-primary">
            <div class="panel-heading">Data User</div>
            <div class="panel-body" style="background: #f5f5f5;">
                <div class="row">
                    <div class="form-group col-lg-6">
                        <?= Form::label('username', User::getAttributeName('username')) ?>
                        <?= Form::text('username',null,['class'=>'form-control','placeholder'=>'e.x : andri']) ?>
                    </div>
                    <div class="form-group col-lg-6">
                        <?= Form::label('password', User::getAttributeName('password')) ?>
                        <div class="input-group">
                            <?= Form::password('password', ['class'=>'form-control']) ?>
                            <span class="input-group-addon clickable">
                                <i class="fa fa-eye"></i>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>                               
    </div>
</div>

<div class="form-group">
    <button type="submit" class="btn btn-success">
        <i class="fa fa-save"></i>
        Simpan
    </button>
    <a href="<?=route('profile.index')?>" class="btn btn-primary">
        <i class="ace-icon fa fa-arrow-left"></i>
        Kembali
    </a>
</div>

@section('js-include')
<script type="text/javascript" src="<?= asset('vendor/') ?>/utils/jquery.format-number.js"></script>
<script type="text/javascript" src="<?= asset('vendor/') ?>/utils/jquery.enter-next.js"></script>
<script type="text/javascript" src="<?= asset('vendor/') ?>/chosen/chosen.jquery.min.js"></script>
@endsection 

@section('css-include')
<link rel="stylesheet" href="<?= asset('vendor/') ?>/chosen/chosen.min.css" />
@endsection 

@section('js-inline')
//<script type="text/javascript">
$(function(){
    ajaxFormGritter.bind('mainForm',{onSuccess:function(data){
        document.location = document.location;
    }});

    $('.datepicker').datepicker({
        format: 'dd-mm-yyyy',
        autoclose: true,
        todayHighlight: true
    });
    
    $('.chosen-select').chosen({allow_single_deselect:true}); 
    
    $('[type=password]').next().mousedown(function(){ $(this).prev().attr('type', 'text'); });
    $('[type=password]').next().mouseup(function(){ $(this).prev().attr('type', 'password'); });
    
    $('.format-number').formatNumber();

    $('input:not([disabled=disabled]),select,[type=submit]').enterNext();
    
    $('[type=password]').next().mousedown(function(){ $(this).prev().attr('type', 'text'); });
    $('[type=password]').next().mouseup(function(){ $(this).prev().attr('type', 'password'); });
});
@endsection