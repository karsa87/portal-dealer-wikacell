@extends('layout/default')

@section('title')
Member
@endsection

@section('breadcrumb')
<li>
    <a href="<?= route('profile.index')?>">Member</a>
</li>
<li>
    Edit
</li>
@endsection

@section('content-header')
Member
<small>
    <i class="ace-icon fa fa-angle-double-right"></i>
    Edit
</small>
@endsection

@section('content')
<div class="content-wrapper">    
    <!-- Main content -->
    <section class="content">
    <div class="row">
        <div class="col-md-12">
            <?= Form::model($model, ['route' => ['profile.update', $model->id], 'id'=>'mainForm', 'method'=>'PUT']) ?>
            @include('member/_form')
            <?= Form::close() ?>
        </div>
    </div>
    </section>
</div>
@endsection