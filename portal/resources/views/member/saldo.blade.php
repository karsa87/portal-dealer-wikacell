
<?php
use Karsa\Helper;
use App\Models\MemberSaldo;
?>

@extends('layout.default')

@section('title')
Saldo
@endsection

@section('content')
<div class="content-wrapper">    
    <!-- Main content -->
    <section class="content">
      <div class="row">
        
        <div class="col-md-3 col-xs-12 col-sm-12 filter-top" style="margin-bottom: 15px;">               
          <form class="form-inline">
            <div class="form-group">              
              <div class="input-group">                  
                <?= Form::text('search',"",['class'=>'form-control',"placeholder"=>"Keyword"]) ?>                
                <div class="input-group-addon search"><span class="fa fa-search"></span></div>
              </div>
            </div>              
          </form>         
        </div>

        <div class="col-md-3 col-xs-12 col-sm-12 filter-top" style="margin-bottom: 15px;">
            <div class="form-group">
                <div class="input-group date">
                    <input type="text" class="form-control pull-right datepicker" placeholder="<?= MemberSaldo::getAttributeName('tanggal') ?>" id="tanggal">
                    <div class="input-group-addon">
                    	<i class="fa fa-calendar"></i>
                    </div>
                </div>                
            </div>                                
        </div>

        <div class="col-md-3 col-xs-12 col-sm-12 filter-top" style="margin-bottom: 15px;">
            <div class="col-md-6 col-sm-6 col-xs-6">
                <a href="#" class="btn btn-block btn-info" title="Saldo" data-toggle="tooltip">
                    Rp <?= Helper::idNumber(Auth::user()->getMemberCard()->member->saldo) ?>
                </a>
            </div>

            <div class="col-md-6 col-sm-6 col-xs-6">
                <a href="#" class="btn btn-block btn-danger" title="Piutang" data-toggle="tooltip">
                    Rp <?= Helper::idNumber(Auth::user()->getMemberCard()->getTotalPiutang()) ?>
                </a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
                <h3 class="box-title">History Add Saldo</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
              <table class="table table-hover" id="mainTable">
                <thead>
                    <th><?= MemberSaldo::getAttributeName('tanggal') ?></th>
                    <th><?= MemberSaldo::getAttributeName('keterangan') ?></th>
                    <th><?= MemberSaldo::getAttributeName('tipe') ?></th>
                    <th>Nominal </th>
                    <th><?= MemberSaldo::getAttributeName('saldo') ?> </th>
                </thead>
              </table>
            </div>
          </div>
        </div>        
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection

@section('js-include')
<script type="text/javascript" src="<?= asset('vendor/') ?>/jquery.datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?= asset('vendor/') ?>/jquery.datatables/jquery.dataTables.bootstrap.min.js"></script>
@endsection

@section('js-inline')
//<script type="text/javascript">
$(function(){
    mainTable = $('#mainTable').dataTable({
        "aoColumns": [ 
            { "data": "tanggal","bSortable": false },
            { "data": "keterangan","bSortable": false },
            { "data": "tipe","bSortable": false },
            { "data": "nominal","bSortable": false },
            { "data": "sisa","bSortable": false }
        ],
        "stateSave": true,
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": "<?= route('profile.saldo.data')?>",
        "sServerMethod": "GET",
//        "aaSorting": [[ 0, "asc" ]],
        "bLengthChange": false,
        "bFilter": false,
        "iDisplayRow": 25,
//        "iDisplayLength": 50,
//        "pageLength" : 25,
        "aLengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]],
        "fnServerParams": function ( aoData ) {
            aoData.push( { "name": "_token", "value": '<?=  csrf_token() ?>' } );
            aoData.push({ "name": "search", "value": $('[name=search]').val() });
            aoData.push({ "name": "tanggal", "value": $('#tanggal').val() });
        }
    });
    
    $('.datepicker').datepicker({
        format: 'dd-mm-yyyy',
        autoclose: true,
        todayHighlight: true
    });
    
    $('[name=search]').keyup(function(){
        mainTable.fnDraw(false);
    });
    
    $('#tanggal').change(function(){
        mainTable.fnDraw(false);
    });
})
@endsection
