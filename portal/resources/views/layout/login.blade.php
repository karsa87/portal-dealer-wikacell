<?php
use Karsa\Notification;
?>
<!DOCTYPE html>
<html>
<head>
	<title><?= Config::get('my.app_name') ?> - Login</title>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="stylesheet" type="text/css" href="<?= asset('vendor/') ?>/assets/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="<?= asset('vendor/') ?>/assets/css/style.css">
    <link rel="icon" type="image/png" href="<?= asset('vendor/') ?>/favicon.png">
    <link rel="stylesheet" type="text/css" href="<?= asset('vendor/') ?>/jquery.gritter/css/jquery.gritter.css" />
    <link rel="stylesheet" type="text/css" href="<?= asset('vendor/') ?>/jquery.gritter/css/jquery.gritter-add.css" />
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-4 login">
				<div class="col-md-12">
					<img src="<?= asset('vendor/') ?>/assets/images/logo.jpeg">
				</div>		
				<div class="col-md-12 form-login">
					<span class="label-ket">Silahkan Login ke Akun anda</span>
					<br>
                    <?= Form::open(['route'=>'auth.login','method'=>'POST','id'=>'mainForm']) ?>
                    <?= csrf_field() ?>
                    <div class="form-group">
                        <input type="text" class="form-control" name="username" placeholder="Username">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" name="password" placeholder="Password">
                    </div>
                    <input type="submit" name="login" value="Login" class="btn btn-primary">
                    <?= Form::close() ?>
				</div>		
			</div>
			<div class="col-md-8">
				<img src="<?= asset('vendor/') ?>/assets/images/loginimage.jpg" class="login-logo">
			</div>
		</div>
	</div>
    
    <script type="text/javascript" src="<?= asset('vendor/') ?>/assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?= asset('vendor/') ?>/assets/js/bootstrap.js"></script>
    <script src="<?= asset('vendor/') ?>/jquery.gritter/js/jquery.gritter.min.js" type="text/javascript"></script>
    <script src="<?= asset('vendor/') ?>/jquery.gritter/js/jquery.gritter-add.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            ajaxFormGritter.bind('mainForm',{
                onSuccess:function(){
                    window.location = "<?= Redirect::intended('dashboard')->getTargetUrl() ?>";
                },
                _text : {
                    process: 'Validasi',
                    done: 'Login Berhasil',
                    error: 'Harap tunggu proses ini selesai...'
                }
            });
            <?= Notification::showGritter() ?>
        });
    </script>
</body>
</html>