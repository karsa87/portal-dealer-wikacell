@extends('layout/default')

@section('title')
403 Not Authorized
@endsection

@section('breadcrumb')
<li>
    Error
</li>
<li>
    403 Not Authorized
</li>
@endsection

@section('content-header')
403
<small>
    <i class="ace-icon fa fa-angle-double-right"></i>
    Not Authorized
</small>
@endsection

@section('content')
<div class="row">
    <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->

        <div class="error-container">
            <div class="well">
                <h1 class="grey lighter smaller">
                    <span class="blue bigger-125">
                        <i class="ace-icon fa fa-lock"></i>
                        403
                    </span>
                    Not Authorized
                </h1>

                <hr>
                <h3 class="lighter smaller">Maaf anda tidak memiliki hak untuk mengakses halaman ini!</h3>

                <hr>
                <div class="space"></div>

                <div class="center">
                    <a class="btn btn-grey" href="javascript:history.back()">
                        <i class="ace-icon fa fa-arrow-left"></i>
                        Kembali
                    </a>

                    <a class="btn btn-primary" href="{{route('dashboard')}}">
                        <i class="ace-icon fa fa-tachometer"></i>
                        Dashboard
                    </a>
                </div>
            </div>
        </div>

        <!-- PAGE CONTENT ENDS -->
    </div><!-- /.col -->
</div>
@endsection