<?php
use Karsa\Notification;
use App\Models\Notifications;
?>

<!DOCTYPE html>
<html>
<head>
    <title><?= Config::get('my.app_name_1') ?> - @yield('title')</title>
    <link rel="icon" type="image/png" href="<?= asset('vendor/') ?>/favicon.png">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="utf-8">
  	<meta http-equiv="X-UA-Compatible" content="IE=edge">
  	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="stylesheet" type="text/css" href="<?= asset('vendor/') ?>/assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="<?= asset('vendor/') ?>/jquery.gritter/css/jquery.gritter.css" />
    <link rel="stylesheet" type="text/css" href="<?= asset('vendor/') ?>/jquery.gritter/css/jquery.gritter-add.css" />		
	<link rel="stylesheet" type="text/css" href="<?= asset('vendor/') ?>/assets/css/dataTables.bootstrap.css">	
	<link rel="stylesheet" type="text/css" href="<?= asset('vendor/') ?>/bootstrap-toggle/bootstrap-toggle.min.css">	
	<link rel="stylesheet" type="text/css" href="<?= asset('vendor/') ?>/assets/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?= asset('vendor/') ?>/iCheck/all.css">
	<link rel="stylesheet" type="text/css" href="<?= asset('vendor/') ?>/datepicker/datepicker3.css">
	<link rel="stylesheet" type="text/css" href="<?= asset('vendor/') ?>/assets/css/AdminLTE.min.css">
	<link rel="stylesheet" type="text/css" href="<?= asset('vendor/') ?>/assets/css/_all-skins.min.css">
	<link rel="stylesheet" type="text/css" href="<?= asset('vendor/') ?>/assets/css/style.css">
    
    @yield('css-include')
    <style type="text/css">@yield('css-inline')</style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
		<header class="main-header">
		    <!-- Logo -->
		    <img src="<?= asset('vendor/') ?>/assets/images/logo.jpeg" class="logo">
		    <!-- Header Navbar: style can be found in header.less -->
		    <nav class="navbar navbar-static-top">
		      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </a>      
		      <div class="navbar-custom-menu">
		        <ul class="nav navbar-nav">
		          <!-- User Account: style can be found in dropdown.less -->
		          <li class="dropdown user user-menu">
		            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
		              <span class="hidden-xs">Halo, <?= ucwords(Auth::user()->nama) ?></span>
		              <img src="<?= asset('vendor/') ?>/assets/images/foto.jpg" class="user-image" alt="User Image">
		            </a>            
		          </li>

		          <!-- Notifications: style can be found in dropdown.less -->
                    <?php
                        $notif = Notifications::getNotifications([
                            'is_read'   => Notifications::IS_NOT_READ,
                            'to'        => Auth::user()->id
                        ]);
                    ?>
		          <li class="dropdown notifications-menu">
		            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
		              <i class="fa fa-bell-o"></i>
                      <span class="label label-warning"><?= count($notif) ?></span>
		            </a>
		            <ul class="dropdown-menu">
                        <li class="header">Anda memiliki <?= count($notif) ?> notifikasi</li>
		              <li>
		                <!-- inner menu: contains the actual data -->
		                <ul class="menu">
                            <?php foreach ($notif as $n) : ?>
                            <li>
                                <a href="<?= route('notification.show',$n['id']) ?>">
                                    <i class="fa fa-users text-aqua"></i> <?= $n['message'] ?>
                                </a>
                            </li>
                            <?php endforeach; ?>
		                </ul>
		              </li>
                      <li class="footer"><a href="<?= route('notification.index') ?>">View all</a></li>
		            </ul>
		          </li>

		          <!-- Menu -->
		          <li>
		            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i></a>
		            <ul class="dropdown-menu">
                        <li class="footer"><a href="<?= route('profile.edit',Auth::user()->getMemberCard()->member->id) ?>"><font color="#777">Edit Porfile</font></a></li>
                      <li class="footer"><a href="<?= route('auth.logout') ?>"><font color="#777">Logout</font></a></li>
		            </ul>
		          </li>
		        </ul>
		      </div>
		    </nav>
		</header>
		<aside class="main-sidebar">
		    <!-- sidebar: style can be found in sidebar.less -->
		    <section class="sidebar">
		      <!-- sidebar menu: : style can be found in sidebar.less -->
		      <ul class="sidebar-menu">     
		      	@if(Auth::user()->hasAbility('dashboard'))  
		        <li class="treeview">
		          <a href="<?= route('dashboard') ?>">
		            <i class="fa fa-home"></i><br> <span>Dashboard</span>            
		          </a>          
		        </li>     
		        @endif
		        @if(Auth::user()->hasAbility('order'))                                                                   
		        <li class="treeview">
		          <a href="<?= route('preorder.create') ?>">
		            <i class="fa fa-shopping-cart"></i><br> <span>Order</span>            
		          </a>          
		        </li>
		        @endif
		        @if(Auth::user()->hasAbility('trackorder'))
		        <li class="treeview">
		          <a href="<?= route('trackorder') ?>">
		            <i class="fa fa-compass"></i><br> <span>Track Order</span>            
		          </a>          
		        </li>
		        @endif
                @if(Auth::user()->hasAbility('order'))
		        <li class="treeview">
		          <a href="<?= route('order.index') ?>">
		            <i class="fa fa-shopping-basket"></i><br> <span>Histori Order</span>            
		          </a>          
		        </li>
		        @endif
                @if(Auth::user()->hasAbility('profile.saldo'))
		        <li class="treeview">
		          <a href="<?= route('profile.saldo') ?>">
		            <i class="fa fa-money"></i><br> <span>Saldo</span>
		          </a>          
		        </li>
		        @endif
		        <li class="treeview">
                    <a href="<?= route('portal_promo.index') ?>">
		            <i class="fa fa-percent"></i><br> <span>Promo</span>            
		          </a>          
		        </li>
		      </ul>
		    </section>
		    <!-- /.sidebar -->
		</aside>
		
		@yield('content')
		
	</div>


	<script type="text/javascript" src="<?= asset('vendor/') ?>/assets/js/jquery.min.js"></script>
	<script type="text/javascript" src="<?= asset('vendor/') ?>/assets/js/bootstrap.js"></script>
	<script type="text/javascript" src="<?= asset('vendor/') ?>/input-mask/jquery.inputmask.js"></script>
	<script type="text/javascript" src="<?= asset('vendor/') ?>/input-mask/jquery.inputmask.date.extensions.js"></script>
	<script type="text/javascript" src="<?= asset('vendor/') ?>/input-mask/jquery.inputmask.extensions.js"></script>
    <script src="<?= asset('vendor/') ?>/jquery.gritter/js/jquery.gritter.min.js" type="text/javascript"></script>
    <script src="<?= asset('vendor/') ?>/jquery.gritter/js/jquery.gritter-add.js" type="text/javascript"></script>
	<script type="text/javascript" src="<?= asset('vendor/') ?>/iCheck/icheck.min.js"></script>
	<script type="text/javascript" src="<?= asset('vendor/') ?>/bootstrap-toggle/bootstrap-toggle.js"></script>
    <script type="text/javascript" src="<?= asset('vendor/') ?>/slimScroll/jquery.slimscroll.min.js"></script>
	<script type="text/javascript" src="<?= asset('vendor/') ?>/fastclick/fastclick.js"></script>
	<script type="text/javascript" src="<?= asset('vendor/') ?>/datepicker/bootstrap-datepicker.js"></script>
	<script type="text/javascript" src="<?= asset('vendor/') ?>/assets/js/app.min.js"></script>
	<script type="text/javascript" src="<?= asset('vendor/') ?>/assets/js/demo.js"></script>
        
    <!-- inline scripts related to this page -->
    @yield('js-include')
    <script type="text/javascript">
        @yield('js-inline')
        $(function () {
            <?= Notification::showGritter() ?>
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
</body>
</html>