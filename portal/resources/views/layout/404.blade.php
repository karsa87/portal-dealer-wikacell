@extends('layout/default')

@section('title')
404 Not Found
@endsection

@section('breadcrumb')
<li>
    Error
</li>
<li>
    404 Not Found
</li>
@endsection

@section('content-header')
404
<small>
    <i class="ace-icon fa fa-angle-double-right"></i>
    Not Found
</small>
@endsection

@section('content')
<div class="row">
    <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->

        <div class="error-container">
            <div class="well">
                <h1 class="grey lighter smaller">
                    <span class="blue bigger-125">
                        <i class="ace-icon fa fa-sitemap"></i>
                        404
                    </span>
                    Not Found
                </h1>

                <hr>
                <h3 class="lighter smaller">Maaf halaman yang anda cari tidak dapat ditemukan!</h3>

                <hr>
                <div class="space"></div>

                <div class="center">
                    <a class="btn btn-grey" href="javascript:history.back()">
                        <i class="ace-icon fa fa-arrow-left"></i>
                        Kembali
                    </a>

                    <a class="btn btn-primary" href="{{route('dashboard')}}">
                        <i class="ace-icon fa fa-tachometer"></i>
                        Dashboard
                    </a>
                </div>
            </div>
        </div>

        <!-- PAGE CONTENT ENDS -->
    </div><!-- /.col -->
</div>
@endsection