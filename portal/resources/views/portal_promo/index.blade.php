<?php
use Karsa\Helper;
use App\Models\PortalPromo;
use App\Models\Kategori;
use App\Models\Brand;

?>

@extends('layout.default')

@section('title')
Dashboard
@endsection

@section('content')
<div class="content-wrapper">    
    <!-- Main content -->
    <section class="content">
      <div class="row">
        
        <div class="col-md-7 col-xs-12 col-sm-12 filter-top" style="margin-bottom: 15px;">               
          <form class="form-inline">
            <div class="form-group">              
              <div class="input-group">                  
                <div class="input-group-addon" style="padding: 0px">
                    <?= Form::text('search',"",['class'=>'form-control filter-select',"placeholder"=>"Search"]) ?>
                </div>
                <div class="input-group-addon" style="padding: 0px">
                    <?= Form::text('start',"",['class'=>'form-control filter-select datepicker',"placeholder"=>"Mulai"]) ?>
                </div>
                <div class="input-group-addon" style="padding: 0px">
                    <?= Form::text('end',"",['class'=>'form-control filter-select datepicker',"placeholder"=>"Sampai"]) ?>
                </div>
                <div class="input-group-addon search"><span class="fa fa-search"></span></div>
              </div>
            </div>              
          </form>         
        </div>

        <div class="col-md-5">
            <!--            <div class="col-md-4 col-sm-4 col-xs-4">
                <a href="<?= route('trackorder') ?>" class="btn btn-block btn-success">
                    <span class="fa fa-compass"></span> <font>Track Order</font>
                </a>
            </div>-->
            <?php
                $memberCard  = Auth::user()->getMemberCard();
                $member      = $memberCard->member;
                $piutang     = $memberCard->getTotalPiutang();
            ?>
            
            <div class="col-md-4 col-sm-4 col-xs-4">
                <a href="#" class="btn btn-block btn-info" title="Saldo" data-toggle="tooltip">
                    Rp <?= Helper::idNumber($member->saldo) ?>
                </a>
            </div>
            
            <div class="col-md-4 col-sm-4 col-xs-4">
                <a href="<?= route('trackorder') ?>" class="btn btn-block btn-warning" title="Limit" data-toggle="tooltip">
                    <!--<span class="fa fa-compass"></span> <font>Track Order</font>-->
                    Rp <?= Helper::idNumber($memberCard->max_piutang - $piutang); ?>
                </a>
            </div>

            <div class="col-md-4 col-sm-4 col-xs-4">
                <a href="#" class="btn btn-block btn-danger" title="Piutang" data-toggle="tooltip">
                    Rp <?= Helper::idNumber($piutang) ?>
                </a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10">
          <div class="box">
            <div class="box-header">
                <h3 class="box-title"><?= PortalPromo::getAttributeName("_title") ?></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
              <table class="table table-hover" id="mainTable">
                <thead>
                    <th class="col-sm-2"><?= PortalPromo::getAttributeName('no_promo') ?></th>
                    <th class="col-sm-4"><?= PortalPromo::getAttributeName('nama') ?></th>
                    <th class="col-sm-2"><?= PortalPromo::getAttributeName('start') ?></th>
                    <th class="col-sm-2"><?= PortalPromo::getAttributeName('end') ?></th>
                    @if(Auth::user()->hasAbility('portal_promo.create', 'portal_promo.destroy', 'portal_promo.edit'))
                    <th class="col-sm-2">
                        Tindakan
                    </th>
                    @endif
                </thead>                
              </table>
            </div>
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection

@section('js-include')
<script type="text/javascript" src="<?= asset('vendor/') ?>/jquery.datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?= asset('vendor/') ?>/jquery.datatables/jquery.dataTables.bootstrap.min.js"></script>
@endsection

@section('js-inline')
//<script type="text/javascript">
var mainTable;
ajaxDeleteGritter.initialize({
    onSuccess:function(data){
        mainTable.fnDraw();
    }
});
$(function(){
    mainTable = $('#mainTable').dataTable({
        "aoColumns": [ 
            { "class":"column-padding-right" },
            { },
            { },
            { },
            { "class":"text-center" }
        ],
        "stateSave": true,
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": "<?= route('portal_promo.index.data')?>",
        "sServerMethod": "GET",
        "aaSorting": [[ 0, "asc" ]],
        "bLengthChange": false,
        "bFilter": false,
        "iDisplayRow": 25,
//        "iDisplayLength": 50,
//        "pageLength" : 25,
        "aLengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]],
        "fnServerParams": function ( aoData ) {
            aoData.push( { "name": "_token", "value": '<?=  csrf_token() ?>' } );
            aoData.push({ "name": "start", "value": $('[name=start]').val() });
            aoData.push({ "name": "end", "value": $('[name=end]').val() });
            aoData.push({ "name": "search", "value": $('[name=search]').val() });
        }
    });
    
    var parent =  $("#mainTable_paginate").parent();
    parent.removeClass("col-xs-6");
    parent.addClass("col-xs-8 text-right");
    
    var parent =  $("#mainTable_info").parent();
    parent.removeClass("col-xs-6");
    parent.addClass("col-xs-4");
    
    
    $('[name=search]').keyup(function(){
        mainTable.fnDraw(false);
    });
    
    $('[name=start]').change(function(){
        mainTable.fnDraw(false);
    });
    
    $('[name=end]').change(function(){
        mainTable.fnDraw(false);
    });
    
    $('.datepicker').datepicker({
        format: 'dd-mm-yyyy',
        autoclose: true,
        todayHighlight: true
    });
});
@endsection
