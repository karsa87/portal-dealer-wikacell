<?php

use App\Models\PortalPromo;
use App\Models\PortalPromoLine;
use Karsa\Helper;

/* @var $model PortalPromo */

?>
@extends('layout/default')

@section('title')
Portal Promo
@endsection

@section('breadcrumb')
<li>
    <a href="<?= route('portal_promo.index')?>">Portal Promo</a>
</li>
<li>
    Detail
</li>
@endsection

@section('content-header')
Detail Portal Promo
@endsection

@section('content')
<div class="content-wrapper">    
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-5">
                <div class="panel panel-default">
                    <div class="panel-heading">Detail Portal Promo</div>
                    <div class="panel-body">                    
                        <div class="row">
                            <div class="col-lg-6">                            
                                <dt><?= PortalPromo::getAttributeName('no_promo') ?></dt>
                                <dd><?= $model->no_promo ?></dd>
                            </div>
                            <div class="col-lg-6">
                                <dt><?= PortalPromo::getAttributeName('nama') ?></dt>
                                <dd><?= $model->nama ?></dd>                            
                            </div>
                        </div>
                        <div class="space-2"></div>
                        <div class="row">
                            <div class="col-lg-6">                            
                                <dt><?= PortalPromo::getAttributeName('start') ?></dt>
                                <dd><?= Helper::formatDate($model->start, Helper::FORMAT_DATE_ID_LONG) ?></dd>
                            </div>
                            <div class="col-lg-6">                            
                                <dt><?= PortalPromo::getAttributeName('end') ?></dt>
                                <dd><?= Helper::formatDate($model->end, Helper::FORMAT_DATE_ID_LONG) ?></dd>
                            </div>
                        </div>
                        <div class="space-2"></div>
                        <div class="row">
                            <div class="col-lg-12">                            
                                <dt><?= PortalPromo::getAttributeName('keterangan') ?></dt>
                                <dd><?= $model->keterangan ? $model->keterangan : "-" ?></dd>
                            </div>  
                        </div>
                    </div>
                </div>            
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 header-green">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Daftar Barang</h3>
                    </div>
                    <div class="box-body no-padding">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th><?= PortalPromoLine::getAttributeName('barang_id') ?></th>
                                    <th><?= PortalPromoLine::getAttributeName('min_jumlah') ?></th>
                                    <th><?= PortalPromoLine::getAttributeName('jenis') ?></th>
                                    <th><?= PortalPromoLine::getAttributeName('potongan_persen') ?></th>
                                    <th><?= PortalPromoLine::getAttributeName('potongan_ribuan') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($model->lines as $l) : ?>
                                <tr>
                                    <td class="text-left"><?= $l->barang->nama ?></td>
                                    <td><?= Helper::idNumber($l->min_jumlah) ?></td>
                                    <td><?= $l->getJenis() ?></td>
                                    <td><?= Helper::idNumber($l->potongan_persen) ?></td>
                                    <td class="text-right"><?= Helper::idNumber($l->potongan_ribuan) ?></td>
                                </tr>
                                <?php if(count($l->bonus) > 0) : ?>
                                <tr>
                                    <td colspan="5">
                                        <table class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th width="70%"><?= PortalPromoLine::getAttributeName('bonus_barang_id') ?></th>
                                                    <th width="50%"><?= PortalPromoLine::getAttributeName('jumlah') ?></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($l->bonus as $b) : ?>
                                                <tr>
                                                    <td><?= $b->nama ?></td>
                                                    <td><?= Helper::idNumber($b->pivot->jumlah) ?></td>
                                                </tr>
                                                <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <?php endif; ?>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <a href="<?= route('portal_promo.index')?>" class="btn btn btn-primary">
                    <i class="ace-icon fa fa-arrow-left"></i>
                    Kembali
                </a>
            </div>
        </div>
    </section>
</div>
@endsection