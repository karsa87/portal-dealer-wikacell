<?php

use App\Models\PortalPromo;
use App\Models\PortalPromoLine;
use Karsa\Helper;

/* @var $model PortalPromo */
?>
<?php ob_start(); ?>
<tr id="tr-{BARANG_ID}">
    <td class="col-lg-3">
        <span id="bm-{BARANG_ID}">{BARANG_NAMA}</span>
        <?= Form::hidden('line[{BARANG_ID}][barang_id]',"{BARANG_ID}",['id'=>'barang_id-{BARANG_ID}']) ?>
        <?= Form::hidden('line[{BARANG_ID}][barang_nama]',"{BARANG_NAMA}",['id'=>'barang_nama-{BARANG_ID}']) ?>
    </td>
    <td>
        <span id="mj-{BARANG_ID}">{SPAN_MIN_JUMLAH}</span>
        <?= Form::hidden('line[{BARANG_ID}][min_jumlah]',"{MIN_JUMLAH}",['id'=>'min_jumlah-{BARANG_ID}']) ?>
    </td>
    <td>
        <span id="jm-{BARANG_ID}">{JENIS_NAMA}</span>
        <?= Form::hidden('line[{BARANG_ID}][jenis]',"{JENIS}",['id'=>'jenis-{BARANG_ID}']) ?>
        <?= Form::hidden('line[{BARANG_ID}][jenis_nama]',"{JENIS_NAMA}",['id'=>'jenis_nama-{BARANG_ID}']) ?>
    </td>
    <td>
        <span id="prs-{BARANG_ID}">{SPAN_POTONGAN_PERSEN}</span>
        <?= Form::hidden('line[{BARANG_ID}][potongan_persen]',"{POTONGAN_PERSEN}",['id'=>'potongan_persen-{BARANG_ID}']) ?>
    </td>
    <td>
        <span id="prb-{BARANG_ID}">{SPAN_POTONGAN_RIBUAN}</span>
        <?= Form::hidden('line[{BARANG_ID}][potongan_ribuan]',"{POTONGAN_RIBUAN}",['id'=>'potongan_ribuan-{BARANG_ID}']) ?>
    </td>
    <td>
        <a tabindex="-1" href="#modal-form" role="button" class="btn btn-sm btn-success" data-toggle="modal" onclick="showModal({BARANG_ID})">
            <i class="fa fa-plus"></i>
        </a>
        <a tabindex="-1" href="javascript:void(0)" onclick="editItem({BARANG_ID})" class="btn btn-sm btn-warning">
            <i class="fa fa-edit"></i>
        </a>
        <a tabindex="-1" href="javascript:void(0)" onclick="deleteItem({BARANG_ID})" class="btn btn-sm btn-danger">
            <i class="fa fa-trash"></i>
        </a>
    </td>
</tr>
<tr id="tr-bonus-{BARANG_ID}" class="hidden">
    <td colspan="6">
        <table class="table table-bordered table-striped" id="table-bonus-{BARANG_ID}">
            <thead>
                <tr>
                    <th width="70%"><?= PortalPromoLine::getAttributeName('bonus_barang_id') ?></th>
                    <th width="20%"><?= PortalPromoLine::getAttributeName('jumlah') ?></th>
                    <th width="10%">Menu</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </td>
</tr>
<?php $template = ob_get_clean(); ?>

<!-- Template Table Bonus -->
<?php ob_start(); ?>
<tr id="tr-{BARANG_ID}-{BONUS_BARANG_ID}">
    <td>
        <span id="bm-{BARANG_ID}-{BONUS_BARANG_ID}">{BARANG_NAMA}</span>
        <?= Form::hidden('line[{BARANG_ID}][bonus][{BONUS_BARANG_ID}][barang_id]',"{BARANG_ID}",['id'=>'barang_id-{BARANG_ID}-{BONUS_BARANG_ID}']) ?>
    </td>
    <td>
        <span id="j-{BARANG_ID}-{BONUS_BARANG_ID}">{SPAN_JUMLAH}</span>
        <?= Form::hidden('line[{BARANG_ID}][bonus][{BONUS_BARANG_ID}][jumlah]',"{JUMLAH}",['id'=>'jumlah-{BARANG_ID}-{BONUS_BARANG_ID}']) ?>
    </td>
    <td>
        <a tabindex="-1" href="javascript:void(0)" onclick="deleteBonus({BARANG_ID},{BONUS_BARANG_ID})" class="btn btn-sm btn-danger">
            <i class="fa fa-trash"></i>
        </a>
    </td>
</tr>
<?php $template2 = ob_get_clean(); ?>

<div class="row">
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">Data Portal Promo</div>
            <div class="panel-body">                    
                <div class="row">
                    <div class="form-group col-lg-6">
                        <?= Form::label('no_promo', PortalPromo::getAttributeName('no_promo')) ?>
                        <?= Form::text('no_promo',$model->exists ? $model->no_promo : PortalPromo::getLastNoPromo(),['class'=>'form-control', $model->exists ? 'disabled' : '']) ?>        
                    </div>
                    <div class="form-group col-lg-6">
                        <?= Form::label('nama', PortalPromo::getAttributeName('nama')) ?>
                        <?= Form::text('nama',null,['class'=>'form-control','placeholder'=>'e.x : Promo Samsung']) ?>        
                    </div>
                </div>
                <div class="space-2"></div>
                <div class="row">
                    <div class="form-group col-lg-6">
                        <?= Form::label('start', PortalPromo::getAttributeName('start')) ?>
                        <div class="input-group">
                            <?= Form::text('start',@$model->exists ? Helper::formatDate($model->start,Helper::FORMAT_DATE_ID_SHORT) : Helper::getTodayDate(Helper::FORMAT_DATE_ID_SHORT),['class'=>'form-control datepicker enternext']) ?>
                            <span class="input-group-addon">
                                <i class="fa fa-calendar bigger-110"></i>
                            </span>
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <?= Form::label('end', PortalPromo::getAttributeName('end')) ?>
                        <div class="input-group">
                            <?= Form::text('end',@$model->exists ? Helper::formatDate($model->end,Helper::FORMAT_DATE_ID_SHORT) : Helper::getTodayDate(Helper::FORMAT_DATE_ID_SHORT),['class'=>'form-control datepicker enternext']) ?>
                            <span class="input-group-addon">
                                <i class="fa fa-calendar bigger-110"></i>
                            </span>
                        </div>
                    </div>
                </div>     
                <div class="row">
                    <div class="form-group col-lg-12">
                        <?= Form::label('keterangan', PortalPromo::getAttributeName('keterangan')) ?>
                        <?= Form::textarea('keterangan',null,['class'=>'form-control','placeholder'=>'e.x : Promo ini hanya untuk produk samsung']) ?>
                    </div>
                </div>
            </div>
        </div>            
    </div>
    
    <div class="col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading">Data Lines Promo</div>
            <div class="panel-body">                    
                <div class="row">
                    <div class="form-group col-lg-6">
                        <?= Form::label('barang', PortalPromoLine::getAttributeName('barang_id')) ?>
                        <div class="input-group">
                            <?= Form::text('barang', @$model->barang->nama, ['class'=>'form-control enternext']) ?>
                            <span class="input-group-addon">
                                <i id="loading" class="fa fa-refresh" style="color: #000;"></i>
                            </span>
                        </div>
                        <?= Form::hidden('barang_id',null,['id'=>'barang_id']) ?>
                    </div>
                    <div class="form-group col-lg-6">
                        <?= Form::label('min_jumlah', PortalPromoLine::getAttributeName('min_jumlah')) ?>
                        <?= Form::number('min_jumlah',null,['class'=>'form-control','placeholder'=>'e.x : 10']) ?>
                    </div>
                </div>
                <div class="space-2"></div>
                <div class="row">
                    <div class="form-group col-lg-6">
                        <?= Form::label('jenis', PortalPromoLine::getAttributeName('jenis')) ?>
                        <?= Form::select('jenis',PortalPromoLine::getJenisList(),null,['class'=>'form-control enternext']) ?>
                    </div>
                    <div class="form-group col-lg-6 hidden" id="potongan_persen">
                        <?= Form::label('potongan_persen', PortalPromoLine::getAttributeName('potongan_persen')) ?>
                        <?= Form::text('potongan_persen',0,['class'=>'form-control','placeholder'=>'e.x : 10','max'=>100]) ?>
                    </div>
                    <div class="form-group col-lg-6"  id="potongan_ribuan">
                        <?= Form::label('potongan_ribuan', PortalPromoLine::getAttributeName('potongan_ribuan')) ?>
                        <?= Form::text('potongan_ribuan',0,['class'=>'form-control format-number','placeholder'=>'e.x : 10,000']) ?>
                    </div>
                </div>
                <div class="space-2"></div>
                <div class="row">
                    <div class="form-group col-lg-12 text-right">
                        <a href="javascript:void(0)" onclick="addItem()" class="btn btn-success">
                            <i class="fa fa-plus"></i>
                            Tambah
                        </a>
                    </div>
                </div>
            </div>
        </div>            
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">Daftar Lines</div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-12 no-margin">
                        <table class="table table-data table-bordered table-striped" id="table-line">
                            <thead>
                                <tr>
                                    <th><?= PortalPromoLine::getAttributeName('barang_id') ?></th>
                                    <th><?= PortalPromoLine::getAttributeName('min_jumlah') ?></th>
                                    <th><?= PortalPromoLine::getAttributeName('jenis') ?></th>
                                    <th><?= PortalPromoLine::getAttributeName('potongan_persen') ?></th>
                                    <th><?= PortalPromoLine::getAttributeName('potongan_ribuan') ?></th>
                                    <th>Menu</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>            
    </div>    
</div>

<div class="form-group">
    <button type="submit" class="btn btn-success">
        <i class="fa fa-save"></i>
        Simpan
    </button>
    <a href="<?=route('portal_promo.index')?>" class="btn btn-primary">
        <i class="ace-icon fa fa-arrow-left"></i>
        Kembali
    </a>
</div>

<!-- Modal -->
<div id="modal-form" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">Pilih Bonus</h4>
			</div>

			<div class="modal-body">
				<div class="row">
					<div class="col-lg-10">
						<div class="form-group col-lg-6">
                            <?= Form::label('barang_bonus', PortalPromoLine::getAttributeName('barang_id')) ?>
                            <div class="input-group">
                                <?= Form::text('barang_bonus', @$model->barang->nama, ['class'=>'form-control enternext']) ?>
                                <span class="input-group-addon">
                                    <i id="loading" class="fa fa-refresh" style="color: #000;"></i>
                                </span>
                            </div>
                            <?= Form::hidden('barang_bonus_id',null,['id'=>'barang_bonus_id']) ?>
                        </div>
                        <div class="form-group col-lg-6">
                            <?= Form::label('jumlah', PortalPromoLine::getAttributeName('jumlah')) ?>
                            <?= Form::number('jumlah',null,['class'=>'form-control','placeholder'=>'e.x : 10']) ?>
                        </div>
					</div>
				</div>
			</div>

			<div class="modal-footer">
                <a href="javascript:void(0)" class="btn btn-primary" onclick="addBonus()"  data-dismiss="modal">
					<i class="ace-icon fa fa-check"></i>
					Save
				</a>
			</div>
		</div>
	</div>
</div><!-- PAGE CONTENT ENDS -->


@section('js-include')
<script type="text/javascript" src="<?= asset('vendor/') ?>/zein/jquery.format-number.js"></script>
<script type="text/javascript" src="<?= asset('vendor/') ?>/my-form-jquery.js"></script>
<script type="text/javascript" src="<?= asset('vendor/') ?>/jquery.typeahead/typeahead.jquery.min.js"></script>
<script type="text/javascript" src="<?= asset('vendor/') ?>/jquery.typeahead/bloodhound.min.js"></script>
<script type="text/javascript" src="<?= asset('vendor/') ?>/zein/jquery.enter-next.js"></script>
<script type="text/javascript" src="<?= asset('vendor/') ?>/ace/chosen.jquery.min.js"></script>
@endsection 

@section('css-include')
<link rel="stylesheet" href="<?= asset('vendor/') ?>/ace/chosen.min.css" />
@endsection 

@section('js-inline')
//<script type="text/javascript">
var template = '<?= preg_replace('/[\r\n]+/', '', $template) ?>';
var template2 = '<?= preg_replace('/[\r\n]+/', '', $template2) ?>';

var barang = new Bloodhound({
    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    remote: '{{route('barang.ajaxBarang','')}}/%QUERY'
});
barang.initialize();
barang.remote.ajax.beforeSend = function() {
    if(loading===true) {
        return false;
    }

    $('#loading').removeClass("fa-refresh");
    $('#loading').addClass("fa-spinner fa-spin");
}

function addItem(){
    if($("[name=barang_id]").val() !== undefined && $("[name=barang_id]").val() !== null 
            && $("[name=barang_id]").val() !== ''){
        var check = $("#tr-"+$("[name=barang_id]").val()).length;
        
        if(check < 1){
            var row = template;
            row = row.replace(/\{BARANG_ID\}/g,$("[name=barang_id]").val());
            row = row.replace(/\{BARANG_NAMA}/g,$("[name=barang]").val());
            row = row.replace(/\{SPAN_MIN_JUMLAH}/g,$("[name=min_jumlah]").val());
            row = row.replace(/\{MIN_JUMLAH}/g,$("[name=min_jumlah]").val());
            row = row.replace(/\{JENIS}/g,$("select[name=jenis]").val());
            row = row.replace(/\{JENIS_NAMA}/g,$("[name=jenis] option:selected").text());
            row = row.replace(/\{SPAN_POTONGAN_PERSEN}/g,$("[name=potongan_persen]").val());
            row = row.replace(/\{POTONGAN_PERSEN}/g,$("[name=potongan_persen]").val().parseFloatId());
            row = row.replace(/\{SPAN_POTONGAN_RIBUAN}/g,$("[name=potongan_ribuan]").val());
            row = row.replace(/\{POTONGAN_RIBUAN}/g,$("[name=potongan_ribuan]").val().parseFloatId());

            $('#table-line > tbody').append(row);
        }else{
            var id = $("[name=barang_id]").val();
            console.log(id);
            $("#barang_id-"+id).val($("[name=barang_id]").val());
            $("#barang_nama-"+id).val($("[name=barang]").val());
            $("#bm-"+id).text($("[name=barang]").val());
            $("#min_jumlah-"+id).val($("[name=min_jumlah]").val().parseFloatId());
            $("#mj-"+id).text($("[name=min_jumlah]").val());
            $("#jenis-"+id).val($("select[name=jenis]").val());
            $("#jenis_nama-"+id).val($("[name=jenis] option:selected").text());
            $("#jm-"+id).text($("[name=jenis] option:selected").text());
            $("#potongan_persen-"+id).val($("[name=potongan_persen]").val().parseFloatId());
            $("#prs-"+id).text($("[name=potongan_persen]").val());
            $("#potongan_ribuan-"+id).val($("[name=potongan_ribuan]").val().parseFloatId());
            $("#prb-"+id).text($("[name=potongan_ribuan]").val());
        }
        
        formLineClear();
    }else{
        showWarningGritter("Pilih Barang Terlebih dahaulu");
    }

    return false;
}

function editItem(id){
    $("[name=barang_id]").val($("#barang_id-"+id).val());
    $("[name=barang]").val($("#barang_nama-"+id).val());
    $("[name=min_jumlah]").val($("#min_jumlah-"+id).val().parseFloatId().format());
    $("[name=jenis]").val($("#jenis-"+id).val());
    $("[name=potongan_persen]").val($("#potongan_persen-"+id).val().parseFloatId().format());
    $("[name=potongan_ribuan]").val($("#potongan_ribuan-"+id).val().parseFloatId().format());
}

function deleteItem(id){
    $("#tr-"+id).remove();
}

function formLineClear(){
    $("[name=barang_id]").val("");
    $("[name=barang]").val("");
    $("[name=min_jumlah]").val(0);
    $("[name=potongan_persen]").val(0);
    $("[name=potongan_ribuan]").val(0);
}

function showModal(id){
    $("[name=barang_id]").val(id);
    
    $("#barang_bonus_id").val("");
    $("[name=barang_bonus]").val("");
    $("[name=jumlah]").val(0);
}

function addBonus(){
    var id = $("[name=barang_id]").val();
    var barang_bonus_id = $("#barang_bonus_id").val();
    var barang_bonus = $("[name=barang_bonus]").val();
    var jumlah = $("[name=jumlah]").val();
    
    var check = $('#tr-'+id+'-'+barang_bonus_id).length;
    if(check <= 0){
        var row = template2;
        row = row.replace(/\{BARANG_ID\}/g,id);
        row = row.replace(/\{BARANG_NAMA}/g,barang_bonus);
        row = row.replace(/\{BONUS_BARANG_ID}/g,barang_bonus_id);
        row = row.replace(/\{SPAN_JUMLAH}/g,jumlah);
        row = row.replace(/\{JUMLAH}/g,jumlah);
        $('#table-bonus-'+id+' > tbody').append(row);
    }else{
        $('#barang_id-'+id+'-'+barang_bonus_id).val(barang_bonus_id);
        $('#jumlah-'+id+'-'+barang_bonus_id).val(jumlah);
        $('#bm-'+id+'-'+barang_bonus_id).text(barang_bonus);
        $('#j-'+id+'-'+barang_bonus_id).text(jumlah);
    }
    
    $('#tr-bonus-'+id).removeClass("hidden");
}

function load(){
    <?php if($model->exists) : ?>
        <?php foreach ($model->lines as $l) : ?>
            var row = template;
            row = row.replace(/\{BARANG_ID\}/g,'<?= $l->barang_id ?>');
            row = row.replace(/\{BARANG_NAMA}/g,'<?= $l->barang->nama ?>');
            row = row.replace(/\{SPAN_MIN_JUMLAH}/g,'<?= Helper::idNumber($l->min_jumlah) ?>');
            row = row.replace(/\{MIN_JUMLAH}/g,'<?= $l->min_jumlah ?>');
            row = row.replace(/\{JENIS}/g,'<?= $l->jenis ?>');
            row = row.replace(/\{JENIS_NAMA}/g,'<?= $l->getJenis() ?>');
            row = row.replace(/\{SPAN_POTONGAN_PERSEN}/g,'<?= Helper::idNumber($l->potongan_persen) ?>');
            row = row.replace(/\{POTONGAN_PERSEN}/g,'<?= $l->potongan_persen ?>');
            row = row.replace(/\{SPAN_POTONGAN_RIBUAN}/g,'<?= Helper::idNumber($l->potongan_ribuan) ?>');
            row = row.replace(/\{POTONGAN_RIBUAN}/g,'<?= $l->potongan_ribuan ?>');

            $('#table-line > tbody').append(row);
            
            <?php foreach ($l->bonus as $b) : ?>
                var id = '<?= $l->barang_id ?>';
                var row = template2;
                row = row.replace(/\{BARANG_ID\}/g,id);
                row = row.replace(/\{BARANG_NAMA}/g,'<?= $b->nama ?>');
                row = row.replace(/\{BONUS_BARANG_ID}/g,'<?= $b->id ?>');
                row = row.replace(/\{SPAN_JUMLAH}/g,'<?= Helper::idNumber($b->pivot->jumlah) ?>');
                row = row.replace(/\{JUMLAH}/g,'<?= $b->pivot->jumlah ?>');

                $('#tr-bonus-'+id).removeClass("hidden");
                $('#table-bonus-'+id+' > tbody').append(row);
            <?php endforeach; ?>
        <?php endforeach; ?>
    <?php endif; ?>
}

$(function(){
    ajaxFormGritter.bind('mainForm',{onSuccess:function(data){
        document.location = '<?=route('portal_promo.show', 999)?>'.replace(/999/, data);
    }});
    
    $('[name=barang]')
    .typeahead({
        hint: true,
        highlight: true,
        minLength: 1
    }, {
        displayKey: 'value',
        source: barang.ttAdapter(),
        templates: {
            suggestion: function(a) {
                $('#loading').removeClass("fa-spinner fa-spin");
                $('#loading').addClass("fa-refresh");
                return a.show;
            }
        }
    })
    .on('typeahead:selected', function(a,b){
        $('#barang_id').val(b.id);
    });
    
    $('[name=barang_bonus]')
    .typeahead({
        hint: true,
        highlight: true,
        minLength: 1
    }, {
        displayKey: 'value',
        source: barang.ttAdapter(),
        templates: {
            suggestion: function(a) {
                $('#loading').removeClass("fa-spinner fa-spin");
                $('#loading').addClass("fa-refresh");
                return a.show;
            }
        }
    })
    .on('typeahead:selected', function(a,b){
        $('#barang_bonus_id').val(b.id);
    });
    
    $('[name=jenis]').change(function(){
        var jns = $(this).val();
        
        if(jns === '<?= PortalPromoLine::JENIS_PERSEN ?>'){
            $("#potongan_persen").removeClass("hidden");
            $("#potongan_ribuan").addClass("hidden");
            $("[name=potongan_ribuan]").val(0);
        }else{
            $("#potongan_ribuan").removeClass("hidden");
            $("#potongan_persen").addClass("hidden");
            $("[name=potongan_persen]").val(0);
        }
    });
    
    $('.chosen-select').chosen({allow_single_deselect:true}); 
    
    $('input:not([disabled=disabled]),select,[type=submit]').enterNext();
    
    $('.datepicker').datepicker({
        format: 'dd-mm-yyyy',
        clearDate: false,
        autoclose: true,
        todayHighlight: true
    });
    
    $('.format-number').formatNumber();
    
    load();
});
@endsection