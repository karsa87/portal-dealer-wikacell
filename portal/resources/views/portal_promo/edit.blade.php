@extends('layout/default')

@section('title')
Portal Promo
@endsection

@section('breadcrumb')
<li>
    <a href="<?= route('portal_promo.index')?>">Portal Promo</a>
</li>
<li>
    Edit
</li>
@endsection

@section('content-header')
Portal Promo
<small>
    <i class="ace-icon fa fa-angle-double-right"></i>
    Edit
</small>
@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <?= Form::model($model, ['route' => ['portal_promo.update', $model->id], 'id'=>'mainForm', 'method'=>'PUT']) ?>
            @include('portal_promo/_form')
            <?= Form::close() ?>
        </div>
    </div>
</div>
@endsection