@extends('layout/default')

@section('title')
Portal Promo
@endsection

@section('breadcrumb')
<li>
    <a href="<?= route('portal_promo.index')?>">Portal Promo</a>
</li>
<li>
    Create
</li>
@endsection

@section('content-header')
Tambah Portal Promo
<!-- <small>
    <i class="ace-icon fa fa-angle-double-right"></i>
    Create
</small> -->
@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <?= Form::model($model, ['route' => ['portal_promo.store'], 'id'=>'mainForm']) ?>
            @include('portal_promo/_form')
            <?= Form::close() ?>
        </div>
    </div>
</div>
@endsection