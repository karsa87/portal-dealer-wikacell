<?php
use Karsa\Helper;
use App\Models\Barang;
use App\Models\Kategori;
use App\Models\Brand;
use App\Models\Preorder;
use App\Models\PortalPromoLine;
?>

@extends('layout.default')

@section('title')
Form Order
@endsection

@section('content')
<div class="content-wrapper">    
    <!-- Main content -->
    <section class="content">
      <div class="row">
        
        <div class="col-md-7 col-xs-12 col-sm-12 filter-top" style="margin-bottom: 15px;">               
          <form class="form-inline">
            <div class="form-group">              
              <div class="input-group">                  
                <?= Form::text('search',"",['class'=>'form-control',"placeholder"=>"Search"]) ?>
                <div class="input-group-addon">
                    <?= Form::select('kategori_id', [''=>'All']+Helper::createSelect(Kategori::orderBy('nama','ASC')->get(), 'nama'), null, ['class'=>'filter-select']) ?>
                </div>
                <div class="input-group-addon">
                    <?= Form::select('brand_id', [''=>'All']+Helper::createSelect(Brand::orderBy('nama','ASC')->get(), 'nama'), null, ['class'=>'filter-select']) ?>
                </div>
                <div class="input-group-addon search"><span class="fa fa-search"></span></div>
              </div>
            </div>              
          </form>         
        </div>

        <div class="col-md-5">
            <!--            <div class="col-md-4 col-sm-4 col-xs-4">
                <a href="<?= route('trackorder') ?>" class="btn btn-block btn-success">
                    <span class="fa fa-compass"></span> <font>Track Order</font>
                </a>
            </div>-->
            <?php
                $memberCard  = Auth::user()->getMemberCard();
                $member      = $memberCard->member;
                $piutang     = $memberCard->getTotalPiutang();
            ?>
            
            <div class="col-md-4 col-sm-4 col-xs-4">
                <a href="#" class="btn btn-block btn-info" title="Saldo" data-toggle="tooltip">
                    Rp <?= Helper::idNumber($member->saldo) ?>
                </a>
            </div>
            
            <div class="col-md-4 col-sm-4 col-xs-4">
                <a href="<?= route('trackorder') ?>" class="btn btn-block btn-warning" title="Limit" data-toggle="tooltip">
                    <!--<span class="fa fa-compass"></span> <font>Track Order</font>-->
                    Rp <?= Helper::idNumber($memberCard->max_piutang - $piutang); ?>
                </a>
            </div>

            <div class="col-md-4 col-sm-4 col-xs-4">
                <a href="#" class="btn btn-block btn-danger" title="Piutang" data-toggle="tooltip">
                    Rp <?= Helper::idNumber($piutang) ?>
                </a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-7">
          <div class="box">
            <div class="alert alert-danger alert-dismissible" style="display: none">
                <button type="button" class="close">&times;</button>
                <h4><i class="icon fa fa-ban"></i> Peringatan!</h4>
                <div id="alert"></div>
            </div>
            <div class="box-header">
                <h3 class="box-title"><?= Barang::getAttributeName("_title") ?></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
              <table class="table table-hover" id="mainTable" width="100%"> 
                <thead>
                    <th><?= Barang::getAttributeName('nama') ?></th>
                    <th><?= Barang::getAttributeName('keterangan') ?></th>
                    <th><?= Barang::getAttributeName('harga') ?></th>
                    <th width="15%">Qty</th>
                    <th width="20%">&nbsp;</th>
                </thead>                            
              </table>
            </div>
          </div>
        </div>

        <div class="col-md-5">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">List Barang</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive tbl-listBarang">
                <table border="0" class="table table-hover" id="tbl-listBarang">
                    <thead>                     
                        <tr>
                            <td width="55%">Nama Barang</td>
                            <td width="10%">Qty</td>                                                          
                            <td width="20%">Subtotal</td>                                                       
                            <td width="5%">&nbsp;</td>                                                          
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>

            <p>&nbsp;</p>

            <table border="0" class="table table-hover" id="tbl-total">
                <thead>                     
                    <tr>
                        <td width="58%">Total Harga Barang</td>                                                          
                        <td align="left" id="total_qty">16</td>                                                          
                        <td align="right" id="total_awal">Rp. 100000</td>                                                          
                        <td>&nbsp;</td>                                                          
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Potongan Harga</td>
                        <td>&nbsp;</td>
                        <td align="right">Rp. 0</td>
                    </tr>
                    <tr>
                        <td>Biaya Lain</td>
                        <td>&nbsp;</td>
                        <td align="right">Rp. 0</td>
                    </tr>
                </tbody>   
                <tfoot>
                    <tr class="green">
                        <td>Total Tagihan</td>
                        <td>&nbsp;</td>
                        <td align="right" id="total_akhir">Rp. 500000</td>
                    </tr>
                    <?php if($saldo > 50000) : ?>
                    <tr>
                        <td>
                            <input type="checkbox" name="use_saldo" class="minimal" onclick="isChecked()"
                                   <?= ($model->use_saldo == Preorder::USE_SALDO_YA) ? 'checked' : '' ?> > 
                            Anda akan menggunakan saldo anda untuk pembayaran orderan anda
                        </td>
                        <td id="usesaldo" style="display: none" colspan="2">
                            <input type="text" name="saldo" class="form-control format-number text-right" value="<?= Helper::idNumber($model->saldo) ?>" onblur="sendSaldo()">
                        </td>
                    </tr>
                    <?php endif; ?>
                </tfoot>                 
            </table>

            <p>&nbsp;</p>

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="col-md-6 col-sm-6 col-xs-12" style="text-align: center">
                    <a href="<?= route('preorder.checkout',["id"=>$model->id]) ?>" class="btn-checkout">Process</a>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12" style="text-align: center">
                    <button class="btn-cancel" onclick="removeAllRow()">Cancel</button>                        
                </div>
            </div>
          </div>          
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection

@section('js-include')
<script type="text/javascript" src="<?= asset('vendor/') ?>/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?= asset('vendor/') ?>/jquery.datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?= asset('vendor/') ?>/jquery.datatables/jquery.dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="<?= asset('vendor/') ?>/utils/jquery.format-number.js"></script>
<script type="text/javascript" src="<?= asset('vendor/') ?>/assets/js/sweetalert2.min.js"></script>
@endsection

@section('css-include')
<link rel="stylesheet" href="<?= asset('vendor/') ?>/assets/css/sweetalert2.min.css" />
@endsection 

@section('js-inline')
//<script type="text/javascript">
var mainTable;
ajaxDeleteGritter.initialize({
    onSuccess:function(data){
        mainTable.fnDraw();
    }
});

$(".close").click(function(){
    $(".alert").hide();
})

function updateList(barang_id){
    var cek = $("#row-"+barang_id).length;
    var qty =  $("#counter-"+barang_id).val();
    
    if(qty > 0){
        var data = {
            preorder_id : "<?= $model->id ?>",
            barang_id   : barang_id,
            qty         : qty,
            _token      : '<?= csrf_token() ?>'
        };

        $.ajax({
            type: "POST",
            url: "<?= route('preorder.update.list_preorder')?>",
            data: data,        
            dataType: "json",
            success: function(respons) {
                if(respons.status !== 0){
                    swal(
                        'Oops...',
                        respons.message,
                        'error'
                    );
//                    $(".alert").show();
//                    $(".alert > #alert").html(respons.message);
                }else{
                    var barang_id = respons.data.id;
                    if(cek > 0){
                        $("#tbl-listBarang > tbody > #row-"+barang_id+" > .qty_").html($("#counter-"+barang_id).val());
                        var row_bonus = $("#row-"+barang_id+"-bonus");
                        if(respons.data["bonus"] !== ""){
                            $("#tbl-listBarang > tbody").append("<tr id='row-"+barang_id+"-bonus'><td colspan='5' class='tket font-11'>Bonus : "+respons.data["bonus"]+"</td></tr>");
                        }else if(row_bonus.length > 0){
                            row_bonus.remove();
                        }
                    }else{
                        $("#tbl-listBarang > tbody").append("<tr id='row-"+respons.data.id+"'></tr>");
                        var ket_bonus = ""
                        for(var key in respons.data){
                            if(key == "qty"){                    
                                $("#tbl-listBarang > tbody > tr:last-child").append("<td class='qty_'>"+respons.data[key]+"</td>");
                            }else if(key === "subtotal"){
                                $("#tbl-listBarang > tbody > tr:last-child").append("<td class='subtotal'>"+respons.data[key].format()+"</td>");
                            }else if(key === "id"){
                                continue;
                            }else if(key === "bonus"){
                                ket_bonus = respons.data[key];
                            }else{
                                $("#tbl-listBarang > tbody > tr:last-child").append("<td>"+respons.data[key]+"</td>");
                            }
                        }
                        $("#tbl-listBarang > tbody > tr:last-child").append("<td><button class='btn-remove' onclick='removeRow("+respons.data.id+")'><span class='fa fa-times-circle-o'></span></button></td>");

                        $("#btn-toggle-"+barang_id).html("Update list");
                        $("#btn-toggle-"+barang_id).removeClass("btn-add").addClass("btn-del");
                        
                        if(ket_bonus !== ""){
                            $("#tbl-listBarang > tbody").append("<tr id='row-"+respons.data.id+"-bonus'></tr>");
                            $("#row-"+respons.data.id+"-bonus").append("<td colspan='5' class='tket font-11'>Bonus : "+ket_bonus+"</td>");
                        }
                    }
                }
                recalculate();
            }
        });
    }
    else if(cek != 0 && qty == 0){
        removeRow(barang_id);         
    }
}

function removeRow(barang_id){
    var data = {
        preorder_id : <?= $model->id ?>,
        barang_id   : barang_id,       
        _token      : '<?= csrf_token() ?>'
    };

    $.ajax({
        type: "POST",
        url: "<?= route('preorder.delete.list')?>",
        data: data,        
        dataType: "json",
        success: function(respons) {
            if(respons.status !== 0){
                swal(
                    'Oops...',
                    respons.message,
                    'error'
                );
//                $(".alert").show();
//                $(".alert > #alert").html(respons.message);
            }else{
                $("#row-"+barang_id).remove();
                $("#btn-toggle-"+barang_id).html("Add to list");
                $("#btn-toggle-"+barang_id).removeClass("btn-del").addClass("btn-add");
                $("#counter-"+barang_id).val(0);
                recalculate();
            }

        }
    });
}

function updateQty(method,id){
    if(method == 0){        
        var ctr = parseFloat($('#counter-'+id).val()) - 1;
        if(ctr < 0){ ctr = 0; }
        $('#counter-'+id).val(ctr);
    }else{
        var ctr = parseFloat($('#counter-'+id).val()) + 1;       
        $('#counter-'+id).val(ctr);
    }
}

function removeAllRow(){
    $("#tbl-listBarang > tbody").empty();
    $(".btn-del").removeClass("btn-del").addClass("btn-add");
}

function recalculate(){
    // Total Awal
    var sub = $(".subtotal");
    var total_awal = 0;
    for (i = 0; i < sub.length; i++) { 
        total_awal += sub[i].innerHTML.parseFloatId();
    }
    
    $("#total_awal").text(total_awal.format());
    
    // Total Awal
    var qty = $(".qty_");
    var total_qty = 0;
    for (i = 0; i < qty.length; i++) { 
        total_qty += qty[i].innerHTML.parseFloatId();
    }
    
    $("#total_qty").text(total_qty.format());
    
    var total_akhir = total_awal;
    $("#total_akhir").text("Rp. "+ total_akhir.format());
}

function isChecked() {
    var checked = $(".minimal").is(":checked");

    if (checked) {
        $("#usesaldo").show();
    } else {
        $("#usesaldo").hide();
    }
}

function sendSaldo() {
    var checked = "<?= Preorder::USE_SALDO_TIDAK ?>";
    if($('[name=use_saldo]').is(":checked")) {
        checked = "<?= Preorder::USE_SALDO_YA ?>";
    }
    
    var saldo = $('[name=saldo]').val().parseFloatId();
    
    var param = {
        _token: '<?= csrf_token() ?>',
        use_saldo: checked,
        saldo: saldo,
        id: '<?= $model->id ?>'
    };
    
    if(checked && saldo > parseInt('<?= $saldo ?>')){
        showWarningGritter("Saldo yang anda miliki kurang");
        $('[name=saldo]').val(parseInt('<?= $saldo ?>').format());
        
        return;
    }
    
    $.post("<?= route('preorder.update.preorder') ?>", param, function(result){
        if(result.status !== 0){
            showDangerGritter(result.message);
        }
    });
}

$(function(){
    mainTable = $('#mainTable').dataTable({
        "aoColumns": [ 
            { },
            { },
            { },
            { },
            { }
        ],
        "stateSave": false,
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": "<?= route('preorder.index.data')?>",
        "sServerMethod": "GET",
        "aaSorting": [[ 0, "asc" ]],
        "bLengthChange": false,
        "bFilter": false,
        "iDisplayRow": 25,
//        "iDisplayLength": 50,
//        "pageLength" : 25,
        "aLengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]],
        "fnServerParams": function ( aoData ) {
            aoData.push( { "name": "_token", "value": '<?=  csrf_token() ?>' } );
            aoData.push({ "name": "kategori_id", "value": $('[name=kategori_id]').val() });
            aoData.push({ "name": "brand_id", "value": $('[name=brand_id]').val() });
            aoData.push({ "name": "search", "value": $('[name=search]').val() });
        },
        "initComplete": function(settings, json) {
            @foreach ($model->detail as $row)
                $("#btn-toggle-<?= $row->id ?>").removeClass("btn-add").addClass("btn-del");
                $("#btn-toggle-<?= $row->id ?>").html("Update List");
                $("#counter-<?= $row->id ?>").val(<?= $row->pivot->jumlah ?>);
                $("#tbl-listBarang > tbody").append("<tr id='row-<?= $row->id ?>'></tr>");               
                $("#tbl-listBarang > tbody > tr:last-child").append(
                    "<td><?= preg_replace("/\"/", "'", $row->nama) ?></td>"
                    +"<td class='qty_'><?= intval($row->pivot->jumlah) ?></td>"
                    +"<td class='subtotal'>"+<?= $row->pivot->total_awal ?>.format()+"</td>"
                    +"<td><button class='btn-remove' onclick='removeRow(<?= $row->id ?>)'><span class='fa fa-times-circle-o'></span></button></td>"
                );
                
                <?php
                    $line = PortalPromoLine::with('bonus')->find($row->pivot->promo_line_id);
                    
                    $ket_bonus = [];
                    if(isset($line)){
                        foreach ($line->bonus as $b){
                            $ket_bonus[] = $b["jumlah"] . " " . $b["nama"];
                        }
                    }
                ?>
                
                if('<?= $row->pivot->promo_line_id ?>' !== ""){
                    $("#tbl-listBarang > tbody").append("<tr id='row-<?= $row->id ?>-bonus'></tr>");
                    $("#row-<?= $row->id ?>-bonus").append("<td colspan='5' class='tket font-11'>Bonus : <?= implode(", ", $ket_bonus) ?></td>");
                }
            @endforeach
            recalculate();
        }
    });
    
    var parent =  $("#mainTable_paginate").parent();
    parent.removeClass("col-xs-6");
    parent.addClass("col-xs-8 text-right");
    
    var parent =  $("#mainTable_info").parent();
    parent.removeClass("col-xs-6");
    parent.addClass("col-xs-4");
    
    
    $('[name=search]').keyup(function(){
        mainTable.fnDraw(false);
    });
    
    $('[name=kategori_id]').change(function(){
        mainTable.fnDraw(false);
    });
    
    $('[name=brand_id]').change(function(){
        mainTable.fnDraw(false);
    });
    
    $('[name=use_saldo]').change(function() {
        var checked = "<?= Preorder::USE_SALDO_TIDAK ?>";
        if($(this).is(":checked")) {
            checked = "<?= Preorder::USE_SALDO_YA ?>";
        }
        
        var param = {
            _token: '<?= csrf_token() ?>',
            use_saldo: checked,
            saldo: 0,
            id: '<?= $model->id ?>'
        };
        
        $.post("<?= route('preorder.update.preorder') ?>", param, function(result){
            if(result.status !== 0){
                showDangerGritter(result.message);
            }
        });
    });
    
    $('.format-number').formatNumber();
    
    isChecked();
});
@endsection