<?php

use App\Models\Preorder;

/* @var $model Preorder */

?>
@extends('layout/default')

@section('title')
Preorder
@endsection

@section('breadcrumb')
<li>
    <a href="<?= route('preorder.index')?>">Preorder</a>
</li>
<li>
    Detail
</li>
@endsection

@section('content-header')
Detail Preorder
@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-5">
            <div class="panel panel-default">
                <div class="panel-heading">Detail Preorder</div>
                <div class="panel-body">                    
                    <div class="row">
                        <div class="col-lg-6">                            
                            <dt><?= Preorder::getAttributeName('kode') ?></dt>
                            <dd><?= $model->kode?></dd>
                        </div>
                    </div>
                    <div class="space-2"></div>
                    <div class="row">
                        <div class="col-lg-6">
                            <dt><?= Preorder::getAttributeName('nama') ?></dt>
                            <dd><?= $model->nama ?></dd>                            
                        </div>
                        <div class="col-lg-6">                            
                            <dt><?= Preorder::getAttributeName('telepon') ?></dt>
                            <dd><?= $model->telepon?></dd>
                        </div>  
                    </div>
                    <div class="space-2"></div>
                    <div class="row">
                        <div class="col-lg-12">                            
                            <dt><?= Preorder::getAttributeName('alamat') ?></dt>
                            <dd><?= $model->alamat ?></dd>
                        </div>  
                    </div>                
                    <div class="space-2"></div>
                    <div class="row">
                        <div class="col-lg-6">                            
                            <dt><?= Preorder::getAttributeName('kabupaten_kota_id') ?></dt>
                            <dd><?= $model->kabupaten_kota_id ? $model->kabupatenKota->nama : null ?></dd>
                        </div>  
                        <div class="col-lg-6">                            
                            <dt><?= Preorder::getAttributeName('provinsi') ?></dt>
                            <dd><?= $model->kabupaten_kota_id ? $model->kabupatenKota->provinsi->nama : null ?></dd>
                        </div>
                    </div>                                    
                </div>
            </div>            
        </div>
        <div class="col-md-5">
            <div class="panel panel-default">
                <div class="panel-heading">Detail Spv Preorder</div>
                <div class="panel-body">          
                    <div class="row">
                        <div class="col-lg-8">                            
                            <dt><?= Preorder::getAttributeName('spv') ?></dt>
                            <dd><?= $model->spv ?></dd>
                        </div>
                    </div>    
                    <div class="space-2"></div>
                    <div class="row">
                        <div class="col-lg-4">                            
                            <dt><?= Preorder::getAttributeName('no_spv') ?></dt>
                            <dd><?= $model->no_spv ?></dd>
                        </div>
                    </div>      
                </div>
            </div>            
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6">
            <?php if(Auth::user()->hasAbility('preorder.edit')) : ?>
            <a href="<?php echo route('preorder.edit', $model->id) ?>" class="btn btn btn-warning">
                <i class="fa fa-edit"></i>
                Edit
            </a>
            <?php endif; ?>
            <a href="<?= route('preorder.index')?>" class="btn btn btn-primary">
                <i class="ace-icon fa fa-arrow-left"></i>
                Kembali
            </a>
        </div>
    </div>
</div>
@endsection