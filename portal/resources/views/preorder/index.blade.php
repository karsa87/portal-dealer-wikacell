<?php
use Karsa\Helper;
use App\Models\Barang;
use App\Models\Kategori;
use App\Models\Brand;
?>

@extends('layout.default')

@section('title')
Form Order
@endsection

@section('content')
<div class="content-wrapper">    
    <!-- Main content -->
    <section class="content">
      <div class="row">
        
        <div class="col-md-7 col-xs-12 col-sm-12 filter-top" style="margin-bottom: 15px;">               
          <form class="form-inline">
            <div class="form-group">              
              <div class="input-group">                  
                <?= Form::text('search',"",['class'=>'form-control',"placeholder"=>"Search"]) ?>
                <div class="input-group-addon">
                    <?= Form::select('kategori_id', [''=>'All']+Helper::createSelect(Kategori::orderBy('nama','ASC')->get(), 'nama'), null, ['class'=>'filter-select']) ?>
                </div>
                <div class="input-group-addon">
                    <?= Form::select('brand_id', [''=>'All']+Helper::createSelect(Brand::orderBy('nama','ASC')->get(), 'nama'), null, ['class'=>'filter-select']) ?>
                </div>
                <div class="input-group-addon search"><span class="fa fa-search"></span></div>
              </div>
            </div>              
          </form>         
        </div>

        <div class="col-md-5">
            <!--            <div class="col-md-4 col-sm-4 col-xs-4">
                <a href="<?= route('trackorder') ?>" class="btn btn-block btn-success">
                    <span class="fa fa-compass"></span> <font>Track Order</font>
                </a>
            </div>-->
            <?php
                $memberCard  = Auth::user()->getMemberCard();
                $member      = $memberCard->member;
                $piutang     = $memberCard->getTotalPiutang();
            ?>
            
            <div class="col-md-4 col-sm-4 col-xs-4">
                <a href="#" class="btn btn-block btn-info" title="Saldo" data-toggle="tooltip">
                    Rp <?= Helper::idNumber($member->saldo) ?>
                </a>
            </div>
            
            <div class="col-md-4 col-sm-4 col-xs-4">
                <a href="<?= route('trackorder') ?>" class="btn btn-block btn-warning" title="Limit" data-toggle="tooltip">
                    <!--<span class="fa fa-compass"></span> <font>Track Order</font>-->
                    Rp <?= Helper::idNumber($memberCard->max_piutang - $piutang); ?>
                </a>
            </div>

            <div class="col-md-4 col-sm-4 col-xs-4">
                <a href="#" class="btn btn-block btn-danger" title="Piutang" data-toggle="tooltip">
                    Rp <?= Helper::idNumber($piutang) ?>
                </a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-7">
          <div class="box">
            <div class="box-header">
                <h3 class="box-title"><?= Barang::getAttributeName("_title") ?></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
              <table class="table table-hover" id="mainTable">
                <thead>
                    <th class="column-padding-right"><?= Barang::getAttributeName('kode') ?></th>
                    <th><?= Barang::getAttributeName('nama') ?></th>
                    <th><?= Barang::getAttributeName('keterangan') ?></th>
                    <th><?= Barang::getAttributeName('harga') ?></th>
                    <th>Qty</th>
                    <th>&nbsp;</th>
                </thead>                            
              </table>
            </div>
          </div>
        </div>

        <div class="col-md-5">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Riwat Transaksi</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
                <table border="0" class="table table-hover">
                    <thead>                     
                        <tr>
                            <td style="padding: 5px 5px 5px 30px">No Nota</td>
                            <td>Tanggal Order</td>
                            <td width="30%" align="center">Status</td>                                                          
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($list_order as $order) : ?>
                        <tr>
                            <td style="padding: 5px 5px 5px 30px"><?= $order->no_order ?></td>
                            <td><?= Helper::formatDate($order->tanggal, Helper::FORMAT_DATE_ID_LONG) ?></td>
                            <td align="center"><font color="yellow"><?= $order->getStatusOrderBadge() ?></font></td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
          </div>

          <div class="col-xs-12 col-sm-12 col-md-12 promo">
              <i><?= $promo ?><i>
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection

@section('js-include')
<script type="text/javascript" src="<?= asset('vendor/') ?>/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?= asset('vendor/') ?>/jquery.datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?= asset('vendor/') ?>/jquery.datatables/jquery.dataTables.bootstrap.min.js"></script>
@endsection

@section('js-inline')
//<script type="text/javascript">

@endsection
