@extends('layout/default')

@section('title')
Preorder
@endsection

@section('breadcrumb')
<li>
    <a href="<?= route('preorder.index')?>">Preorder</a>
</li>
<li>
    Edit
</li>
@endsection

@section('content-header')
Preorder
<small>
    <i class="ace-icon fa fa-angle-double-right"></i>
    Edit
</small>
@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <?= Form::model($model, ['route' => ['preorder.update', $model->id], 'id'=>'mainForm', 'method'=>'PUT']) ?>
            @include('preorder/_form')
            <?= Form::close() ?>
        </div>
    </div>
</div>
@endsection