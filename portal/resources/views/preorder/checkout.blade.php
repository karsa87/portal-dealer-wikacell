<?php
use Karsa\Helper;
use App\Models\Preorder;
use App\Models\Lookup;
?>

@extends('layout.default')

@section('title')
Dashboard
@endsection

@section('content')
<div class="content-wrapper">    
    <!-- Main content -->
    <section class="content">    
    <div class="row">
        <div class="col-md-8">
          <div class="box">
            <div class="box-header bg-aqua">
                <h3 class="box-title">Checkout</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
                <?php
                    $no = 1;
                    $total_qty = 0;
                    $total_awal = 0;
                    $total_akhir = 0;
                    $total_potongan = 0;
                ?>
                <table class="table table-hover" id="mainTable">
                    <thead>
                        <th class="text-center">No</th>
                        <th>Barang</th>
                        <th class="text-center">Qty</th>
                        <th class="text-center">Harga Satuan</th>
                        <th class="text-center">Potongan</th>
                        <th class="text-center">Total</th>
                    </thead>
                    <tbody>
                        <?php foreach ($model->detail as $k => $d) : 
                            $c = "";
                            if($d->pivot->status == Preorder::LINE_STATUS_BONUS){
                                $c = "<span class='badge bg-red'><small>bonus</small></span>";
                            }
                        ?>
                        <tr>
                            <td class="text-center"><?= $no++; ?></td>
                            <td><?= sprintf("%s %s",$d->nama,$c) ?></td>
                            <td class="text-center"><?= Helper::idNumber($d->pivot->jumlah) ?></td>
                            <td align="right">Rp. <?= Helper::idNumber($d->pivot->harga_jual) ?></td>
                            <td align="right">Rp. <?= Helper::idNumber($d->pivot->total_potongan) ?></td>
                            <td align="right">Rp. <?= Helper::idNumber($d->pivot->total_awal) ?></td>
                        </tr>
                        <?php $total_qty += $d->pivot->jumlah ?>
                        <?php $total_awal += $d->pivot->total_awal ?>
                        <?php $total_akhir += $d->pivot->total_akhir ?>
                        <?php $total_potongan += $d->pivot->total_potongan ?>
                        <?php endforeach; ?>
                    </tbody>
                    <tfoot class="bg-gray" style="opacity: .50">
                        <tr>
                            <td colspan="5" class="text-right"><b>Sub Total : </b></td>
                            <td align="right">Rp. <?= Helper::idNumber($total_awal) ?></td>
                        </tr>
                        <tr>
                            <td colspan="5" class="text-right"><b>Total Potongan : </b></td>
                            <td align="right">Rp. <?= Helper::idNumber($total_potongan) ?></td>
                        </tr>
                        <?php if($model->use_saldo) : 
                            $total_akhir -= $model->saldo;
                        ?>
                        <tr>
                            <td colspan="5" class="text-right"><b>Saldo : </b></td>
                            <td align="right">Rp. <?= Helper::idNumber($model->saldo) ?></td>
                        </tr>
                        <?php endif; ?>
                        <tr>
                            <td colspan="5" class="text-right"><b>Total : </b></td>
                            <td align="right">Rp. <?= Helper::idNumber($total_akhir) ?></td>
                        </tr>
                    </tfoot>                
                </table>
            </div>
          </div>
        </div>
        <div class="col-lg-4">
            <div class="box">
                <div class="box-header bg-blue">
                    <h3 class="box-title">Catatan</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive">
                    <?= Form::textarea('keterangan',"",['class'=>'form-control',"placeholder"=>"Catatan"]) ?>
                </div>
            </div>
            
            <div class="box">
                <div class="box-header bg-blue">
                    <h3 class="box-title">Payment Method</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive">
                    <p><img src="https://image.cermati.com/v1428073854/brands/avqoa9rfng8bklutfhm6.jpg" alt="BCA" width="200" /></p>
                    <p>Anda bisa melakukan pembayaran dengan melakukan transfer ke : </p>
                    <p>BCA 98372423743</p>
                    <p>a/n Rudi Rakhmad Hidayatullah</p>
                </div>
            </div>
            
            <div class="box">
                <div class="box-header bg-blue">
                    <h3 class="box-title">Informasi Toko</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive">
                    <table width="100%">
                        <tr>
                            <td width="20%">Delivery To</td>
                            <td width="3%">:</td>
                            <td><?= $model->memberCard->member->nama ?></td>
                        </tr>
                        <tr>
                            <td>Alamat</td>
                            <td>:</td>
                            <td><?= $model->memberCard->member->alamat ?></td>
                        </tr>
                        <tr>
                            <td>Telp</td>
                            <td>:</td>
                            <td><?= $model->memberCard->member->telepon ?></td>
                        </tr>
                        <?php if($model->penjualan_id) : ?>
                        <tr>
                            <td>No Nota</td>
                            <td>:</td>
                            <td>$model->penjualan->no_referensi</td>
                        </tr>
                        <?php endif; ?>
                        <tr>
                            <td>No Order</td>
                            <td>:</td>
                            <td><?= $model->no_order ?></td>
                        </tr>
                        <tr>
                            <td>Jatuh Tempo</td>
                            <td>:</td>
                            <td>
                                <?php if($model->jenis == Preorder::JENIS_CASH) : ?>
                                <span id="tgl_tempo">-</span>
                                <?php else : ?>
                                <span id="tgl_tempo"><?= Helper::formatDate($model->tanggal_tempo, Helper::FORMAT_DATE_ID_LONG) ?></span>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <!-- <tr>
                            <td>Status</td>
                            <td>:</td>
                            <td><?= $model->getStatusOrderBadge() ?></td>
                        </tr> -->
                        <tr>                      
                            <td>
                                <input type="checkbox" data-toggle="toggle" name="jenis"
                                       <?= $model->jenis == Preorder::JENIS_CASH ? 'checked' : '' ?>
                                       data-on="<?= Lookup::item(Lookup::PREORDER_JENIS, Preorder::JENIS_CASH) ?>" 
                                       data-off="<?= Lookup::item(Lookup::PREORDER_JENIS, Preorder::JENIS_TEMPO) ?>">
                            </td>
                            <td colspan="3" align="right">
                                <a href="<?= route('preorder.create') ?>" class="btn-checkout">Back</a>
                                <a href="<?= route('preorder.payment',["id"=>$model->id]) ?>" class="btn-cancel" id="btn-payment">Payment</a>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection

@section('js-include')
<script type="text/javascript" src="<?= asset('vendor/') ?>/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?= asset('vendor/') ?>/jquery.datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?= asset('vendor/') ?>/jquery.datatables/jquery.dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="<?= asset('vendor/') ?>/utils/my-script.js"></script>
<script type="text/javascript" src="<?= asset('vendor/') ?>/utils/jquery.format-number.js"></script>
<script type="text/javascript" src="<?= asset('vendor/') ?>/assets/js/sweetalert2.min.js"></script>
@endsection

@section('css-include')
<link rel="stylesheet" href="<?= asset('vendor/') ?>/assets/css/sweetalert2.min.css" />
@endsection 

@section('js-inline')
//<script type="text/javascript">
$(document).ready(function(){
    $('[name=jenis]').change(function(){
        var jenis = '<?= Preorder::JENIS_CASH ?>';
        var tgl_tempo = '';
        if(!$(this).is(":checked")){
            jenis = '<?= Preorder::JENIS_TEMPO ?>';

            var tempo = '<?= $model->memberCard->jumlah_tempo ?>';
            var someDate = new Date();
            someDate.setDate(someDate.getDate() + parseInt(tempo)); 
            var dd = someDate.getDate();
            var mm = someDate.getMonth() + 1;
            var y = someDate.getFullYear();

            tgl_tempo = y +'-'+ mm +'-'+ dd;

            $('#tgl_tempo').text(dd+" "+_bulan(mm)+" "+y);
        }else{
            $('#tgl_tempo').text('-');
        }
        
        var piutang   = '<?= Auth::user()->getMemberCard()->getTotalPiutang() ?>'.parseFloatId();
        var max_piutang   = '<?= Auth::user()->getMemberCard()->max_piutang ?>'.parseFloatId();
        
        if(piutang > max_piutang){
            swal(
                'Oops...',
                'Anda tidak bisa membeli secara tempo, karena anda sudah melebihi batas maximal hutang anda',
                'error'
            );
//            showDangerGritter("Anda tidak bisa membeli secara tempo, karena anda sudah melebihi batas maximal hutang anda");
            $(this).prop('checked', false);
        }else{
            var param = {
                _token: '<?= csrf_token() ?>',
                jenis: jenis,
                tanggal_tempo:tgl_tempo,
                id: '<?= $model->id ?>'
            };

            $.post("<?= route('preorder.update.preorder') ?>", param, function(result){
                if(result.status !== 0){
                    swal(
                        'Oops...',
                        result.message,
                        'error'
                    );
//                    showDangerGritter(result.message);
                }
            });
        }
    });
    
    $("#btn-payment").click(function(){
        var keterangan = $('[name=keterangan]').val();

        var param = {
            _token: '<?= csrf_token() ?>',
            keterangan: keterangan,
            id: '<?= $model->id ?>'
        };
        
        $.post("<?= route('preorder.update.preorder') ?>", param, function(result){
            if(result.status !== 0){
                showDangerGritter(result.message);
            }
        });
    });
});
@endsection
