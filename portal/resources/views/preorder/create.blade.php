@extends('layout/default')

@section('title')
Preorder
@endsection

@section('breadcrumb')
<li>
    <a href="<?= route('preorder.index')?>">Preorder</a>
</li>
<li>
    Create
</li>
@endsection

@section('content-header')
Tambah Preorder
<!-- <small>
    <i class="ace-icon fa fa-angle-double-right"></i>
    Create
</small> -->
@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <?= Form::model($model, ['route' => ['preorder.store'], 'id'=>'mainForm']) ?>
            @include('preorder/_form')
            <?= Form::close() ?>
        </div>
    </div>
</div>
@endsection