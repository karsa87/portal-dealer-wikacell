<?php

return [
    'app_name' => 'Portal Dealer System',
    'app_name_1' => 'PDS',
    'app_name_2' => '(Portal Dealer System)',
    'app_name_long' => 'PDS (Portal Dealer System)',
    'app_copyright' => 'IT SIGUB',
    'app_version' => 'Beta Version 0.1',
    'app_year' => 2017,
    'link_billing' => 'http://localhost/counterpos/public/'
];
