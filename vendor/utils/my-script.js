function _bulan(id) {
    var tmp = {1: 'Januari', 2: 'Februari', 3: 'Maret', 4: 'April', 5: 'Mei', 6: 'Juni', 7: 'Juli', 8: 'Agustus', 9: 'September', 10: 'Oktober', 11: 'November', 12: 'Desember'};
    return id ? tmp[id] : tmp;
}

function reverseDateFormat(date)
{
    if (date.match(/\d{4}-\d{2}-\d{2}/)) {
        return date.replace(/(\d{4})-(\d{2})-(\d{2})/, '$3-$2-$1');
    } else {
        return date.replace(/(\d{2})-(\d{2})-(\d{4})/, '$3-$2-$1');
    }
}

$(function(){
    $('[type=password]').next().mousedown(function(){ $(this).prev().attr('type', 'text'); });
    $('[type=password]').next().mouseup(function(){ $(this).prev().attr('type', 'password'); });
});