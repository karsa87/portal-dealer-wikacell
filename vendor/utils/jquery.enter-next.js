/*
 * Author : Zein
 * Mail   : big_zein@yahoo.com
 * 
 * Desc   : Apply next focus on enter, to desired selector
 */
(function($){
    $.fn.enterNext = function(){
        var _multi = this;
        if(_multi) { 
            (function(tmp){
                setTimeout(function(){
                    $(tmp).filter(':visible:first').get(0).focus();
                },200);
            })(_multi);
        }
        _multi.unbind('keypress.enterNext');
        return _multi.bind('keypress.enterNext',function(e){
            if( e.keyCode === 13 && !$(this).is('[type=submit],textarea') || $(this).is('textarea') && e.keyCode === 13 && e.ctrlKey ){
                var _self = this;
                var stop = false;
                var next = false;
                _multi.each(function(){
                    if(stop && $(this).is(":visible") && !$(this).is(":disabled") ){
                        next = this;
                        return false;
                    }
                    if(this === _self){
                        stop = true;
                    }
                });
                $(this).blur();
                $(next).focus();
                $(next).select();
                return false;
            }
        });
    };
}(jQuery));