/*
 * Author : Zein
 * Mail   : big_zein@yahoo.com
 * 
 * Desc   : Format number on key up, to desired selector
 */
(function($){
    $.fn.formatNumber = function(){
        var _multi = this;
        _multi.unbind('keyup.formatNumber');
        var f = _multi.bind('keyup.formatNumber',function(){
            var val = $(this).val().parseFloatId();
            if( val ) {
                val = val.format();
            }
            $(this).val(val);
        });
        _multi.keyup();
        return f;
    };
}(jQuery));

Number.prototype.format = function (fix)
{
    return this.toFixed(fix ? fix : 0).replace(/\d(?=(\d{3})+$)/g, '$&,');
};

String.prototype.parseFloatId = function()
{
    var string = this.concat().replace(/[^-0-9\,\.]/g, '');
    if (!string) {
        return '';
    }
    return typeof string === 'string' && string !== '' && string !== '-' ? parseFloat(string.replace(/\,/g, '').replace(/\./g, '.')) : string;
};