function showGritter(body, opt) {
	if (!body) {
		return false;
    }
	var def = $.extend({class_name: '', image: ''}, opt);
	def.class_name = 'gritter-light' + (def.class_name ? ' gritter-' + def.class_name : '');
    $.extend(def, {text: body});
    return $.gritter.add(def);
}
function showSuccessGritter(body, opt) {
	return showGritter(body, $.extend({class_name: 'success'}, opt));
}
function showDangerGritter(body, opt) {
	return showGritter(body, $.extend({class_name: 'error', sticky:true}, opt));
}
function showWarningGritter(body, opt) {
	return showGritter(body, $.extend({class_name: 'warning'}, opt));
}
function showInfoGritter(body, opt) {
	return showGritter(body, $.extend({class_name: 'info'}, opt));
}

ajaxSaveGritter = {
	options: {
		url: false,
		data: false,
		success: false,
        onAjaxStart: function(){},
        onComplete: function(){},
        onSuccess: function(){},
		_text : {
			process: 'Menyimpan data...',
			done: 'Data telah tersimpan...',
			error: 'Harap tunggu proses ini selesai...'
		}
	},
	_is_saving: false,
	_is_saved: false,
	_success_param: '1',
	render: false,
	onreadystatechange: false,
	updateText: function(message) {
		$(this.render.render).html(message);
	},
	run: function(data, check_saved) {
		this.options.data = data;
		if (check_saved === undefined)
			check_saved = false;
		if (this._is_saving === true) {
			showWarningGritter(this.options._text.error);
			return false;
		}
		if (this._is_saved === true && check_saved) {
			showDangerGritter(this.options._text.done);
			return false;
		}
		this._is_saving = true;
        _self = this;
        _self.options.onAjaxStart();
        _self.render = showGritter(_self.options._text.process, {sticky: true, before_open: function() {
                var xhr = $.ajax($.extend({}, _self.options, {
                    complete: function(data) {
                        if (data.responseText === _self._success_param) {
                            _self._is_saved = true;
                            _self.options.onSuccess(data.responseText);
                        }
                        setTimeout(function() {
                            $.gritter.remove(_self.render);
                        }, 100);
                        _self._is_saving = false;
                    }}));
                if (_self.onreadystatechange) {
                    var tmp = xhr.onreadystatechange;
                    var lastText = '';
                    xhr.onreadystatechange = function() {
                        tmp();
                        if (xhr.readyState === 3) {
                            _self.onreadystatechange(xhr.responseText.replace(lastText, ''));
                            lastText = xhr.responseText;
                        }
                    };
                }
            }
        });
	}, initialize: function(config) {
		if (config.onreadystatechange !== undefined) {
			this.onreadystatechange = config.onreadystatechange;
		}
		$.extend(this.options, {type: 'POST'}, config);
		this._is_saved = false;
		this._is_saving = false;
	}
};
ajaxDeleteGritter = $.extend(true, {}, ajaxSaveGritter,{
	options:{
        method:'DELETE',
		_text:{
			process:'Menghapus data...',
			done:'Data berhasil dihapus...'
		},
        success:function(data){
            if (isNaN(data)) {
                showDangerGritter(data);
            }else{
                _self.options.onSuccess(data);
                showSuccessGritter(_self.options._text.done);
            }
        }
	}
});
ajaxFormGritter = $.extend(true, {}, ajaxSaveGritter,{
	bind:function(formId,opt){	
		var _saveGritter = this;
		var _form = $('#'+formId);
		var options = {
			url:_form.attr('action'),
			method:_form.attr('method'),
			success:function(data){
				if (isNaN(data)) {
					showDangerGritter(data);
				}else{
                    _saveGritter.options.onSuccess(data);
					showSuccessGritter(_saveGritter.options._text.done);
                    if( _saveGritter.options._forceSubmit === true ) {
                        $(_form).data('ajaxDone',true);
                        setTimeout(function(){$(_form).submit();},1000);
                    }
				}
                _saveGritter.options.onComplete(data);
			},
            _forceSubmit: false
		};
        if(opt._forceSubmit === true) {
            options._text = {
                process : 'Melakukan validasi data...',
                done : 'Melakukan validasi data lanjut...'
            };
        }
        options = $.extend(options, opt);
		_saveGritter.initialize(options);
		_form.submit(function(){
			if($(_form).data('ajaxDone')===true) return true;
			_saveGritter.run($(_form).serialize());
			return false;
		});
	}
});