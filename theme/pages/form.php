<!DOCTYPE html>
<html>
<head>
	<title>Wika Cell - Order</title>
	<link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="../assets/css/style.css">
	<link rel="stylesheet" type="text/css" href="../assets/css/font-awesome.min.css">
</head>
<body style="background-color: #eff6ff">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12 panel-top">
				<div class="col-md-6" style="text-align: left">
					<img src="../assets/images/logo.jpeg" class="logo">
				</div>
				<div class="col-md-6 panel-menu">
					<ul class="nav navbar-nav navbar-right">
				        <li class="user">Halo, William</li>
				        <li><img src="../assets/images/foto.jpg" class="img-circle"></li>
				        <li style="font-size: 20px;"><span class="fa fa-bell-o notif"></span></li>
				        <li class="dropdown">
				          	<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><img src="../assets/images/menu.png" class="i-menu"></a>
				          	<ul class="dropdown-menu">
				            	<li><a href="#">Action</a></li>
				            	<li><a href="#">Another action</a></li>
				            	<li><a href="#">Something else here</a></li>
				            	<li role="separator" class="divider"></li>
				            	<li><a href="#">Separated link</a></li>
				          	</ul>
				        </li>
				    </ul>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-1 panel-left">
				<ul class="side-menu">
					<li><a href="#"><span class="fa fa-home"></span> <br> Dashboard</a></li>
					<li><a href="#"><span class="fa fa-shopping-cart"></span> <br> Order</a></li>
					<li><a href="#"><span class="fa fa-compass"></span> <br> Track Order</a></li>
					<li><a href="#"><span class="fa fa-percent"></span> <br> Promo</a></li>
				</ul>
			</div>	
			<div class="col-md-11">
				<div class="col-md-7 windows filter-top">
					<div class="col-md-6" style="padding:0;">
						<input type="text" name="search" placeholder="Search" class="text-search" style="padding: 10px">
					</div>
					<div class="col-md-2 brs">
						<select class="filter-select">
							<option selected="">Kategory</option>
							<option>Xiomi</option>
							<option>Asus</option>
							<option>Samsung</option>
						</select>
						<span class="fa fa-chevron-down"></span>
					</div>
					<div class="col-md-2 brs">
						<select class="filter-select">
							<option>Brand</option>
							<option>1</option>
							<option>1</option>
						</select>
						<span class="fa fa-chevron-down"></span>
					</div>
					<div class="col-md-2" style="padding:10px;text-align: center;color: #fff;">
						<span class="fa fa-search"></span>
					</div>
				</div>
				
				<div class="col-md-4" style="padding:0;width: 470px;">					
					<a href="#" class="col-md-4 windows btn-order">					
						<span class="fa fa-compass" style="font-size: 29px;"></span> <font>Track Order</font>
					</a>
					<div class="col-md-7 windows btn-saldo">					
						Saldo : <b>Rp 112.000.000</b>
					</div>
				</div>

				<div class="col-md-7 windows" style="padding:0px;">
					<div class="windows-header">
						List Barang
					</div>
					<table border="0" class="col-md-12 windows-table">
						<thead>						
							<tr>
								<td style="padding: 5px 10px 5px 30px" width="10%">Kode</td>
								<td width="30%">Nama Barang</td>
								<td width="15%">Kategory</td>
								<td width="15%">Harga</td>								
								<td>Qty</td>
								<td>&nbsp;</td>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td style="padding: 5px 5px 5px 30px">734</td>
								<td>Xiomi Mi 5c 3/64 GB Black</td>
								<td>Handphone</td>
								<td>Rp. 5.987.164</td>
								<td><img src="../assets/images/qty.jpg"></td>
								<td><button class="btn btn-success" style="padding:5px 10px;font-size:10px;width: 80px;">Update List</button></td>
							</tr>
							<tr>
								<td style="padding: 5px 5px 5px 30px">734</td>
								<td>Xiomi Mi 5c 3/64 GB Black</td>
								<td>Handphone</td>
								<td>Rp. 5.987.164</td>
								<td><img src="../assets/images/qty.jpg"></td>
								<td><button class="btn btn-primary" style="padding:5px 10px;font-size:10px;border-radius: 5px;width: 80px;">Add to List</button></td>
							</tr>
							<tr>
								<td style="padding: 5px 5px 5px 30px">734</td>
								<td>Xiomi Mi 5c 3/64 GB Black</td>
								<td>Handphone</td>
								<td>Rp. 5.987.164</td>
								<td><img src="../assets/images/qty.jpg"></td>
								<td><button class="btn btn-success" style="padding:5px 10px;font-size:10px;width: 80px;">Update List</button></td>
							</tr>
							<tr>
								<td style="padding: 5px 5px 5px 30px">734</td>
								<td>Xiomi Mi 5c 3/64 GB Black</td>
								<td>Handphone</td>
								<td>Rp. 5.987.164</td>
								<td><img src="../assets/images/qty.jpg"></td>
								<td><button class="btn btn-success" style="padding:5px 10px;font-size:10px;width: 80px;">Update List</button></td>
							</tr>
							<tr>
								<td style="padding: 5px 5px 5px 30px">734</td>
								<td>Xiomi Mi 5c 3/64 GB Black</td>
								<td>Handphone</td>
								<td>Rp. 5.987.164</td>
								<td><img src="../assets/images/qty.jpg"></td>
								<td><button class="btn btn-success" style="padding:5px 10px;font-size:10px;width: 80px;">Update List</button></td>
							</tr>
							<tr>
								<td style="padding: 5px 5px 5px 30px">734</td>
								<td>Xiomi Mi 5c 3/64 GB Black</td>
								<td>Handphone</td>
								<td>Rp. 5.987.164</td>
								<td><img src="../assets/images/qty.jpg"></td>
								<td><button class="btn btn-success" style="padding:5px 10px;font-size:10px;width: 80px;">Update List</button></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="col-md-4" style="padding:0px; width: 460px !important;">
					<div class="col-md-12 windows" style="padding: 0px;">
						<div class="windows-header">
							Daftar Belanja
						</div>
						<table border="0" class="col-md-12 windows-table">
							<thead>						
								<tr>
									<td width="15%" style="padding: 5px 5px 5px 30px">#</td>
									<td width="35%">Nama Barang</td>
									<td width="10%">Qty</td>
									<td width="25%" align="center">Subtotal</td>
									<td width="30%" align="center">&nbsp;</td>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td style="padding: 5px 5px 5px 30px">1</td>
									<td>Xiomi Mi 5c 3/64 GB Black</td>
									<td>8</td>
									<td align="right">Rp. 5.987.164</td>
									<td align="center" style="color:red;"><span class="fa fa-times"></span></td>
								</tr>
								<tr>
									<td style="padding: 5px 5px 5px 30px">2</td>
									<td>Xiomi Mi 5c 3/64 GB Black</td>
									<td>8</td>
									<td align="right">Rp. 5.987.164</td>
									<td align="center" style="color:red;"><span class="fa fa-times"></span></td>
								</tr>
								<tr>
									<td style="padding: 5px 5px 5px 30px">3</td>
									<td>Xiomi Mi 5c 3/64 GB Black</td>
									<td>8</td>
									<td align="right">Rp. 5.987.164</td>
									<td align="center" style="color:red;"><span class="fa fa-times"></span></td>
								</tr>
								<tr>
									<td style="padding: 5px 5px 5px 30px">4</td>
									<td>Xiomi Mi 5c 3/64 GB Black</td>
									<td>8</td>
									<td align="right">Rp. 5.987.164</td>
									<td align="center" style="color:red;"><span class="fa fa-times"></span></td>
								</tr>							
							</tbody>
						</table>

						<table border="0" class="col-md-12 windows-table">
							<thead>						
								<tr>
									<td width="50%" style="padding: 5px 5px 5px 30px">Total Harga Barang</td>
									<td width="8%">16</td>
									<td width="30%" align="right">Rp. 10.000.000</td>
									<td width="12%">&nbsp;</td>									
								</tr>
							</thead>
							<tbody>
								<tr>
									<td style="padding: 5px 5px 5px 30px">Potonngan Harga</td>									
									<td>&nbsp;</td>									
									<td align="right">Rp. 20.000</td>									
								</tr>			
								<tr>
									<td style="padding: 5px 5px 5px 30px">Biaya Lain</td>									
									<td>&nbsp;</td>									
									<td align="right">Rp. 8.000</td>									
								</tr>													
							</tbody>
							<tfoot>
								<td style="padding: 5px 5px 5px 30px">Total Tagihan</td>									
								<td>&nbsp;</td>									
								<td align="right">Rp. 10.769.000</td>
								<td></td>
							</tfoot>
						</table>

						<center>
							<button class="btn btn-success" style="width: 150px;">Checout</button>
						</center>
					</div>
				</div>
			</div>		
		</div>
	</div>

	<script type="text/javascript" src="../assets/js/jquery.min.js"></script>
	<script type="text/javascript" src="../assets/js/bootstrap.js"></script>
</body>
</html>