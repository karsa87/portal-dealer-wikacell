<!DOCTYPE html>
<html>
<head>
	<title>Wekka Cell</title>
	<link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="../assets/css/style.css">
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-4 login">
				<div class="col-md-12">
					<img src="../assets/images/logo.jpeg">
				</div>		
				<div class="col-md-12 form-login">
					<span class="label-ket">Silahkan Login ke Akun anda</span>
					<br>
					<form>
					  	<div class="form-group">
					    	<input type="text" class="form-control" name="username" placeholder="Username">
					  	</div>
					  	<div class="form-group">
					    	<input type="password" class="form-control" name="password" placeholder="Password">
					  	</div>
					  	<input type="submit" name="login" value="Login" class="btn btn-primary">
					 </form>
				</div>		
			</div>
			<div class="col-md-8">
				<img src="../assets/images/loginimage.jpg" class="login-logo">
			</div>
		</div>
	</div>
</body>
</html>